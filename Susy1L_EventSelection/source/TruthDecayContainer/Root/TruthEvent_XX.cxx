#include "TruthDecayContainer/TruthEvent_XX.h"

#include <iostream>
#include <vector>

#include "TLorentzVector.h"

#include "TruthDecayContainer/TruthDecayUtils.h"
#include "TruthDecayContainer/TruthEvent_VV.h"


typedef TLorentzVector tlv;


TruthEvent_XX::TruthEvent_XX() :
  prodLabel(-1),
  decayLabel_chi1(-1),
  decayLabel_chi2(-1),
  chi1_pdg(0),
  chi2_pdg(0)
{
  clear();  
}

TruthEvent_XX::~TruthEvent_XX(){}

TruthEvent_XX::TruthEvent_XX(const TruthEvent_XX &rhs) :
  TruthEvent_VV(rhs),
  pchi1(rhs.pchi1),
  pchi2(rhs.pchi2),
  pN1A(rhs.pN1A),
  pN1B(rhs.pN1B),
  prodLabel(rhs.prodLabel),
  decayLabel_chi1(rhs.decayLabel_chi1),
  decayLabel_chi2(rhs.decayLabel_chi2),
  chi1_pdg(rhs.chi1_pdg),
  chi2_pdg(rhs.chi2_pdg)
{}


/////////////////////////////////////////////////////
void TruthEvent_XX::set_evt_XX(const int &in_chi1_pdg, const int &in_chi2_pdg, 
			       const tlv &in_pN1A, const tlv &in_pN1B, 
			       const Decay_boson &in_B1, const Decay_boson &in_B2){

  B1 = in_B1;  
  B2 = in_B2;  

  chi1_pdg = in_chi1_pdg;
  chi2_pdg = in_chi2_pdg;

  pN1A = in_pN1A;
  pN1B = in_pN1B;

  finalize();

}
/////////////////////////////////////////////////////
void TruthEvent_XX::finalize(){

  // input:  
  //  N1, N2 (tlv, pdg)
  //  B1, B2

  TruthEvent_VV::finalize();

  pMis = pN1A+pN1B+B1.pMis+B2.pMis;
  
  if(B1.P4.E()>0){
    pchi1 = pN1A + B1.P4;
    decayLabel_chi1=getBosonLabel(B1);
  }
  if(B2.P4.E()>0) {
    pchi2 = pN1B + B2.P4;
    decayLabel_chi2=getBosonLabel(B2);
  }
  prodLabel=getProdLabel(chi1_pdg,chi2_pdg);

  //
  check_swap();

  return;
}

//////////////////////////////////////////////////
int TruthEvent_XX::getChiLabel(const int &pdg){
  // run 0-5
  if     (std::abs(pdg)==1000022) return 0; // N1
  else if(std::abs(pdg)==1000023) return 1; // N2
  else if(std::abs(pdg)==1000024) return 2; // C1
  else if(std::abs(pdg)==1000025) return 3; // N3
  else if(std::abs(pdg)==1000035) return 4; // N4
  else if(std::abs(pdg)==1000037) return 5; // C2
  return -1;
}

//////////////////////////////////////////////////
int TruthEvent_XX::getProdLabel(const int &pdg1,const int &pdg2){
  int chiLabel1 = getChiLabel(pdg1);
  int chiLabel2 = getChiLabel(pdg2);

  int label = chiLabel1>=chiLabel2 ?
    chiLabel1*10 + chiLabel2 :
    chiLabel2*10 + chiLabel1 ;

  //std::cout << "TruthEvent_XX::getProdLabel  DEBUG  " << pdg1 << " " << chiLabel1 << " "<< pdg2 << " " << chiLabel2 << " " << label << std::endl;
  
  return label;
}



//////////////////////////////////////////////////
int TruthEvent_XX::getBosonLabel(const Decay_boson &B){

  // run 0-3

  if(B.pdg==0)                 return 0; // no bosons (direct)
  else if(std::abs(B.pdg)==24) return 1; // W (from C1)
  else if(B.pdg==23)           return 2; // Z (from N2)
  else if(B.pdg==25)           return 3; // H (from N2)

  std::cout << "TruthEvent_XX::getBosonLabel  WARNING  Unknown boson : pdg=" << B.pdg << ". Return -1. " <<std::endl;
  return -1;
}



//////////////////////////////////////////////////
void TruthEvent_XX::swap12(){

  TruthEvent_VV::swap12();

  TruthDecayUtils::swap(pchi1, pchi2);
  TruthDecayUtils::swap(pN1A, pN1B);
  TruthDecayUtils::swap(decayLabel_chi1, decayLabel_chi2);
  TruthDecayUtils::swap(chi1_pdg, chi2_pdg);
  return;
}

//////////////////////////////////////////////////
void TruthEvent_XX::check_swap(){
  // swap 1<->2 if needed                                    

  if(B1.pdg==-24 && B2.pdg==24)           swap12(); // always place B1 as W+ in case of WW
  if(B1.pdg==23  && std::abs(B2.pdg)==24) swap12(); // always place B2 as Z in case of WZ
  if(B1.pdg==25  && std::abs(B2.pdg)==24) swap12(); // always place B2 as h in case of Wh
  if(B1.pdg==25  && B2.pdg==23)           swap12(); // always place B2 as h in case of Zh

  return;
}

//////////////////////////////////////////////////
void TruthEvent_XX::print(){


  std::cout << "TruthEvent_XX::print() **************************************" << std::endl;

  TruthEvent_VV::print();

  std::cout << "chi1_pdg: " << chi1_pdg << std::endl;
  std::cout << "chi2_pdg: " << chi2_pdg << std::endl;
  std::cout << "prodLabel: " << prodLabel << std::endl;
  
  TruthDecayUtils::print4mom(pN1A,  "pN1A");
  TruthDecayUtils::print4mom(pN1B,  "pN1B");

  std::cout << "         ************************************************" << std::endl;
  TruthDecayUtils::print4mom(pMis,  "pMis");  
  std::cout << "********************************************************" << std::endl;


  return;
}

//////////////////////////////////////////////
void TruthEvent_XX::clear(){

  TruthEvent_VV::clear();

  // tlv
  pchi1.SetXYZM(0,0,0,0); pchi2.SetXYZM(0,0,0,0);  
  pN1A.SetXYZM(0,0,0,0); pN1B.SetXYZM(0,0,0,0);  

  // labels
  prodLabel=-1;   
  decayLabel_chi1=-1;   
  decayLabel_chi2=-1;   

  // pdg
  chi1_pdg=0; chi2_pdg=0;
 
  return;
}
