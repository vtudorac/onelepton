#include "xAODRootAccess/Init.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/ScanDir.h"
#include "SampleHandler/DiskListXRD.h"
#include "SampleHandler/fetch.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/SampleLocal.h"
#include "EventLoop/Job.h"
#include "EventLoop/DirectDriver.h"
#include "EventLoop/LSFDriver.h"
#include "EventLoop/CondorDriver.h"
#include "EventLoop/LocalDriver.h"

// Grid tools
#include "EventLoopGrid/GridDriver.h"
#include "EventLoopGrid/PrunDriver.h"

// Rootcore includes
#include "SusySkimDriver/xAODEvtLooper.h"
#include "SusySkimDriver/ConfigExtract.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/ObjectTools.h"
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/ConfigMgr.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/PkgDiscovery.h"

#include "TSystem.h"

#include <boost/regex.hpp>

bool check(const char* inputFlag, const char* compar, const char* log)
{

  if (strcmp(inputFlag, compar) == 0   ) return true;
  if ((strcmp(inputFlag, "-help") ==0 || strcmp(inputFlag, "-h") ==0 )){
    printf("  => %-30s : %-50s \n",compar,log);
    return false;
  }
  return false;
}


int main( int argc, char* argv[] )
{

  const char* APP_NAME = "xAODNtMaker";


  //
  TString submitDir     = "submitDir";
  TString packageName   = "";

  TString samples       = "";
  TString sampleList    = "";
  TString file          = "";
  TString fileList      = "";

  std::string selectorName  = "";
  std::string sysSet        = "";

  TString productionGroup  = "";
  TString voms             = "";
  TString tag              = "";
  TString gridSite         = "";
  TString excludeGridSite  = "";
  TString destinationSite  = "";

  TString groupSet         = "";
  TString includeFiles     = "";
  TString excludeFiles     = "";
  TString deepConfig       = "";
  TString grid_filter      = "";
  float nEvents = -1.0;

  int writeSkims = 1;
  int writeTrees = 0;
  int writeMetaData = 3;
  int MaxEvents  = -1;
  int SkipEvents  = -1;
  int priority   = -1;
  int nFilesPerJob = -1;
  int nGBPerJob = -1;
  int nFiles = -1;

  TString DAODKernelName   =    "";
  bool noCutBookkeepers    = false;
  bool disableCuts         = false;
  bool isData              = false;
  bool isAf2               = false;
  bool isTruthOnly         = false;
  bool useXROOTd           = false;
  bool submitToGrid        = false;
  bool submitToLxPlus      = false;
  bool submitToMogon       = false;
  bool submitToCondor      = false;
  bool skipPRWCheck        = false;
  bool checkAllSamples     = false;
  bool skipSampleChecks    = false;
  bool express             = false;

  // This is now default
  bool noSubmit            = false;
  bool noEmail             = false;

  bool allowTaskDuplication = false;
  bool useNewCode = false;

  std::cout<<"Read input arguments"<<endl;

  /** Read inputs to program **/
  for(int i = 1; i < argc; i++) {
    if ((strcmp(argv[i], "-help") ==0 || strcmp(argv[i], "-h") ==0 ))
      printf("%s","Printing help menu... \n");
    if (check(argv[i],"-d","Specifies the name of the submission directory created by SampleHandler"))
      submitDir = argv[++i];
    else if (check(argv[i], "-packageName","Name of the package which contains all your configuration files"))
      packageName= argv[++i];
    else if (check(argv[i], "-deepConfig","Deep config file, needs to be placed in <PackageName>/data/config/ (Rel. 20) or have a name PackageName/<anything>.config and be installedi (Rel. 21)"))
      deepConfig = argv[++i];
    else if (check(argv[i],"-s","Name of a single input sample"))
      samples = argv[++i];
    else if (check(argv[i],"-S","Name of a text file containing samples (only for grid running)"))
      sampleList = argv[++i];
    else if (check(argv[i],"-F","Name of a single input file/url (only for local running)"))
      file = argv[++i];
    else if (check(argv[i],"-f","Name of a text file containing file paths/urls (only for local running)"))
      fileList = argv[++i];
    else if (check(argv[i], "-writeSkims","Write out skims, Write nothing==0, Nominal only==1, nominal+systematics==2"))
      writeSkims = atoi(argv[++i]);
    else if (check(argv[i], "-writeTrees","Write out trees produced as defined in analysis code, Write nothing==0, Nominal only==1, nominal+systematics==2"))
      writeTrees= atoi(argv[++i]);
    else if (check(argv[i], "-writeMetaData","Write metaData, default 3. 0==no metaData, 1==skim, 2==tree, 3==both files" ))
      writeMetaData = atoi(argv[++i]);
    else if (check(argv[i], "-selector","Name of selector for tree stream"))
      selectorName = argv[++i];
    else if (check(argv[i], "-groupSet","Name of a groupSet in SampleHandler"))
      groupSet= argv[++i];
    else if (check(argv[i], "-sysSet","Systematic set: text file containing list of systematic names as defined by CP tools"))
      sysSet= argv[++i];
    else if (check(argv[i], "-nEvents","Sets the total number of events in sample, used for normalization"))
      nEvents = atof(argv[++i]);
    else if (check(argv[i], "-gridFilter","Sets SampleHandlers nc_grid_filter field (default is *.root) used to collect input files (only respected for grid running)"))
      grid_filter = argv[++i];

    else if (check(argv[i], "-DAODKernelName","DAOD kernel in the CutBookkeepers to use. Note: DxAODKernel in the deep config will be overwritten!"))
      DAODKernelName = argv[++i];
    else if (check(argv[i], "--noCutBookkeepers","disable usage of CutBookkeepers (e.g. for own TRUTH1 production)"))
      noCutBookkeepers = true;
    else if (check(argv[i], "--disableCuts","disable the selector level cuts"))
      disableCuts = true;
    else if (check(argv[i], "-MaxEvents","Sets the maximum number of events to run over, i.e. 10 will only run over the first 10 events"))
      MaxEvents = atoi(argv[++i]);
    else if (check(argv[i], "-SkipEvents","Sets the number of events to skip (useful for debugging)"))
      SkipEvents = atoi(argv[++i]);
    else if (check(argv[i], "-tag","Specific a tag to be added to the data set name for grid jobs"))
      tag = argv[++i];
    else if (check(argv[i], "-gridSite","Specific grid site(s) you want to send the job to"))
      gridSite = argv[++i];
    else if (check(argv[i], "-excludeGridSite","Specific grid site(s) you DONT want to send your jobs to"))
      excludeGridSite = argv[++i];
    else if (check(argv[i], "-destinationSite","Specific grid site you want to send your job output to"))
      destinationSite = argv[++i];
    else if (check(argv[i],"-includeFiles","Specific files to include" ) )
      includeFiles = argv[++i];
    else if (check(argv[i],"-excludeFiles","Specific files to exclude" ) )
      excludeFiles = argv[++i];
    else if (check(argv[i], "-priority","Select samples only with this priority, specified as a property in sample text files"))
      priority=atoi(argv[++i]);
    else if (check(argv[i], "-nFilesPerJob","Number of files per job for grid running "))
      nFilesPerJob=atoi(argv[++i]);
    else if (check(argv[i], "-nGBPerJob","Number of GB per job for grid running "))
      nGBPerJob=atoi(argv[++i]);
    else if (check(argv[i], "-nFiles","Process a limited number of files for grid running (useful for testing)"))
      nFiles=atoi(argv[++i]);
    else if (check(argv[i], "-productionGroup","Submit jobs as group jobs (with given group). User needs to have production role for that group"))
      productionGroup = argv[++i];
    else if (check(argv[i], "-voms","Specify VOMS proxy. Used for group production jobs, e.g. atlas:/atlas/phys-susy/Role=production"))
      voms = argv[++i];
    else if (check(argv[i], "--isData","Are we running over data?"))
      isData=true;
    else if (check(argv[i], "--isAf2","ATLAS fast sim sample? Needed for SUSYTools"))
      isAf2=true;
    else if (check(argv[i], "--isTruthOnly","Run over truth only samples and write out objects using truth information?"))
      isTruthOnly=true;
    else if (check(argv[i], "--useXROOTd","Access files via xrootd. Only sample input is supported currently (-s). Single file input (-F) is not yet supported."))
      useXROOTd=true;
    else if (check(argv[i], "--submitToGrid","Submit this sample to the grid using PrunRunDriver; be sure to run 'scripts/setupGrid.sh'"))
      submitToGrid=true;
    else if (check(argv[i], "--submitToLxPlus","Submit to the lxplus batch system. Currently can only read files from EOS, e.g. /eos/atlas/atlaslocalgroupdisk/"))
      submitToLxPlus=true;
    else if (check(argv[i], "--submitToMogon","Submit to the LSF batch system on Mogon"))
      submitToMogon=true;
    else if (check(argv[i], "--condor","Submit to the condor system. Currently only configured for Penn t3 and its SRM"))
      submitToCondor=true;
    else if (check(argv[i], "--noSubmit","Don't submit anything, useful for running over grid samples and you just want to check if the submission passes check "))
      noSubmit=true;
    else if (check(argv[i], "--noEmail","Don't send email notification from atlpan."))
      noEmail=true;
    else if (check(argv[i], "--skipPRWCheck","Don't check for valid prw profiles in the grid submission check"))
      skipPRWCheck=true;
    else if (check(argv[i], "--checkAllSamples","Perform grid submission checks for all samples in a groupset"))
      checkAllSamples=true;
    else if (check(argv[i], "--skipSampleChecks","Skip grid submission checks"))
    	skipSampleChecks=true;
    else if (check(argv[i], "--express","Set express mode for grid jobs (useful for testing)"))
    	express=true;
    else if (check(argv[i], "--allowTaskDuplication","Allow Task Duplication for broken jobs"))
    	allowTaskDuplication=true;
    else if (check(argv[i], "--useNewCode","Use New code in resubmission"))
    	useNewCode=true;
    else if ((strcmp(argv[i], "-help") ==0 || strcmp(argv[i], "-h") ==0 ))
      return 0;
    else{
      std::cout << "Unknown argument:  " << argv[i] << std::endl;
      return 0;
    }
  }
  std::cout<<"Done with input arg"<<std::endl;
  // Add in tag
  if( !submitDir.Contains( tag.Data() ) ) submitDir += "_"+tag;

  // (Detailed) sample checks for these batch systems
  // Add your batch system here if you want them to be run
  bool runSampleChecks = false;
  if( !skipSampleChecks && submitToGrid ) runSampleChecks = true;

  // This will get the necessary deep configuration file,
  // check that it exists, provided the user did not
  // specify anything at the command line
  std::cout<<"PkgDiscovery::Init in run_xAODNtMaker"<<std::endl;
  CHECK( PkgDiscovery::Init(deepConfig,selectorName) );

  SampleHandler* sampleHandler = new SampleHandler();
  sampleHandler->includeFiles(includeFiles);
  sampleHandler->excludeFiles(excludeFiles);
  // NO! Get from groupSet
  // sampleHandler->setDeepConfig( PkgDiscovery::getDeepConfig() );
  sampleHandler->setSamplePriority(priority);
  sampleHandler->isBulkSubmission( submitToGrid || submitToLxPlus || submitToMogon || submitToCondor );
  sampleHandler->disablePRWCheck(skipPRWCheck);

  // User doesn't want a predefined set, so build the SampleInfo object from users configuration
  std::cout<<"groupSet.IsWhitespace() = "<<groupSet.IsWhitespace()<<std::endl;
  if( groupSet.IsWhitespace() ){

    // User defined groupSet
    groupSet = "USER";

    SampleHandler::SampleInfo* sampleInfo = new SampleHandler::SampleInfo();
    sampleInfo->groupName = groupSet;

    // Set deep config file: note that the full path is specified in CentralDB::Read
    // to ensure things work properly both locally and on the grid!
    sampleInfo->deepConfigFile = PkgDiscovery::getDeepConfig();

    // User configured run
    sampleInfo->addSysSet(TString(sysSet),writeSkims,writeTrees);

    //
    SampleHandler::SampleConfig* sampleConfig = new SampleHandler::SampleConfig();
    sampleConfig->process        = "USER";
    sampleConfig->isData         = isData;
    sampleConfig->isAF2          = isAf2;
    sampleConfig->isTruthOnly    = isTruthOnly;
    sampleConfig->priority       = priority;

    if(isData){
      std::cout<<"This is Data"<<std::endl;
    }else{
      std::cout<<"This is MC"<<std::endl;
    }

    //
    if( sampleList!="" ){
      // Note this method pushes the updated sampleConfig into sampleInfo!
      sampleHandler->addSampleConfig(sampleInfo,sampleConfig,sampleList);
    }
    else if(samples !="" || file !="" || fileList !=""){

      sampleConfig->file = "";
      sampleConfig->fileList = "";
      sampleConfig->samples = vector<TString>();

      if (samples !="" ) {
        std::vector<TString> localSampleList;
        localSampleList.clear();
        localSampleList.push_back(samples);
        sampleConfig->samples = localSampleList;
      }
      if (file !="" ) {
        sampleConfig->file = file;
      }
      if (fileList !="") {
        sampleConfig->fileList = fileList;
      }

      //
      sampleInfo->sampleConfig.push_back(sampleConfig);

    }

    // Finally save configuration
    sampleHandler->addGroup(sampleInfo);

  }
  else{
    sampleHandler->initialize(groupSet,tag);
  }

  // Set up the job for xAOD access:
  xAOD::Init().ignore();
  
  std::cout<<"Checking sample"<<std::endl;
  // Check submission (including configuration files)
  CHECK( sampleHandler->check(runSampleChecks,groupSet,selectorName,checkAllSamples) );

  // Don't submit, return after above checks..
  if( noSubmit ) return 0;

  
  // Mainly for use with the CondorDriver...
  TString hostname = "";
  const char* hostname_tmp = getenv("HOSTNAME");
  if(hostname_tmp != NULL){
    hostname = hostname_tmp;
  }
  TString username = "";
  const char* username_tmp = getenv("USER");
  if(username_tmp != NULL){
    username = username_tmp;
  }

  //
  const SampleHandler::SampleInfo* sampleInfo = sampleHandler->getSampleInfo(groupSet);

  // One job submitted for each systematic and each sampleConfig
  for( auto& sys : sampleInfo->sysConfig){
    for( auto& sampleConfig : sampleInfo->sampleConfig ){


      // Construct the samples to run on:
      SH::SampleHandler sh;

      for( auto& s : sampleConfig->samples ){

        // Only take the samples specified by the user, if given
        if (!samples.IsWhitespace()) {
          if (!samples.Contains(s.Data())) {
            continue;
          }
        }

        // Grid driver
        if(submitToGrid){
          SH::addGrid(sh,s.Data());

          // Only set if the user asked
          if( !grid_filter.IsWhitespace() ){
            sh.setMetaString("nc_grid_filter", grid_filter.Data() );
          }

        }
        // LSF driver
        else if(submitToLxPlus || submitToMogon){

          //if (submitToMogon) {
          SH::ScanDir().filePattern("*.root*").scan(sh,s.Data());

          //} else {
          //      TString rootDir_samples = "root://eosatlas//"+s;
          //      SH::ScanDir().scanEOS (sh, rootDir_samples.Data() );
          //}
        }
        else if(submitToCondor){
          SH::addGrid(sh,s.Data());
          sh.setMetaString("nc_grid_filter", "*");

          if(hostname.Contains("bnl")){
            //SH::makeGridDirect(sh, "BNL-OSG2_LOCALGROUPDISK", "srm://dcsrm.usatlas.bnl.gov:8443/srm/managerv2?SFN=", "root://dcxrd.usatlas.bnl.gov:1096//", false);
            //SH::makeGridDirect(sh, "BNL-OSG2_LOCALGROUPDISK", "root://dcdoor16.usatlas.bnl.gov:1094/", "root://dcxrd.usatlas.bnl.gov:1096//", true); // temporarily set to true, to use datasets where we are missing a few files!
	    // From BNL experts: "dcgftp is a round robin so that is a better choice all the time". Port 1096 is apperently preferred as well.
	    SH::makeGridDirect(sh, "BNL-OSG2_LOCALGROUPDISK", "root://dcgftp.usatlas.bnl.gov:1096/", "root://dcxrd.usatlas.bnl.gov:1096//", true); // temporarily set to true, to use datasets where we are missing a few files!
          }
          else if(hostname.Contains("upenn")){
            SH::makeGridDirect(sh, "UPENN_LOCALGROUPDISK", "srm://srm.hep.upenn.edu:8443/srm/v2/server?SFN=/disk/space00/", "root://hn.at3f//", false);
          }
          else{
            std::cerr << "Host \"" << hostname << "\" not currently setup for Condor submission! Aborting." << std::endl;
            abort();
          }
        }

	// Access file via xrootd
	else if(useXROOTd){
	  std::cout << "Use xrootd to access to the file(s)." << std::endl;
	  // assume format like root://someserver:/path/to/sample/
	  std::string server = ((TObjString*)s.Tokenize(":")->At(1))->String().ReplaceAll("//","").Data();
	  std::string path =  ((TObjString*)s.Tokenize(":")->At(2))->String().Data();	  
	  SH::DiskListXRD dlist(server, path, true);
	  SH::ScanDir().scan(sh, dlist);
	}

        // Normal user interactive submission
        else{
          SH::ScanDir().scan( sh,s.Data() );
        }
      }
      
      // add single file/url to sample handler
      // code copied from SH::readFileList code
      if (sampleConfig->file != "") {
        std::auto_ptr<SH::SampleLocal> sample (new SH::SampleLocal ("sample"));
        sample->add( sampleConfig->file.Data() );
        sh.add( sample.release() );
      }

      // read input files/urls from txt file
      if (sampleConfig->fileList != "") {
        SH::readFileList (sh, "sample", sampleConfig->fileList.Data());
      }

      // Input tree
      sh.setMetaString( "nc_tree", "CollectionTree" );

      // Print what we found:
      sh.print();

      // Create an EventLoop job:
      EL::Job* job = new EL::Job();
      job->options()->setDouble (EL::Job::optRemoveSubmitDir, 1);

      job->sampleHandler( sh );

      // Use TTreeCache to speed up the jobs
      job->options()->setDouble (EL::Job::optCacheSize, 10*1024*1024);

      // Run over a user defined subset of the full file
      if(MaxEvents>0){
        job->options()->setDouble (EL::Job::optMaxEvents,MaxEvents);
      }
      if (SkipEvents>0) {
        job->options()->setDouble (EL::Job::optSkipEvents, SkipEvents);
      }

      // Configure job
      std::cout<<"In run_xAODNtMaker:: Initiating xAODEvtLooper()"<<std::endl;
      xAODEvtLooper* alg = new xAODEvtLooper();
      alg->setDeepConfig( sampleInfo->deepConfigFile );
      alg->isTruthOnly( sampleConfig->isTruthOnly );
      alg->setSampleName(samples);
      alg->setSystematicSet( sys->systematic );
      alg->writeSkims( sys->writeSkims );
      alg->writeTrees( sys->writeTrees );
      alg->isData( sampleConfig->isData );
      alg->isAf2( sampleConfig->isAF2 );
      alg->setSelectorName(selectorName);
      alg->setNumEvents(nEvents);
      alg->setDAODKernelName(DAODKernelName);
      alg->setNoCutBookkeepers(noCutBookkeepers);
      alg->setDisableCuts(disableCuts);
      alg->writeMetaData(writeMetaData);
      alg->setMaxEvents(MaxEvents);

      // Give algorithm to Job
      job->algsAdd( alg );

      TString submissionDir = submitDir;

      if( !sys->systematic.IsWhitespace() ){
        submissionDir += "_"+sys->systematic;
      }

      if(submitToGrid){
        EL::PrunDriver prun_driver;
        //EL::LSFDriver prun_driver; // for debugging locally

        TString name = "";

        if( sampleConfig->isData ){
          if( !productionGroup.IsWhitespace() ) name = "group." + productionGroup + ".%in:name[1]%.%in:name[2]%.%in:name[3]%";
          else name = "user.%nickname%.%in:name[1]%.%in:name[2]%.%in:name[3]%";
        }
        else{
          // use tags instead of human readable for mc, since we might have multiple samples per dsid/processname
          if( !productionGroup.IsWhitespace() ) name = "group." + productionGroup;
          else name = "user.%nickname%";
          // for convenience, also add process name (will be matched by wildcard later)
          std::string processName(sampleConfig->process.Data());
          boost::regex re("_%[0-9]+"); // replace the number _%[0-9] fields in signal samples
          name += "."+boost::regex_replace(processName, re, "");
          name += ".%in:name[2]%.%in:name[6]%";
        }

        // Add in tag
        name += "_" + sampleHandler->getDSNameSuffix(tag,sys->systematic,sys->writeSkims,sys->writeTrees);

        if( !gridSite.IsWhitespace() ){
          std::cout << " SUBMITTING TO THE FOLLOWING GRID SITE: " << gridSite << std::endl;
          prun_driver.options()->setString(EL::Job::optGridSite,gridSite.Data());
        }

        if( !excludeGridSite.IsWhitespace() ){
          std::cout << " EXCLUDING THE FOLLOWING GRID SITE: " << excludeGridSite << std::endl;
          prun_driver.options()->setString(EL::Job::optGridExcludedSite,excludeGridSite.Data());
        }

        if( !destinationSite.IsWhitespace() ){
          std::cout << " SETTING DESTINATION GRID SITE: " << destinationSite << std::endl;
          prun_driver.options()->setString(EL::Job::optGridDestSE,destinationSite.Data());
        }

        if (nFiles > 0) {
          std::cout << " SETTING NUMBER OF FILES TO PROCESS: " << nFiles << std::endl;
          prun_driver.options()->setDouble(EL::Job::optGridNFiles, nFiles);
        }

        if (!voms.IsWhitespace()) {
          std::cout << " SETTING VOMS PROXY: " << voms << std::endl;
          prun_driver.options()->setString(EL::Job::optVoms, voms.Data());
        }

        if (express) {
          std::cout << " SETTING EXPRESS MODE" << std::endl;
          prun_driver.options()->setDouble(EL::Job::optGridExpress, 1);
        }

        if (!productionGroup.IsWhitespace()) {
          std::cout << " SETTING OFFICIAL GROUP MODE" << voms << std::endl;
          prun_driver.options()->setDouble(EL::Job::optOfficial, 1);
        }

        std::cout << "SUBMITTING JOBS WITH OUTPUT FORMAT " << name.Data() << std::endl;

        prun_driver.options()->setString("nc_output", "example.txt");
        if(noEmail) prun_driver.options()->setString (EL::Job::optSubmitFlags, "--noEmail");
        if(allowTaskDuplication) prun_driver.options()->setString (EL::Job::optSubmitFlags, "--allowTaskDuplication");
        if(useNewCode) prun_driver.options()->setString (EL::Job::optSubmitFlags, "--useNewCode");
        prun_driver.options()->setString("nc_outputSampleName",name.Data() );
        if (nFilesPerJob > 0) {
          prun_driver.options()->setDouble("nc_nFilesPerJob",nFilesPerJob);
        }
        if (nGBPerJob > 0) {
          prun_driver.options()->setDouble("nc_nGBPerJob",nGBPerJob);
        }
        prun_driver.submitOnly( *job, submissionDir.Data() );

      }
      else if(submitToLxPlus || submitToMogon){
        EL::LSFDriver driver;
        //EL::LocalDriver driver;
        TString shellCommand = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase &&";
        shellCommand += "source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh" ;

        // Number of files per worker node
        if (submitToMogon)
	        job->options()->setDouble (EL::Job::optFilesPerWorker,20);
	    else
	    	job->options()->setDouble (EL::Job::optFilesPerWorker,5);

        job->options()->setBool (EL::Job::optResetShell, false); //debug
//        job->options()->setDouble (EL::Job::optMaxEvents, 5000); //debug

        if (submitToMogon)
        	driver.options()->setString (EL::Job::optSubmitFlags, "-app Reserve3G -R \"rusage[atlasio=5]\" -q atlaslong -W 20:00 -J xAODNtMaker");
        else
	        driver.options()->setString (EL::Job::optSubmitFlags, "-L /bin/bash -q medium");
        //driver.shellInit = shellCommand.Data();

        driver.submitOnly ( *job, submissionDir.Data() );

      }
      else if(submitToCondor){

        // Append the sample name to the dir, since this is in a loop,
        // but driver.submitOnly() will fail if a duplicate submissionDir
        // name is used
        if( !sampleConfig->process.IsWhitespace() ){
          submissionDir += "_"+sampleConfig->process;
        }

        EL::CondorDriver driver;

        TString optCondorConf = "getEnv = True";

        if(hostname.Contains("bnl")){
          // Note! This will have to be adjusted for non-Penn users on the USATLAS BNL shared t3
          if(username.Contains("jgonski")){
            optCondorConf += "\n accounting_group = group_atlas.harvard";
          }
	  else if(username.Contains("jshahini")){
	    optCondorConf += "\n accounting_group = group_atlas.ucsc";
	  }
          else{
            optCondorConf += "\n accounting_group = group_atlas.upenn";
          }

          // Example for how to exclude a specific node, if needed
          //optCondorConf += "\n Requirements = (Machine != \"spar0404.usatlas.bnl.gov\")";
        }

        job->options()->setString(EL::Job::optCondorConf, optCondorConf.Data());
        job->options()->setDouble(EL::Job::optRetries,3); // in case of transient xrd issues

        // To avoid having more jobs than the queue allows for. Besides, the data files tend to be small
        // NOTE: Turned off now, due to memory issues when switching files...
        //if(sampleConfig->isData){
        //  job->options()->setDouble(EL::Job::optFilesPerWorker, 10);
        //}

        driver.submitOnly ( *job, submissionDir.Data() );
      }
      else{
        //EL::LocalDriver driver;
        EL::DirectDriver driver;
        //EL::PrunDriver driver;
        driver.submit( *job, submissionDir.Data() );
        std::cout<<"hei"<<std::endl;
      }
    }
  }

  return 0;

}
