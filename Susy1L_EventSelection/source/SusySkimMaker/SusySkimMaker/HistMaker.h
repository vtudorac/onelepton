#ifndef XAOD_NTUPLEMAKER_HISTMAKER_H
#define XAOD_NTUPLEMAKER_HISTMAKER_H

/*
  Written by: Matthew Gignac
*/

#include <map>
#include "TH1.h"
#include "TString.h"
#include "TFile.h"

class HistMaker
{

 private:
  bool retrieve(TH1*& hist,TString name); 

 public:

  ///
  /// Default constructor
  ///
  HistMaker();

  ///
  /// Destructor
  ///
  ~HistMaker();

  ///
  /// Associates the histograms to this file
  ///
  void addOutputFile(TFile*& file);

  ///
  /// Creation of 1-D histograms -> create and insert into m_hists
  ///
  void addHist(TString name,int nBins, double min, double max);

  ///
  /// Check if a histogram with this name exists
  ///
  bool checkHistExists(TString name);

  ///
  /// Filling
  ///
  void fill(TString name, double value, double weight=1.0);
  void fill(TString name, TString value, double weight=1.0);

  ///
  void setBinLabel(TString name,int bin,TString label);

 protected:

  TFile* m_file;

  std::map<TString,TH1*> m_hists;

};

#endif
