#ifndef SusySkimMaker_RetrieveInfo_h
#define SusySkimMaker_RetrieveInfo_h

/**
 *  Class to get get a variety of info either directly from the skims,
 *  or using functions that would have filled the skims (depends now on
 *  what input the user is giving);
 */

// RootCore includes
#include "SusySkimMaker/EventObject.h"

// ROOT includes
#include "TIterator.h"
#include "TKey.h"
#include "TString.h"
#include "TSystem.h"
#include "TH1.h"

#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <string>

using namespace std;

class TFile;

class RetrieveInfo
{

 private:
  bool getHist(TString rootFile,TString histName, TH1*& hist);


 public:
  RetrieveInfo();
  ~RetrieveInfo(){};

  void Process();

   // Meta data methods
  const MetaData* getMetaData(TString fileName);

  // Methods to get and format cross sections to output files
  void getXsec(TString process,std::map<int,double> norm,std::vector<TString> samples, EventObject* evt);
  void outputXsec();
 
  // Method to get and compare number of processed events
  void compareNEvents(TString rootFileName);

  // Methods to print cut flows
  std::vector<TH1F*> getCutFlows(TString rootFileName,TString matchingName="cutFlow");
  std::vector<TH1F*> getAddedCutFlows(TString cutFlowFiles);
  std::vector<TH1F*> getAdditionalProcessCutFlows(TString cutFlowFiles, std::vector<TH1F*>& baseVecHists);
  std::vector<std::pair<TString, TString> > parseProcessFile(TString cutFlowProcesses);
  std::map<TString, TString> parseCutNameFile(TString cutFlowCutNames);

  void formatCutflow(TH1F*& hist);
  void formatCutflow(std::vector<TH1F*>& hists,
		     std::vector<TString>& titles,
		     std::map<TString, TString> cutNameSubstitutions);
  void outputCutflows();
  void Add(TH1F*& global, TH1F* local);

  void setXsecDSID(TString xSecDSIDs){m_xSecDSIDs=xSecDSIDs;}
  void setCutFlowFile(TString cutFlowFile){m_cutFlowFile=cutFlowFile;}
  void setCutFlowFiles(TString cutFlowFiles){m_cutFlowFiles=cutFlowFiles;}
  void setCutFlowFormat(TString cutFlowFormat){m_cutFlowFormat=cutFlowFormat;}
  void setCutFlowProcesses(TString cutFlowProcesses){m_cutFlowProcesses=cutFlowProcesses;}
  void setCutFlowCutNames(TString cutFlowCutNames){m_cutFlowCutNames=cutFlowCutNames;}
  void setCutFlowScaleFactor(Float_t cutFlowScaleFactor){m_cutFlowScaleFactor=cutFlowScaleFactor;}
  void setOutputDir(TString outputDirname) {m_outputDirname=outputDirname;}
  void setCutFlowPrefix(TString cutFlowPrefix) {m_cutFlowPrefix=cutFlowPrefix;}

 protected:
  TString m_xSecDSIDs;          // Text file containing a list of datasets or DSIDs
  TString m_cutFlowFile;        // Rootfile name that contains a the cutflow 
  TString m_cutFlowFiles;       // Textfile name containing all rootfiles you wish to process for a cutflow
  TString m_cutFlowFormat;      // Specify output format: "txt" or "tex"
  TString m_cutFlowProcesses;   // Textfile containing names and textfiles the cutflow should contain
  TString m_cutFlowCutNames;    // Textfile containing cut name substitutions for nicer output
  Float_t m_cutFlowScaleFactor; // Scale weighted cut flows by a constant factor (e.g. for luminosity)
  TString m_outputDirname;      // Dir to write output files      
  TString m_cutFlowPrefix;      // Prefix for the cutflow output files

  std::ofstream ofs; // Save Info to outputfile


};

#endif
