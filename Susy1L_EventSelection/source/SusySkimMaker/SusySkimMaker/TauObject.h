#ifndef SusySkimMaker_TauObject_h
#define SusySkimMaker_TauObject_h

// Rootcore
#include "SusySkimMaker/TauVariable.h"
#include "SusySkimMaker/BaseHeader.h"

// xAOD
#include "xAODTau/TauJet.h"
#include "xAODTau/TauJetContainer.h"


namespace TauAnalysisTools {
  class TauSelectionTool;
}

class TreeMaker;
class StatusCode;

typedef std::vector<TauVariable*> TauVector;

class TauObject
{

 private:

  ///
  /// Initialize tools needed for running. Called by init(...) function
  ///
  StatusCode init_tools();

 private:
  TauAnalysisTools::TauSelectionTool*  m_looseTauSelTool;
  TauAnalysisTools::TauSelectionTool*  m_mediumTauSelTool;
  TauAnalysisTools::TauSelectionTool*  m_tightTauSelTool;

 public:

  ///
  /// Default constructor
  ///
  TauObject();

  ///
  /// Destructor
  ///
  virtual ~TauObject();

  ///
  /// Initialization method, user should always call this 
  ///
  StatusCode init(TreeMaker*& treeMaker);

  ///
  /// Sets TauVariable method values
  ///
  void fillTauContainer(xAOD::TauJet* tau_xAOD,const xAOD::VertexContainer* primVertex,std::string sys_name,std::map<TString,float> recoSF);

  /// Returns a TauVector for a systematic sysName. If no object exists,
  /// returns a null pointer
  ///
  const TauVector* getObj(TString sysName);

  /// 
  /// Clear all vectors in m_tauVectorMap
  ///
  void Reset();

 protected:

  ///
  /// Contains all TauVectors for provided systematics
  ///
  std::map<TString,TauVector*>  m_tauVectorMap;

  ///
  /// Conversion factors from MeV into desired units.
  ///
  float                         m_convertFromMeV;

};

#endif
