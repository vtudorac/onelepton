
/*
 * To those editing adding new fields
 * could you please add a short comment 
 * about the field your adding? We should have been
 * doing it in the past, but at least we can start now
 * and maybe eventually add some descriptions for the 
 * already existing ones.
 */


namespace CentralDBFields
{

// Full path to Good Run List XML
const TString GRL                = "Grl";

// Actual mu files for PRW
// At some point this will/should be indexed by 
// run number similar to everything else, so
// its name might change! For now, just testing 
// with MC16D
const TString ACTUALMU           = "ActualMu";

// Lumicalc files to pass to the pileup reweighting tool.
// This is the "mc15-style" configuration, which doesn't
// require us to combine multiple mc campaigns!
const TString LUMICALC           = "LumiCalc";

// Lumicalc files to pass to the pileup reweighting tool.
// This is the "mc16-style" configuration, where we have
// multiple mc campaigns each corresponding to different
// luminosities in data
const TString LUMICALCMC16A      = "LumiCalcMC16A";
const TString LUMICALCMC16CD     = "LumiCalcMC16CD";
const TString LUMICALCMC16E      = "LumiCalcMC16E";
const TString LUMICALCMC21       = "LumiCalcMC21";

const TString PRWGENERAL         = "PRWGeneral";
const TString PRWMC16A           = "PRWMC16A";
const TString PRWMC16CD          = "PRWMC16CD";
const TString PRWMC16E           = "PRWMC16E";
const TString PRWMC21            = "PRWMC21";

const TString ISRUN3           = "isRun3";

//Special PRW files
const TString PRWSpecial_Ztautau_364221_mc16d = "PRWSpecial_Ztautau_364221_mc16d"; 
const TString  PRWSpecial_Znunu_364222_mc16d = "PRWSpecial_Znunu_364222_mc16d"  ; 
const TString  PRWSpecial_Znunu_364223_mc16a = "PRWSpecial_Znunu_364223_mc16a"; 
const TString PRWSpecial_WjetsPTV_364224_mc16e = "PRWSpecial_WjetsPTV_364224_mc16e";
const TString PRWSpecial_QCDdijet_PowerLaw_mc16a = "PRWSpecial_QCDdijet_PowerLaw_mc16a";
const TString PRWSpecial_QCDdijet_Pyth_361027_mc16d = "PRWSpecial_QCDdijet_Pyth_361027_mc16d";
const TString PRWSpecial_QCDdijet_Pyth_361032_mc16d = "PRWSpecial_QCDdijet_Pyth_361032_mc16d";
const TString PRWSpecial_QCDdijet_Pyth8235_800036_mc16a = "PRWSpecial_QCDdijet_Pyth8235_800036_mc16a"; 
const TString PRWSpecial_QCDdijet_Pyth8235_800036_mc16d = "PRWSpecial_QCDdijet_Pyth8235_800036_mc16d"; 
const TString PRWSpecial_QCDdijet_Pyth8235_800036_mc16e = "PRWSpecial_QCDdijet_Pyth8235_800036_mc16e"; 

const TString TAUID              = "Tau.PID"; // FIXME deprecated, can remove?
const TString XSEC               = "CrossSection";
const TString NUMEVT             = "NumEvtPath";
const TString UNITS              = "UnitsFromMEV";
const TString STMSGLEVEL         = "SUSYToolsMsgLevel"; // FIXME deprecated, can remove?
const TString EXCLUDEADDLEP      = "ExcludeAdditionalLeptons"; 
const TString FILTERBASELINE     = "FilterBaseline";
const TString FILTEROR           = "FilterOR";


// When greater than zero, only save events that have 
// the configured number of baseline leptons in the event
const TString SKIMNLEP = "SkimNLeptons";

// When greater than zero, only save events that have
// the configured number of baseline photons in the event
const TString SKIMNPHOTON = "SkimNPhotons";

const TString SAVETRUTHINFO      = "SaveTruthInfo";
const TString REQTRIGDATA        = "RequireTrigInData";
const TString EMULATEBTAG        = "EmulateTruthBtagging";
const TString DISABLEPRW         = "DisablePRW";

// Use SUSYTools pileup reweighting auto-configuration
const TString USEAUTOMAGICPRW    = "UseAutoMagicPRW";

const TString DOTRIGSF           = "DoTriggerSFs";
const TString IGNOREGRL          = "IgnoreGRL";
const TString DOSAMPLEOR         = "DoSampleOR";

// For studying overlap removal between Z+jets and Z+gamma
// samples; this probably needs to be updated since 
// it is dependent on DSIDs which may have since changed
const TString DOSAMPLEORZJETZGAM = "DoSampleORZjetZgam";

const TString DOSAMPLEORWPLVWMQQ = "DoSampleORWplvWmqq";
const TString DXAODKERNEL        = "DxAODKernel";
const TString REQGRLDATA         = "RequireGRL";

// Include photons or taus in the MET building step
const TString USEPHOTONSINMET    = "UsePhotonsInMET";
const TString USETAUSINMET       = "UseTausInMET";

// Use nearby lepton isolation correction for events where
// isolation cones for real leptons overlap
const TString USENEARBYLEPISOCORR = "UseNearbyLepIsoCorr";
const TString NEARBYLEPTYPE       = "NearbyLepType";

const TString SAVETRUTHTRACKS          = "SaveTruthTracks";
const TString SAVETRUTHEVT             = "SaveTruthEvtInfo";
const TString SAVETRUTHEVTFULL         = "SaveTruthEvtInfoFull";
const TString SAVETRUTHPARTICLES       = "SaveTruthParticleInfo";
const TString DRESSTRUTHLEPTONS        = "DressTruthLeptons";

//
// **************************************************** //
// **************** WRITING OUT TRACKS **************** //
// **************************************************** //
//
// Toggles the framework to created a TrackObject
// This might be improved in the future
// The name of the container(s) below must also be set
const TString SAVETRACKS = "SaveTracks";

// Save track links associated to electrons/muons
// Note: this requires the user to have SaveTracks = 1
const TString SAVETRACKLINKS = "SaveTrackLinks";

// Name of the primary track container you 
// want to write out
const TString PRIMARYTRACKCONTANIER = "PrimaryTrackContainer";

// Name of the secondary track container 
// you want to write out. Here secondary 
// refers to tracks that are associated to the 
// primary track container above and are 
// stored as a link. In order to associate the
// secondary tracks to the primary, a vertex 
// name (below) must be configured
const TString SECONDARYTRACKCONTANIER = "SecondaryTrackContainer";

// Name of the vertex container that will
// be used to associate the primary and 
// secondary tracks to each other
// Note the vtx container must save the 
// track links to make the association..
const TString PRISECVTXCONTAINER = "PriSecVtxContainer";

// **************************************************** //

// Paths for GIT migration 
const TString TRIGFILEDIR        = "TriggerFileDir";
const TString SYSFILEDIR         = "SysFileDir";
const TString GROUPSETDIR        = "GroupSetDir";
const TString STCONFIG           = "SusyToolsConfigFile";


const TString WRITEDETAILEDSKIM     = "WriteDetailedSkim";
const TString WRITETRIGGERPRESCALES = "WriteTriggerPrescales";

const TString DISABLETRIGMATCH = "DisableTrigMatch";
// Some derivations perform the trigger matching upstream and do not contain the
// trigger navigation, which requires to use a different type of trigger matching tool
// prefix used in DAOD_PHYS "TrigMatch_" used as default
const TString TRIGGERUPSTREAMMATCHING = "TriggerUpstreamMatching";
const TString TRIGGERMATCHINGPREFIX = "TriggerMatchingPrefix";

//
// Write out branches for PMG truth LHE3 weight variation?
//
const TString WRITELHE3     = "WriteLHE3";
//
// WriteLHE3Flat=on:  write as flat float branches
// WriteLHE3Flat=off: write as vector<float>
//
const TString WRITELHE3FLAT = "WriteLHE3Flat";

//
// DeltaR between truth jets (i.e. clustered truth particles) and all truth bayrons
// When a b-hadron and truth jet are matched within this cone, TruthVariable::bjet is true
//
// Default value 0.4
// 
const TString DRJETBHADRON = "DeltaRTruthJetBHadron";

// Verbosity for some tools which are sometimes too verbose
const TString TRIGMATCHVERBOSITY = "TrigMatchToolVerbosity";
const TString SUSYTOOLSVERBOSITY = "SUSYToolsVerbosity";
const TString PMGTWTOOLSVERBOSITY = "PMGTruthWeightToolVerbosity";

// Add another MET variable, in which the muons are flagged as invisible particles
const TString ADDMETMUONSINVIS = "AddMETWithMuonsInvis";

// Add another MET variable, in which all leptons are flagged as invisible particles
const TString ADDMETLEPTONSINVIS = "AddMETWithLeptonsInvis";

// Add the different MET WPs
const TString ADDMETLOOSEWP = "AddMETLooseWP";
const TString ADDMETTIGHTERWP = "AddMETTighterWP";
const TString ADDMETTENACIOUSWP = "AddMETTenaciousWP";

// Write out the MVA classifed photon vertex
// Hope to obsolete this flag at some point 
// but need to test it works in all derivations first!
const TString WRITEMVAPHOTONVTX = "WriteMVAPhotonVtx";

///
/// Use PMG Xsec Tool; default to true
/// When false switched back to tradiational SUSYTOOLs
/// cross section DB (more configurable..)
///
const TString USEPMGXSECTOOL = "UsePMGXsecTool";

// Turns on/off b-tagging SFs and warnings about missing
// b-tagging information, incase you have a derivation
// that doesn't have b-tagging information stored...
// Note: You might need to still tell SUSYTools to not 
// use b-tagging in your configuration file! This only 
// controls places where b-tagging is used in this framework
// and some calls to ST
const TString DOBTAGGING = "DoBtagging";

// Do you want to retrive MV2c10 and DL1r scores to compare the perfomances?
// The btagging scores will be retrieved and filled if DoBTagScores is toggled on 
// via BTaggingSelectionTools defined in JetObject. 
// IMPORTANT NOTE: These tool instances are separated from what is used in the SUSYTools!
// Please make sure you're feeding consistent btagging container name with the ones in the SUSYTools (either given by the SUSYTools config or the default), and that these containers exist in the DAOD files you are going to run!
//
// Example:
//  DoBTagScores: 1
//  MV2c10ContainerName:        AntiKt4EMPFlowJets_BTagging201810
//  DL1rContainerName:          AntiKt4EMPFlowJets_BTagging201903
//  MV2c10ContainerName_trkJet: AntiKtVR30Rmax4Rmin02TrackJets_BTagging201810
//  DL1rContainerName_trkJet:   AntiKtVR30Rmax4Rmin02TrackJets_BTagging201903
//
// Tagger/Jets with empty container names given will be skipped.
//
const TString DOBTAGSCORES = "DoBTagScores";
const TString MV2C10CONTAINERNAME = "MV2c10ContainerName";
const TString DL1RCONTAINERNAME = "DL1rContainerName";
 const TString DL1DV01CONTAINERNAME = "DL1dv01ContainerName";

const TString MV2C10CONTAINERNAME_TRKJET = "MV2c10ContainerName_trkJet";
const TString DL1RCONTAINERNAME_TRKJET = "DL1rContainerName_trkJet";
 const TString DL1DV01CONTAINERNAME_TRKJET = "DL1dv01ContainerName_trkJet";

// DeltaR(fatjet, trackjet) thershold used in matching track jets to a given fat jet.
// This DR matching is only performed when the ghost-associated track jet collection is unavailable.
const TString DRTHRESH_FATJET_TRACKJETS = "DRThresh_fatJet_trackJets";

// Emulate special jet/met triggers or higgsino triggers?
const TString EMULATEAUXTRIGGERS = "EmulateAuxTriggers";

// Turns on and off saving of tau leptons
// Default is off, since ST will crash 
// if a derivation misses taus...
const TString SAVETAUS = "SaveTaus";

// Use photon?
const TString DOPHOTON = "DoPhoton";

// Use fat jet?
const TString DOFATJET = "DoFatjet";
const TString FATJETCONTAINERNAME = "FatjetContainerName";
const TString TRUTHFATJETCONTAINERNAME = "TruthFatjetContainerName";

// Use track jet?
const TString DOTRACKJET = "DoTrackjet";
const TString TRACKJETCONTAINERNAME = "TrackjetContainerName";

// Use truth jet?
const TString TRUTHJETCONTAINERNAME = "TruthjetContainerName";

// Write out egamma clusters?
// Requires inputs to be saved.
const TString WRITEEGAMMACLUSTERS = "WriteEgammaClusters";

// Write out global trigger SFs
// Defaulted to be on
const TString SAVEGLOBALDILEPTONTRIGSFS = "SaveGlobalDileptonTrigSFs";
const TString SAVEGLOBALPHOTONTRIGSF    = "SaveGlobalPhotonTrigSFs";

// Single-lepton trigger chain name for SF computation
// Do harmonize with Ele.TriggerSFStringSingle in the SUSYTools for the electron one!
const TString SINGLEELETRIGSFCHAINS      = "SingleEleTrigSFChains";
const TString SINGLEMUTRIGSFCHAINS2015   = "SingleMuTrigSFChains2015";
const TString SINGLEMUTRIGSFCHAINS201618 = "SingleMuTrigSFChains201618";

// Turn on auto PRW patch for dsid 366010 to 366035
const TString ENABLEPRWZNUNUFIX = "EnablePRWZnunuFix";

}
