#ifndef SusySkimMaker_ObjectVariable_h
#define SusySkimMaker_ObjectVariable_h

// C++
#include <map>
#include <utility>
#include <iostream>
#include <iomanip> 
#include <string>

// Root
#include "TLorentzVector.h"
#include "TString.h"

/*
 *
 * Common base class to all objects
 * All fundamental quanities to all objects go here
 *
*/


class ObjectVariable : public TLorentzVector
{

 public:
  ObjectVariable();
  virtual ~ObjectVariable() {};

  ObjectVariable(const ObjectVariable&);
  ObjectVariable& operator=(const ObjectVariable&);

  std::map<TString,bool> triggerMap;

  ///
  /// Filled when a truth match is found for a reco particle
  ///
  TLorentzVector truthTLV;

  // Barcode of a truth particle that has been
  // associated to this class
  int barcode;

  bool trigMatchResult(const TString trigChain); //!
  void setDefault( float& var, float def); //!
  void setDefault( bool& var, bool def); //!
  void setDefault( int& var, int def); //!
  void setDefault( uint8_t& var, uint8_t def); //!


  ClassDef(ObjectVariable, 1);

};


#endif
