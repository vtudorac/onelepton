
#ifndef SusySkimMaker_ElectronObject_h
#define SusySkimMaker_ElectronObject_h

// Rootcore
#include "SusySkimMaker/ElectronVariable.h"
#include "SusySkimMaker/BaseHeader.h"
#include "SusySkimMaker/BaseObject.h"
#include "SusySkimMaker/TrackObject.h"

// xAOD
#include "xAODEgamma/Electron.h"
#include "xAODEgamma/ElectronContainer.h"
#include "ElectronPhotonSelectorTools/AsgElectronLikelihoodTool.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODEventInfo/EventInfo.h"

typedef std::vector<ElectronVariable*> ElectronVector;

class TreeMaker;

class ElectronObject
{

 private:

  ///
  /// Initialize all tools needed for this class
  ///
  StatusCode init_tools();

 public:

  ///
  /// Default constructor
  ///
  ElectronObject();

  ///
  /// Destructor
  ///
  virtual ~ElectronObject();


  ///
  /// Initialization method for this class, connects ElectronVectors up to
  /// TTrees found in TreeMaker
  ///
  StatusCode init(TreeMaker*& treeMaker, bool isData, bool isPhyslite);

  ///
  /// Fill ElectronVariable memebers, i.e. the variables which are written into the skims
  /// This is done for a systematic 'sys_name' and is found in m_electronVectorMap. 
  ///
  void fillElectronContainer(xAOD::Electron* ele_xAOD, 
			     const xAOD::VertexContainer* primVertex,
                             const xAOD::EventInfo* eventInfo,
			     const std::map<TString,bool> trigMap,
			     const std::map<TString,float> recoEffSF,
                             const MCCorrContainer* trigEff, 
			     std::string sys_name);


  ///
  /// Fill into ElectronVariable more maniac ones: i.e. input variables to PromptLeptonTagger etc
  ///
  void fillDetailedElectronVariables(xAOD::Electron* ele_xAOD, ElectronVariable*& ele);


  ///
  /// For filliing track links
  ///
  void setTrackObject(TrackObject* trackObject){ m_trackObject=trackObject; }

  ///
  /// Similiar method to create electron object from truth information
  ///
  void fillElectronContainerTruth(xAOD::TruthParticle* ele_xAOD, std::string sys_name="");

  ///
  /// Similiar method to modify ElectronVariable with NearbyLepIsoCorr values
  ///
  void fillElectronContainer_NearbyLepIsoCorr(xAOD::Electron* ele_xAOD,
                                              unsigned int index_baseline_el,
                                              std::string sys_name);

  ///
  /// Returns an ElectronVector corresponding to the systematic sysName.
  /// In the event it does not exist, returns a null pointer
  ///
  const ElectronVector* getObj(TString sysName);

  ///
  /// Clears all ElectronVectors inside m_electronVectorMap
  ///
  void Reset();


 protected:

  ///
  /// Global std::map used to store ElectronVectors for all provided systematics
  ///
  std::map<TString,ElectronVector*> m_electronVectorMap;

  ///
  /// Conversion factors to convert from MeV into desired units
  ///
  float                             m_convertFromMeV;

  ///
  /// Electron selector tools
  ///
  AsgElectronLikelihoodTool *m_elec_llhveryloose;
  AsgElectronLikelihoodTool *m_elec_llhloose;
  AsgElectronLikelihoodTool *m_elec_llhlooseBL;
  AsgElectronLikelihoodTool *m_elec_llhmedium;
  AsgElectronLikelihoodTool *m_elec_llhtight;

  ///
  /// Is data?
  ///
  bool m_isData;
  bool m_isPhyslite;

  ///
  /// Interface to isolation tools and in principle anything else common to leptons
  ///
  BaseObject* m_baseObject;

  ///
  /// TrackObject tol
  ///
  TrackObject* m_trackObject;

  ///
  /// Write out additional variables, typically only for specific studies
  /// so turned off by default. Can be steering by deep.conf
  ///
  bool m_writeDetailedSkim;

};

#endif
