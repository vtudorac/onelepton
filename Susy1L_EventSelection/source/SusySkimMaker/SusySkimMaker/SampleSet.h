#ifndef SusySkimMaker_SampleSet_h
#define SusySkimMaker_SampleSet_h

// ROOT includes
#include "TObject.h"
#include "TString.h"
#include "TH1F.h"
#include "TChain.h"

// C++ includes
#include <vector>

//
// This class will replace SampleConfig eventually
// but right now is merely being used as an easy way
// to sample the input samples used for the merge in SH
//

class SampleSet : public TObject
{

 public:

   ///
   /// Constructor
   /// 
   SampleSet();
 
   ///
   /// Destructor
   ///
   virtual ~SampleSet();

   // 
   void addMergeInput(TString sample){ m_mergeInputs.push_back(sample); }
   std::vector<TString> getMergeInputs(){ return m_mergeInputs; }

   ///
   /// TChain all input trees
   ///
   std::vector<TChain*> getTChainList(TString treeName="");

   ///
   /// Sum totgether all histograms found with histName
   /// that are in the mergeInputs list
   ///
   TH1F* getTotalHist(TString histName);

   ///
   /// Get a list of trees, exlcuding any which have 
   /// the CBK or MetaData names
   ///
   std::vector<TString> getListOfTrees();

   /// Misc
   void printMergeInputs();
   void printSampleInfo();
   void write(TString directory); 

   ///
   /// Give this class a name
   /// Recommended, since it will be used in the write
   ///
   void setName(TString name){ m_name=name; }

 private:
   //
   std::vector<TString> m_mergeInputs; 

   //
   TString m_name;

 public:
   // Fields about this set of samples
   bool isData;
   bool isTruth;
   bool isAF2;
   double lumi;
   TString baseDir;
   TString groupSet;
   TString tag;
   TString process;
   TString deepConfig;
   std::map<TString, int> nEvtAMIMap;

 public:
   //
   ClassDef(SampleSet,2);

};

#endif
