#ifndef SusySkimMaker_STATUSCODECHECK_H
#define SusySkimMaker_STATUSCODECHECK_H

#include "AsgMessaging/StatusCode.h"


#define CHECK( ARG )                                           \
   do {                                                        \
      const StatusCode sc__ = ARG;                             \
      const char* APP_NAME = "StatusCodeCheck.h";              \
      if( ! sc__.isSuccess() ) {                               \
         ::Error( APP_NAME, "Failed to execute: \"%s\"",       \
                  #ARG );                                      \
      }                                                        \
   } while( 0 )

#endif




