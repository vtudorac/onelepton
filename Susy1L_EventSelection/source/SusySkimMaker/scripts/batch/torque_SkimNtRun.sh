#!/bin/bash

#PBS -N TestJob
#PBS -m n 
#PBS -l walltime=02:00:00

echo Running on host `hostname`
echo Time is `date`
echo Directory is `pwd`
echo This jobs runs on the following processors:
echo `cat $PBS_NODEFILE`

echo "Selector: " $selector
echo "Input:    " $input
echo "Output:   " $output

run_SkimNtRun -F $input -s $output -selector $selector

