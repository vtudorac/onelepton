#include "SusySkimMaker/LeptonVariable.h"

// -------------------------------------------------------------------------------- //
LeptonVariable::LeptonVariable() : truthLink(0),
                                   trkLink(0)
{

  setDefault(q,-99);
  setDefault(author,-1);
  setDefault(ptcone20,-1.0);
  setDefault(ptcone30,-1.0);
  setDefault(ptcone40,-1.0);
  setDefault(topoetcone20,-1.0);
  setDefault(topoetcone30,-1.0);
  setDefault(topoetcone40,-1.0);
  setDefault(ptvarcone20,-1.0);
  setDefault(ptvarcone30,-1.0);
  setDefault(ptvarcone40,-1.0);
  setDefault(ptvarcone30_TightTTVA_pt1000,-1.0);
  setDefault(ptvarcone30_TightTTVA_pt500,-1.0); 
  setDefault(ptvarcone20_TightTTVA_pt1000,-1.0);
  setDefault(ptvarcone20_TightTTVA_pt500,-1.0);
  setDefault(ptcone20_TightTTVALooseCone_pt500, -1.0); 
  setDefault(ptcone20_TightTTVALooseCone_pt1000, -1.0); 
  setDefault(ptcone20_TightTTVA_pt500, -1.0); 
  setDefault(ptcone20_TightTTVA_pt1000, -1.0); 
  setDefault(neflowisol20,-1.0);
  setDefault(ptvarcone30_Prompt_D0SigTTVALooseCone_pt1000,-1.0);

  //Old WPs
  setDefault(IsoFCHighPtCaloOnly,false); 
  setDefault(IsoHighPtCaloOnly,false); 
  setDefault(IsoHighPtTrackOnly,false);
  setDefault(IsoTightTrackOnly_VarRad,false);
  setDefault(IsoTightTrackOnly_FixedRad,false);
  setDefault(IsoLoose_VarRad,false);
  setDefault(IsoLoose_FixedRad,false);
  setDefault(IsoTight_VarRad,false);
  setDefault(IsoTight_FixedRad,false);     
  setDefault(IsoPflowLoose_VarRad,false);  
  setDefault(IsoPflowLoose_FixedRad,false);
  setDefault(IsoPflowTight_VarRad,false);  
  setDefault(IsoPflowTight_FixedRad,false);
  setDefault(IsoPLVLoose,false);  
  setDefault(IsoPLVTight,false);  
  setDefault(corr_ptcone20,-1.0);
  setDefault(corr_ptcone30,-1.0);
  setDefault(corr_ptcone40,-1.0);
  setDefault(corr_topoetcone20,-1.0);
  setDefault(corr_topoetcone30,-1.0);
  setDefault(corr_topoetcone40,-1.0);
  setDefault(corr_ptvarcone20,-1.0);
  setDefault(corr_ptvarcone30,-1.0);
  setDefault(corr_ptvarcone40,-1.0);
  setDefault(corr_neflowisol20,-1.0);
  
  //Old WPs 
  /*
  setDefault(corr_IsoFCHighPtCaloOnly,false); 
  setDefault(corr_IsoFCLoose,false); 
  setDefault(corr_IsoHighPtCaloOnly,false); 
  */  

  setDefault(corr_IsoHighPtCaloOnly,false); 
  setDefault(corr_IsoHighPtTrackOnly,false);
  setDefault(corr_IsoTightTrackOnly_VarRad,false);
  setDefault(corr_IsoTightTrackOnly_FixedRad,false);
  setDefault(corr_IsoLoose_VarRad,false);
  setDefault(corr_IsoLoose_FixedRad,false);
  setDefault(corr_IsoTight_VarRad,false);
  setDefault(corr_IsoTight_FixedRad,false);     
  setDefault(corr_IsoPflowLoose_VarRad,false);  
  setDefault(corr_IsoPflowLoose_FixedRad,false);
  setDefault(corr_IsoPflowTight_VarRad,false);  
  setDefault(corr_IsoPflowTight_FixedRad,false);
  setDefault(type,-1);
  setDefault(origin,-1);
  setDefault(iff_type,-1);
  setDefault(d0,-1.0);
  setDefault(z0,-1.0);
  setDefault(d0Err,-1.0);
  setDefault(z0Err,-1.0);
  setDefault(pTErr,-1.0);
  setDefault(signal,false);
  setDefault(passOR,false);
  setDefault(baseline,false);
  //setDefault(baseline_EWKcomb,false);
  setDefault(nPixHits,0);
  setDefault(nIBLHits,0);
  setDefault(truthQ,-99);
  setDefault(plt_input_TrackJetNTrack,0);
  setDefault(plt_input_DRlj,0.0);
  setDefault(plt_input_rnnip,0.0);
  setDefault(plt_input_DL1mu,0.0);
  setDefault(plt_input_PtRel,0.0);
  setDefault(plt_input_PtFrac,0.0);
  setDefault(plt_input_Topoetcone30Rel,0.0);
  setDefault(plt_input_Ptvarcone30Rel,0.0);
  setDefault(promptLepVeto_score,0.0);
  setDefault(LowPtPLV_score,0.0);
}
// -------------------------------------------------------------------------------- //
LeptonVariable::LeptonVariable(const LeptonVariable &rhs):
  ObjectVariable(rhs),
  q(rhs.q),
  author(rhs.author),
  ptcone20(rhs.ptcone20),
  ptcone30(rhs.ptcone30),
  ptcone40(rhs.ptcone40),
  topoetcone20(rhs.topoetcone20),
  topoetcone30(rhs.topoetcone30),
  topoetcone40(rhs.topoetcone40),
  ptvarcone20(rhs.ptvarcone20),
  ptvarcone30(rhs.ptvarcone30),
  ptvarcone40(rhs.ptvarcone40),
  ptvarcone30_TightTTVA_pt1000(rhs.ptvarcone30_TightTTVA_pt1000),
  ptvarcone30_TightTTVA_pt500(rhs.ptvarcone30_TightTTVA_pt500),
  ptvarcone20_TightTTVA_pt1000(rhs.ptvarcone20_TightTTVA_pt1000),
  ptvarcone20_TightTTVA_pt500(rhs.ptvarcone20_TightTTVA_pt500),
  ptcone20_TightTTVALooseCone_pt500(rhs.ptcone20_TightTTVALooseCone_pt500),
  ptcone20_TightTTVALooseCone_pt1000(rhs.ptcone20_TightTTVALooseCone_pt1000),
  ptcone20_TightTTVA_pt500(rhs.ptcone20_TightTTVA_pt500),
  ptcone20_TightTTVA_pt1000(rhs.ptcone20_TightTTVA_pt1000),
  neflowisol20(rhs.neflowisol20),
  ptvarcone30_Prompt_D0SigTTVALooseCone_pt1000(rhs.ptvarcone30_Prompt_D0SigTTVALooseCone_pt1000),
  
  //Old WPs
  IsoFCHighPtCaloOnly(rhs.IsoFCHighPtCaloOnly), 
  IsoFCTightTrackOnly(rhs.IsoFCTightTrackOnly),
  IsoFCLoose(rhs.IsoFCLoose),
  IsoFCTight(rhs.IsoFCTight),
  IsoFCLoose_FixedRad(rhs.IsoFCLoose_FixedRad),
  IsoFCTight_FixedRad(rhs.IsoFCTight_FixedRad),
  
  IsoHighPtCaloOnly(rhs.IsoHighPtCaloOnly), 
  IsoHighPtTrackOnly(rhs.IsoHighPtTrackOnly),
  IsoTightTrackOnly_VarRad(rhs.IsoTightTrackOnly_VarRad),
  IsoTightTrackOnly_FixedRad(rhs.IsoTightTrackOnly_FixedRad),
  IsoLoose_VarRad(rhs.IsoLoose_VarRad),
  IsoLoose_FixedRad(rhs.IsoLoose_FixedRad),
  IsoTight_VarRad(rhs.IsoTight_VarRad),
  IsoTight_FixedRad(rhs.IsoTight_FixedRad),
  IsoPflowLoose_VarRad(rhs.IsoPflowLoose_VarRad),  
  IsoPflowLoose_FixedRad(rhs.IsoPflowLoose_FixedRad),
  IsoPflowTight_VarRad(rhs.IsoPflowTight_VarRad),
  IsoPflowTight_FixedRad(rhs.IsoPflowTight_FixedRad),
  IsoPLVLoose(rhs.IsoPLVLoose),
  IsoPLVTight(rhs.IsoPLVTight),  
  corr_ptcone20(rhs.corr_ptcone20),
  corr_ptcone30(rhs.corr_ptcone30),
  corr_ptcone40(rhs.corr_ptcone40),
  corr_neflowisol20(rhs.corr_neflowisol20),
  corr_topoetcone20(rhs.corr_topoetcone20),
  corr_topoetcone30(rhs.corr_topoetcone30),
  corr_topoetcone40(rhs.corr_topoetcone40),
  corr_ptvarcone20(rhs.corr_ptvarcone20),
  corr_ptvarcone30(rhs.corr_ptvarcone30),
  corr_ptvarcone40(rhs.corr_ptvarcone40),
  corr_IsoHighPtCaloOnly(rhs.corr_IsoHighPtCaloOnly), 
  corr_IsoHighPtTrackOnly(rhs.corr_IsoHighPtTrackOnly),
  corr_IsoTightTrackOnly_VarRad(rhs.corr_IsoTightTrackOnly_VarRad),
  corr_IsoTightTrackOnly_FixedRad(rhs.corr_IsoTightTrackOnly_FixedRad),
  corr_IsoLoose_VarRad(rhs.corr_IsoLoose_VarRad),
  corr_IsoLoose_FixedRad(rhs.corr_IsoLoose_FixedRad),
  corr_IsoTight_VarRad(rhs.corr_IsoTight_VarRad),
  corr_IsoTight_FixedRad(rhs.corr_IsoTight_FixedRad),
  corr_IsoPflowLoose_VarRad(rhs.corr_IsoPflowLoose_VarRad),  
  corr_IsoPflowLoose_FixedRad(rhs.corr_IsoPflowLoose_FixedRad),
  corr_IsoPflowTight_VarRad(rhs.corr_IsoPflowTight_VarRad),  
  corr_IsoPflowTight_FixedRad(rhs.corr_IsoPflowTight_FixedRad),
  trackTLV(rhs.trackTLV),
  type(rhs.type),
  origin(rhs.origin),
  iff_type(rhs.iff_type),
  d0(rhs.d0),
  z0(rhs.z0),
  d0Err(rhs.d0Err),
  z0Err(rhs.z0Err),
  pTErr(rhs.pTErr),
  signal(rhs.signal),
  passOR(rhs.passOR),
  baseline(rhs.baseline),
//baseline_EWKcomb(rhs.baseline_EWKcomb),
  nPixHits(rhs.nPixHits),
  nIBLHits(rhs.nIBLHits),
  truthQ(rhs.truthQ),
  recoSF(rhs.recoSF),
  trigEff(rhs.trigEff),
  truthLink(rhs.truthLink),
  trkLink(rhs.trkLink),
  plt_input_TrackJetNTrack(rhs.plt_input_TrackJetNTrack),
  plt_input_DRlj(rhs.plt_input_DRlj),
  plt_input_rnnip(rhs.plt_input_rnnip),
  plt_input_DL1mu(rhs.plt_input_DL1mu),
  plt_input_PtRel(rhs.plt_input_PtRel),
  plt_input_PtFrac(rhs.plt_input_PtFrac),
  plt_input_Topoetcone30Rel(rhs.plt_input_Topoetcone30Rel),
  plt_input_Ptvarcone30Rel(rhs.plt_input_Ptvarcone30Rel),
  promptLepVeto_score(rhs.promptLepVeto_score),
  LowPtPLV_score(rhs.LowPtPLV_score)
{

}
// -------------------------------------------------------------------------------- //
LeptonVariable& LeptonVariable::operator=(const LeptonVariable &rhs)
{

  if (this != &rhs) {
    ObjectVariable::operator=(rhs);
    q           = rhs.q;
    author      = rhs.author;
    ptcone20    = rhs.ptcone20;
    ptcone30    = rhs.ptcone30;
    ptcone40    = rhs.ptcone40;
    topoetcone20 = rhs.topoetcone20;
    topoetcone30 = rhs.topoetcone30;
    topoetcone40 = rhs.topoetcone40;
    ptvarcone20  = rhs.ptvarcone20;
    ptvarcone30  = rhs.ptvarcone30;
    ptvarcone40  = rhs.ptvarcone40;
    ptvarcone30_TightTTVA_pt1000  = rhs.ptvarcone30_TightTTVA_pt1000;
    ptvarcone30_TightTTVA_pt500  = rhs.ptvarcone30_TightTTVA_pt500;
    ptvarcone20_TightTTVA_pt1000  = rhs.ptvarcone20_TightTTVA_pt1000;
    ptvarcone20_TightTTVA_pt500  = rhs.ptvarcone20_TightTTVA_pt500;
    ptcone20_TightTTVALooseCone_pt500 = rhs.ptcone20_TightTTVALooseCone_pt500;
    ptcone20_TightTTVALooseCone_pt1000 = rhs.ptcone20_TightTTVALooseCone_pt1000;
    ptcone20_TightTTVA_pt500 = rhs.ptcone20_TightTTVA_pt500;
    ptcone20_TightTTVA_pt1000 = rhs.ptcone20_TightTTVA_pt1000;
    neflowisol20  = rhs.neflowisol20;
    ptvarcone30_Prompt_D0SigTTVALooseCone_pt1000 = rhs.ptvarcone30_Prompt_D0SigTTVALooseCone_pt1000;
    
    //Old WPs
    IsoFCHighPtCaloOnly          = rhs.IsoFCHighPtCaloOnly; 
    IsoFCLoose                   = rhs.IsoFCLoose; 
    IsoFCTightTrackOnly          = rhs.IsoFCTightTrackOnly;
    IsoFCLoose                   = rhs.IsoFCLoose;
    IsoFCTight                   = rhs.IsoFCTight; 
    IsoFCLoose_FixedRad          = rhs.IsoFCLoose_FixedRad;
    IsoFCTight_FixedRad          = rhs.IsoFCTight_FixedRad;

    
    IsoHighPtCaloOnly          = rhs.IsoHighPtCaloOnly; 
    IsoHighPtTrackOnly         = rhs.IsoHighPtTrackOnly;
    IsoTightTrackOnly_VarRad   = rhs.IsoTightTrackOnly_VarRad;
    IsoTightTrackOnly_FixedRad = rhs.IsoTightTrackOnly_FixedRad;
    IsoLoose_VarRad            = rhs.IsoLoose_VarRad;
    IsoLoose_FixedRad          = rhs.IsoLoose_FixedRad;
    IsoTight_VarRad            = rhs.IsoTight_VarRad;
    IsoTight_FixedRad          = rhs.IsoTight_FixedRad;
    IsoPflowLoose_VarRad       = rhs.IsoPflowLoose_VarRad;  
    IsoPflowLoose_FixedRad     = rhs.IsoPflowLoose_FixedRad;
    IsoPflowTight_VarRad       = rhs.IsoPflowTight_VarRad;  
    IsoPflowTight_FixedRad     = rhs.IsoPflowTight_FixedRad;
    IsoPLVLoose                = rhs.IsoPLVLoose;
    IsoPLVTight                = rhs.IsoPLVTight;
    corr_ptcone20    = rhs.corr_ptcone20;
    corr_ptcone30    = rhs.corr_ptcone30;
    corr_ptcone40    = rhs.corr_ptcone40;
    corr_topoetcone20 = rhs.corr_topoetcone20;
    corr_topoetcone30 = rhs.corr_topoetcone30;
    corr_topoetcone40 = rhs.corr_topoetcone40;
    corr_ptvarcone20  = rhs.corr_ptvarcone20;
    corr_ptvarcone30  = rhs.corr_ptvarcone30;
    corr_ptvarcone40  = rhs.corr_ptvarcone40;
    corr_neflowisol20 = rhs.corr_neflowisol20;
    
    //Old WPs
    corr_IsoFCHighPtCaloOnly          = rhs.corr_IsoFCHighPtCaloOnly; 
    corr_IsoFCTightTrackOnly = rhs.corr_IsoFCTightTrackOnly;
    corr_IsoFCLoose = rhs.corr_IsoFCLoose;
    corr_IsoFCTight = rhs.corr_IsoFCTight; 
    corr_IsoFCLoose_FixedRad = rhs.corr_IsoFCLoose_FixedRad;
    corr_IsoFCTight_FixedRad = rhs.corr_IsoFCTight_FixedRad;

    
    corr_IsoHighPtCaloOnly          = rhs.corr_IsoHighPtCaloOnly; 
    corr_IsoHighPtTrackOnly         = rhs.corr_IsoHighPtTrackOnly;
    corr_IsoTightTrackOnly_VarRad   = rhs.corr_IsoTightTrackOnly_VarRad;
    corr_IsoTightTrackOnly_FixedRad = rhs.corr_IsoTightTrackOnly_FixedRad;
    corr_IsoLoose_VarRad            = rhs.corr_IsoLoose_VarRad;
    corr_IsoLoose_FixedRad          = rhs.corr_IsoLoose_FixedRad;
    corr_IsoTight_VarRad            = rhs.corr_IsoTight_VarRad;
    corr_IsoTight_FixedRad          = rhs.corr_IsoTight_FixedRad;
    corr_IsoPflowLoose_VarRad       = rhs.corr_IsoPflowLoose_VarRad;  
    corr_IsoPflowLoose_FixedRad     = rhs.corr_IsoPflowLoose_FixedRad;
    corr_IsoPflowTight_VarRad       = rhs.corr_IsoPflowTight_VarRad;  
    corr_IsoPflowTight_FixedRad     = rhs.corr_IsoPflowTight_FixedRad;
    trackTLV    = rhs.trackTLV;
    type        = rhs.type;
    origin      = rhs.origin;
    iff_type    = rhs.iff_type;
    d0          = rhs.d0;
    z0          = rhs.z0;
    d0Err       = rhs.d0Err;
    z0Err       = rhs.z0Err;
    pTErr       = rhs.pTErr;
    signal      = rhs.signal;
    passOR      = rhs.passOR;
    baseline    = rhs.baseline;
    //baseline_EWKcomb    = rhs.baseline_EWKcomb;
    nPixHits    = rhs.nPixHits;
    nIBLHits    = rhs.nIBLHits;
    truthQ      = rhs.truthQ;
    recoSF      = rhs.recoSF;
    trigEff     = rhs.trigEff;
    truthLink   = rhs.truthLink;
    trkLink     = rhs.trkLink;
    plt_input_TrackJetNTrack = rhs.plt_input_TrackJetNTrack;
    plt_input_DRlj           = rhs.plt_input_DRlj;
    plt_input_rnnip          = rhs.plt_input_rnnip;
    plt_input_DL1mu          = rhs.plt_input_DL1mu;
    plt_input_PtRel          = rhs.plt_input_PtRel;
    plt_input_PtFrac         = rhs.plt_input_PtFrac;
    plt_input_Topoetcone30Rel= rhs.plt_input_Topoetcone30Rel;
    plt_input_Ptvarcone30Rel = rhs.plt_input_Ptvarcone30Rel;
    promptLepVeto_score      = rhs.promptLepVeto_score;    
    LowPtPLV_score           = rhs.LowPtPLV_score;    
  }

  return *this;

}
// -------------------------------------------------------------------------------- //
float LeptonVariable::getRecoSF(const TString sys)
{

  auto it = this->recoSF.find(sys);
  
  if(it == this->recoSF.end()){

    auto nom = this->recoSF.find("");

    if( nom == this->recoSF.end() ){
      std::cout << " <LeptonVariable::getRecoSF> WARNING Request to reconstruction SF, but it doesn't exist for systematic: " << sys << " or nominal" << std::endl;
      return 1.0;
    }  
    else{
      return nom->second;
    }

  }
  else{
    return it->second;
  }

  

}
// -------------------------------------------------------------------------------- //
double LeptonVariable::getTrigEffMC(const TString instance, const TString sys)
{
  double effMC=1., effData=1.;
  this->trigEff.getEff(instance,sys,effMC,effData);

  return effMC; 
}
// -------------------------------------------------------------------------------- //
double LeptonVariable::getTrigEffData(const TString instance, const TString sys)
{
  double effMC=1., effData=1.;
  this->trigEff.getEff(instance,sys,effMC,effData);

  return effData; 
}
// -------------------------------------------------------------------------------- //
double LeptonVariable::getTrigSF(const TString instance, const TString sys)
{
  double sf=1.;
  this->trigEff.getSF(instance,sys,sf);

  return sf; 
}
// -------------------------------------------------------------------------------- //
bool LeptonVariable::hasTruthLink()
{

  if( this->truthLink ) return true;
  else                  return false;

}


