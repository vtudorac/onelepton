#include "SusySkimMaker/RetrieveInfo.h"
#include "SusySkimMaker/SampleHandler.h"
#include "SusySkimMaker/Systematics.h"
#include "SampleHandler/SampleHandler.h"
#include "SampleHandler/fetch.h"
#include "SampleHandler/DiskListLocal.h"
#include "SampleHandler/ToolsDiscovery.h"
#include "SampleHandler/DiskListEOS.h"
#include "SampleHandler/SampleLocal.h"
#include "SusySkimMaker/ObjectTools.h"
#include "SusySkimMaker/MergeTool.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/ConfigMgr.h"
#include "SampleHandler/ScanDir.h"
#include "SusySkimMaker/SampleSet.h"
#include "PathResolver/PathResolver.h"
#include "AsgMessaging/StatusCode.h"
#include "TObjString.h"
#include "TObjArray.h"
#include "TObject.h"
#include "TKey.h"
#include <dirent.h>
#include <string.h>
#include <stdexcept>
#include <thread>
#include <sstream>
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>
#include <boost/range/iterator_range.hpp>

SampleHandler::SampleHandler() : m_baseDir(""),
				 m_treeDir(""),
				 m_localTreeDir(""),
				 m_skimDir(""),
				 m_mergeDir(""),
				 m_sampleInfoDir(""),
                                 m_prwDir(""),
                                 m_pTag(""),
				 m_groupName(""),
				 m_tag(""),
                                 m_groupSetDir(""),
				 m_includeFiles(""),
				 m_excludeFiles(""),
				 m_batchStringfilter(""),
				 m_disableMetaDataCheck(false),
				 m_disablePRWCheck(false),
				 m_disableSkims(false),
				 m_disableTrees(false),
				 m_noSubmit(false),
				 m_lumi(1.0),
				 m_maxQueuedJobs(-1),
				 m_maxEventsPerJob(1000000),
				 m_sumOfWeightsPath(0),
				 m_xsecPath(0),
				 m_eventObject(0),
				 m_objectTools(0),
                                 m_DEC(0),
				 m_configured(0),
				 m_bulkSubmission(false),
				 m_skipSampleInfoDump(false),
				 m_mergeByProcessName(false),
				 m_resubmitMergeJobs(false),
                                 m_resubmitBatchJobs(false),
				 m_priority(-1),
				 m_splitLargeFiles(false),
				 m_mergeOutputs(true),
				 m_jobCounter(0)
{

  m_priorityTags.clear();

}
// ---------------------------------------------------------------------- //
bool SampleHandler::initialize(TString groupSet,TString tag, TString prefix)
{

  if( m_configured >= 1 ) return true;

  // Check user supplied groupSet and tag, or extact from env variables
  setVariable(groupSet,"GROUP_SET_NAME","Required to configure with a group-set name!");
  setVariable(tag,"GROUP_SET_TAG","Required to configure with a tag!");

  //
  CentralDB::retrieve(CentralDBFields::GROUPSETDIR,m_groupSetDir);

  // Load into queue the sets required for this group set
  
  defineSets(groupSet);
  
  m_groupName = groupSet;
  m_tag       = tag;
  m_prefix 	  = prefix;

  // Initialize SUSY xsec tool
  m_eventObject = new EventObject();
  CHECK(m_eventObject->init_tools(false,false)); // afs and data flags not used anyhow
  
  // Initialize tools, for NNLO weights  
  m_objectTools = new ObjectTools();
  
  // Successful!
  m_configured = 1;

  return true;

}
// ---------------------------------------------------------------------- //
bool SampleHandler::initialize(TString user_path,TString tag,TString groupSet, TString prefix)
{

  // Already configured
  if( m_configured >= 2 ) return true;

  // Check user_path or extract it from env variable
  setVariable(user_path,"GROUP_SET_DIR","Required to configure with a directory to store output!");
  initialize(groupSet,tag, prefix);

  // Build proper directories
  makeDirectoryStructure(user_path,m_tag,m_groupName);

  // Successful!
  m_configured = 2;

  return true;

}
// ---------------------------------------------------------------------- //
void SampleHandler::setVariable(TString& field, TString env, TString errorMsg)
{

  // User supplied configuration always taken priority
  if( field != "" ) return;

  // Check if the user configured environment variables
  TString auto_field = TString( gSystem->Getenv( env.Data()) );

  if( auto_field=="" ){
    MsgLog::ERROR("SampleHandler::setVariable",errorMsg);
    abort();
  }
  else{
    MsgLog::INFO("SampleHandler::setVariable","Auto-detected %s and set to %s",env.Data(),auto_field.Data() );
    field = auto_field;
  }

}
// ---------------------------------------------------------------------- //
void SampleHandler::defineSets(TString groupSet)
{

  TString path = getPath(groupSet,m_groupSetDir,m_groupSetDir);
  MsgLog::INFO("SampleHandler::defineSets","Reading group set %s from directory %s ...",groupSet.Data(),path.Data() );

  //
  SampleInfo* sampleinfo = readGroupSetConfig( path + "/groupset.config");

  // Require the header
  if( !sampleinfo ){
    MsgLog::WARNING("SampleHandler::defineSets","Could not create a SampleInfo object for (now ignored) groupSet %s",groupSet.Data() );
    return;
  }

  //
  sampleinfo->groupName  = groupSet;
  addSamples(sampleinfo,groupSet,path);
  addGroup(sampleinfo);

}
// ---------------------------------------------------------------------- //
SampleHandler::SampleInfo* SampleHandler::readGroupSetConfig(TString fileName)
{

  SampleInfo* sampleinfo = 0;

  // Open file
  std::ifstream file ( fileName.Data() );

  if( !file.is_open() ){
    Error("SampleHandler::readGroupSetConfig","Cannot open file %s ",fileName.Data() );
    return sampleinfo;
  }

  //
  sampleinfo = new SampleInfo();

  //
  TString line;
  int sysSet     = 0;
  int writeTrees = -1;
  int writeSkims = -1;
  TString sysSetFile = "";

  while( file ){
    line.ReadLine(file, kTRUE);

    if( line.IsWhitespace() || line.Contains("#") ) continue;

    // Remove whitespace
    line.ReplaceAll(" ","");

    TString field = "";
    TString value = "";

    if( line.Contains(":") && line.Tokenize(":")->GetEntries()>=2 ){
      field = ((TObjString*)line.Tokenize(":")->At(0))->String();
      value = ((TObjString*)line.Tokenize(":")->At(1))->String();
    }

    // SUSYTools configuration file
    if( field.Contains("SUSYTOOLS") ){
      MsgLog::WARNING("SampleHandler::readGroupSetConfig","Detected a SUSYTools configuration file in your groupSet configuration. NOTE THIS IS DEPRECIATED AND WILL BE IGNORED!!!!!");
    }

    // Deep configuration file
    if( field.Contains("DEEPCONFIG") ){
      MsgLog::INFO("SampleHandler::readGroupSetConfig","Adding a deep configuration file %s",value.Data() );
      sampleinfo->deepConfigFile = value;
    }

    // This class is setup to split the jobs into an
    // arbitrary number of jobs split by systematic sets,
    // therefore we need to read sets of configurations

    // 1 == about to read in a systematic set, wait for a "{"
    if( line.Contains("SYSTEMATIC_SET") ){
      sysSet = 1;
    }

    // 2 == we found a SYSTEMATIC_SET field and finally its "{"
    if( sysSet==1 && line.Contains("{") ){
      sysSet = 2;
    }

    if( sysSet==2 ){

      // Turn off systematic set scan
      // and add a systematic set info SampleInfo
      if( line.Contains("}") ){

	sysSet = 0;

	if( writeTrees<0 || writeSkims<0 || sysSetFile=="" ){
	  MsgLog::ERROR("SampleHandler::readGroupSetConfig","Incomplete systematic set from group set configuration file, please provide a complete configuration!");
	  continue;
	}

	MsgLog::INFO("SampleHandler::readGroupSetConfig","Adding a systematic set %s with writeTrees %i and writeSkims %i",sysSetFile.Data(),writeTrees,writeSkims);
	sampleinfo->addSysSet(sysSetFile,writeSkims,writeTrees);

	// Return defaults
	writeTrees = -1;
	writeSkims = -1;
	sysSetFile = "";
	continue;

      }

      if( field.Contains("NAME")  ) sysSetFile = value;
      if( field.Contains("TREES") ) writeTrees = value.Atoi();
      if( field.Contains("SKIMS") ) writeSkims = value.Atoi();

    }
  }

  // Use the configuration file that this class required.
  // I.e. from the user passing it directly to any excutable,
  if( sampleinfo->deepConfigFile.IsWhitespace() ){
    MsgLog::ERROR("SampleHandler::readGroupSetConfig","Please specify a deep configuration file for your group set!!");
    abort();
  }

  //
  return sampleinfo;

}
// ---------------------------------------------------------------------- //
void SampleHandler::addSamples(SampleInfo*& sampleInfo,TString groupSet,TString path)
{

  //
  TString fullPath = gSystem->ExpandPathName( path.Data() );

  DIR* dp = opendir( fullPath.Data() );
  if(dp){

    // Store directory we scanned
    sampleInfo->dirName = groupSet;

    struct dirent * de;
    while ((de = readdir(dp)) != NULL) {

      // File name
      TString fileName (de->d_name);

      // Ignore hidden and backup files (~), and the groupset configuration file
      if(fileName.BeginsWith(".")) continue;
      if(fileName.Contains("~")) continue;
      if(fileName.Contains("groupset.config") ) continue;

      // User requested to not consider these files
      if( m_excludeFiles.Contains(fileName) ) continue;

      // User wants only files in m_includeFile
      if( !m_includeFiles.IsWhitespace() ){
	if( !m_includeFiles.Contains( fileName ) ) continue;
      }

      // Add in samples
      addSampleConfig(sampleInfo,fileName,fullPath);

    }
    closedir(dp);
  }
  else{
    MsgLog::ERROR("SampleHandler::addSamples","Cannot open directory %s ",path.Data() );
    return;
  }

}
// ---------------------------------------------------------------------- //
void SampleHandler::addSampleConfig(SampleInfo*& sampleInfo,TString textFileName, TString path)
{

  // Initialization, by reading in the user header configuration
  SampleConfig* master_config = readHeader( textFileName, path );

  // Filter samples by priority, only when user requested(>0=)
  if( m_priority>=0 && master_config->priority>=0 && master_config->priority!=m_priority ){
    MsgLog::INFO("SampleHandler::addSampleConfig","Process %s has priority %i. User requested %i priority, ignoring this process!",
                                                   master_config->process.Data(),
                                                   master_config->priority,
                                                   m_priority );
    delete master_config;
    return;
  }

  // Check that a SampleConfig doesn't exist with the name already
  // which would cause the samples to be merged from difference processes
  for( auto& sampleConfig : sampleInfo->sampleConfig ){

    if( master_config->process.EqualTo( sampleConfig->process ) ){
      MsgLog::WARNING("SampleHandler::addSampleConfig","Sample name is already found, please specify a unique name. Excluding this process %s", sampleConfig->process.Data());
      delete master_config;
      return;
    }
  }

  //
  addSampleConfig(sampleInfo,master_config,textFileName,path);

}
// ---------------------------------------------------------------------- //
void SampleHandler::addSampleConfig(SampleInfo*& sampleInfo,SampleConfig*& master_config, TString textFileName, TString path)
{

  //
  TString fullPath = path + "/" + textFileName;

  // User wants to look in the run directory
  if( path.IsWhitespace() ){
    fullPath = textFileName;
  }

  // Open file
  std::ifstream file ( fullPath.Data() );

  if( !file.is_open() ){
    Error("SampleHandler::addSampleConfig","Cannot open file %s ",fullPath.Data() );
    return;
  }

  bool seperateConfig = ( master_config->process.Contains("%") && !m_bulkSubmission ) ? true : false;

  // Clean slate
  master_config->samples.clear();

  TString line;
  while( file ){
    line.ReadLine(file, kTRUE);

    if( line.IsWhitespace() || line.Contains("#") ) continue;

    // Remove whitespace
    line.ReplaceAll(" ","");

    if( line.Contains(":") ){
      line = ((TObjString*)line.Tokenize(":")->At(1))->String();
    }

    if( seperateConfig ){

      // Now extract the dataset name
      // Example:
      // oneStepSS_%5_%6_%7  <---- numbers are where to tokenize in "_"

      const char* pc = master_config->process;

      TString digit   = "";
      bool find_digit = false;

      //
      std::vector<int> tokens;
      tokens.clear();

      for(int s=0;s<master_config->process.Length();s++){

	TString compare = TString(pc[s]);

	// Once we hit a '%' toggle the search for a digit
	if( compare == "%" ){
	  find_digit=true;
	  continue;
	}
	else if ( find_digit ){

	  // Checks
	  if( !isdigit( pc[s] ) ){
	    std::cout << "<SampleHandler::addSampleConfig> WARNING Invalid syntax, your process name should contain an interger after each '%" << std::endl;
	    std::cout << "<SampleHandler::addSampleConfig> WARNING I will abort. Your process name was: " << master_config->process << std::endl;
	    abort();
	  }
	  else{
	    digit += TString( pc[s] );
	  }

	  // Check if we should turn off
	  if( s+1 <= master_config->process.Length() ){

	    // If the next char in the string is not a digit,
	    //   -> Turn off the search, wait for another '%' if it exists
	    //   -> Save the integer we found
	    //   -> Reset the digit variable
	    if( !isdigit( pc[s+1] ) ){
	      tokens.push_back ( digit.Atoi() );
	      find_digit = false;
	      digit= "";
	    }
	  }
	  else{
	    tokens.push_back ( digit.Atoi() );
	    find_digit = false;
	    digit= "";
	  }

	} // find_digit

	// We don't care
	else{
	  continue;
	}
      }

      //
      // Now lets change the process name
      TString processName = master_config->process;

      for( auto& token : tokens ){

	// Tokenize
	TString var = ((TObjString*)line.Tokenize("_")->At(token))->String();

	// If contains a dot. we are most likely containing something we don't want. Take the first instance...
	if( var.Contains(".") ) var = ((TObjString*)var.Tokenize(".")->At(0))->String();

	// Now replace corresponding %i with the extracted field!
	std::stringstream ss;
	ss << token;

	TString unique_id = "%";
	unique_id         += TString( ss.str() );

	if( processName.Contains( unique_id.Data() ) ){
	  processName.ReplaceAll( unique_id , var );
	}

      }

      // TODO: this should be going into a copy constructor...
      // TODO: Migrate this to its own class -> DONE this is now SampleSet
      SampleConfig* singleConfig = new SampleConfig();
      singleConfig->process      = processName;
      singleConfig->isTruthOnly  = master_config->isTruthOnly;
      singleConfig->isData       = master_config->isData;
      singleConfig->isAF2        = master_config->isAF2;
      singleConfig->priority     = master_config->priority;

      // Single sample
      singleConfig->samples.push_back(line);

      // Save into SampleInfo object
      sampleInfo->sampleConfig.push_back( singleConfig );

    }
    // All samples into one configuration
    else{
      master_config->samples.push_back(line);
    }

  }

  // Save into the sampleInfo if we have something non-trival
  if(master_config->samples.size() > 0 ){
    sampleInfo->sampleConfig.push_back( master_config );
  }


}
// ---------------------------------------------------------------------- //
SampleHandler::SampleConfig* SampleHandler::readHeader(TString textFileName, TString path)
{

  SampleConfig* sampleConfig = 0;

  TString fullPath = path + "/"+  textFileName;

  // Open file
  std::ifstream file ( fullPath.Data() );

  if( !file.is_open() ){
    std::cout << "<SampleHandler::readHeader ERROR Cannot open file " << fullPath << std::endl;
    return sampleConfig;
  }

  std::cout << "<SampleHandler::readHeader INFO Open file " << fullPath << std::endl;
    
  // Store our configuration
  sampleConfig = new SampleConfig();

  // Defaults
  TString process = textFileName;
  int isData      = 999;
  int isAF2       = 0;
  int isTruthOnly = 0;
  int priority    = -1;

  TString line;
  while( file ){
    line.ReadLine(file, kTRUE);

    if( line.IsWhitespace() ) continue;

    // Internal hard coded values local to this method only, to avoid more flags
    if( line.Contains("mc") && isData==999   ) isData=100;
    if( line.Contains("data") && isData==999 ) isData=200;

    // Only consider lines starting with #
    if( line.Contains("#") ){

      // Tokenize second field and remove whitespace
      if( !line.Contains(":") || line.Tokenize(":")->GetEntries() !=2  ) continue;

      TString user_config = ((TObjString*)line.Tokenize(":")->At(1))->String();
      user_config.ReplaceAll(" ","");

      // Configurations
      // In principle, the user could add any configurations here to
      // the sample configuration
      // Note if you are going to do this, refrain from using any field which is found
      // in any of the dataset names (like TRUTH...)
      if( line.Contains("NAME")      ) process = user_config;
      if( line.Contains("AF2")       ) isAF2 = user_config.Atoi();
      if( line.Contains("DATA")      ) isData = user_config.Atoi();
      if( line.Contains("TRUTHONLY") ) isTruthOnly = user_config.Atoi();
      if( line.Contains("PRIORITY")  ) priority = user_config.Atoi();

    }
  }

  //
  file.close();
    
  // In the event user didn't set
  if( isData==100 ) isData = 0;
  if( isData==200 ) isData = 1;
  if(process.Contains(".txt")) process.ReplaceAll(".txt","");
  // Store into the sample config
  sampleConfig->process      = process;
  sampleConfig->isData       = isData==0 ? false : true;
  sampleConfig->isAF2        = isAF2==0 ? false : true;
  sampleConfig->isTruthOnly  = isTruthOnly==0 ? false : true;
  sampleConfig->priority     = priority;

  return sampleConfig;

}
// ---------------------------------------------------------------------- //
void SampleHandler::addGroup(SampleInfo*& sampleInfo)
{

  // Insert into sampleInfo
  m_sampleInfo.insert( std::pair<TString,SampleInfo*>(sampleInfo->groupName,sampleInfo) );

}
// ---------------------------------------------------------------------- //
SampleHandler::SampleInfo* SampleHandler::getSampleInfo(TString groupName)
{

  auto it = m_sampleInfo.find(groupName);

  if( it == m_sampleInfo.end() ){
    Error("SampleHandler::getSampleInfo","Cannot find SampleInfo for %s", groupName.Data() );
    abort();
  }

  // In the event we have no systematics, add a dummy one
  if( it->second->sysConfig.size()==0 ){
    it->second->addSysSet("",1,0);
  }

  return it->second;

}
// ---------------------------------------------------------------------- //
void SampleHandler::download()
{

  if( m_configured < 2 ){
    Error("SampleHandler::download","Class not configured, please call SampleHandler::initialize(...)" );
    abort();
  }

  Info("SampleHandler::download","Downloading samples...");

  SampleInfo* set =  getSampleInfo(m_groupName);

  // Download trees
  if( !m_disableTrees ) download(set,m_tag,m_treeDir,"tree");

  // Download skims
  if( !m_disableSkims ) download(set,m_tag,m_skimDir,"skim");

  //
  Info("SampleHandler::download","Finshed downloading all requested samples");

  return;

}
// ---------------------------------------------------------------------- //
void SampleHandler::download(const SampleInfo* set, TString tag, TString dir, TString stream)
{

  //
  Info( "SampleHandler::download","Downloading stream %s to directory %s ... ",stream.Data(),dir.Data() );
  //

  for(auto& sampleConfig : set->sampleConfig ){
    std::cout << " >> Sample configuration: " << sampleConfig->process << std::endl;
    for(auto & sample : sampleConfig->samples ){
      for( auto& sys : set->sysConfig ){
        std::cout << " >> Systematic: " << sys->systematic << std::endl;
        TString partialSampleName = getDSName(sample,tag,sys->systematic,sampleConfig->isData,sys->writeSkims,sys->writeTrees);
        TString download_log = sample + "_" + stream + ".download_log";
        std::string processName(sampleConfig->process.Data());
        boost::regex re("_%[0-9]+"); // replace the number _%[0-9] fields in signal samples
        TString exactSampleName = boost::regex_replace(processName, re, "")+"."+partialSampleName;
        // for data the process is not merged into the name currently
        if (sampleConfig->isData) {
          exactSampleName = partialSampleName;
        }

        // Formatting
        if( download_log.Contains(":") )
          download_log = ((TObjString*)download_log.Tokenize(":")->At(1))->String();
        if( download_log.Contains("/") ) download_log.ReplaceAll("/","");

        //
        download_log = dir + "/" + download_log;

        TString cmd = TString::Format("rucio download --ndownloader 5 --dir=%s %s:%s.%s_%s.root > %s",dir.Data(),
                                      m_prefix.Data(),m_prefix.Data(),exactSampleName.Data(), stream.Data(), download_log.Data() );

        // Download!
        if( !m_noSubmit ){
          gSystem->Exec( cmd.Data() );
        }

        // Disabling.. MG
        continue;

        // Append ".root" to files not ending in ".root"
        TString download_folder = TString::Format("%s/%s.%s_%s.root", dir.Data(), m_prefix.Data(), exactSampleName.Data(), stream.Data());
        boost::filesystem::path download_path(download_folder.Data());
        for(auto& entry : boost::make_iterator_range(boost::filesystem::directory_iterator(download_path), {})) {
          TString filename(entry.path().c_str());
          if (!filename.EndsWith(".root")) {
            TString newfilename = filename+".root";
            std::cout << "Found a file not ending in \".root\" - Renaming " << filename << " to " << newfilename << std::endl;
            boost::filesystem::rename(boost::filesystem::path(filename.Data()), boost::filesystem::path(newfilename.Data()));
          }
        }

      }
    }
  }

}
// ---------------------------------------------------------------------- //
void SampleHandler::downloadPRW()
{

 if( m_configured < 2 ){
    Error("SampleHandler::download","Class not configured, please call SampleHandler::initialize(...)" );
    return;
  }

  if( m_pTag==""){
    MsgLog::ERROR("downloadPRW","No p-tag specified! Required for this method!");
    return;
  }

  Info("SampleHandler::download","Downloading PRW profiles...");

  SampleInfo* set =  getSampleInfo(m_groupName);

  for(auto& sampleConfig : set->sampleConfig ){
    std::cout << " >> Sample configuration: " << sampleConfig->process << std::endl;
    for(auto & sample : sampleConfig->samples ){
      std::cout << " >> Sample: " << sample << std::endl;

      //
      TObjArray* tokens = sample.Tokenize(".");
      TString dataYear  = ((TObjString*)tokens->At(0))->String();
      TString datasetID(""),process(""),taglist("");
      if (tokens->GetLast() >= 1) datasetID = ((TObjString*)tokens->At(1))->String();
      if (tokens->GetLast() >= 2) process   = ((TObjString*)tokens->At(2))->String();
      if (tokens->GetLast() >= 5) taglist   = ((TObjString*)tokens->At(5))->String();

      if( dataYear=="" || datasetID=="" || process=="" || taglist=="" ){
        MsgLog::WARNING("SampleHandler::downloadPRW","Could not download PRW sample for %s",sample.Data() );
        continue;
      }

      TObjArray* tags = taglist.Tokenize("_");

      // mc16_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.merge.NTUP_PILEUP.e5607_s3126_r9364_r9315_p3127_p3126/
      // mc16_13TeV.343981.PowhegPythia8EvtGen_NNLOPS_nnlo_30_ggH125_gamgam.deriv.DAOD_HIGG1D1.e5607_s3126_r9364_r9315_p3472
      // mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.deriv.NTUP_PILEUP.e5340_e5984_s3126_s3136_r9364_r9315_p3288
      // Ignore the p-tags in our dataset
      TString finalTags="";
      for(int i=0; i<=tags->GetLast(); i++){
        TString t = ( (TObjString*)tags->At(i) )->String().Data();
        if( t.Contains("e") || t.Contains("s") || t.Contains("r") || t.Contains("a") ) finalTags += t+"_";
      }

      // Command
      TString cmd = TString::Format("rucio download --ndownloader 5 --dir=%s %s:%s.%s.%s.deriv.NTUP_PILEUP.%s*%s",m_prwDir.Data(),
                                    dataYear.Data(),dataYear.Data(),datasetID.Data(),process.Data(), finalTags.Data(),m_pTag.Data() );

      std::cout << cmd << std::endl;


      // Run
      gSystem->Exec( cmd.Data() );

    } // Loop over samples within a config
  } // Loop over sample configurations




}
// ---------------------------------------------------------------------- //
void SampleHandler::prepareMergeGroupSet()
{

  if( m_configured < 2 ){
    MsgLog::ERROR("SampleHandler::merge","Class not configured, please call SampleHandler::initialize(...)." );
    return;
  }

  // TODO
  //MetaData* metaData  = 0;

  //
  MsgLog::INFO("SampleHandler::merge","Starting merge of samples...");

  // Set env vars to make use of in the executables
#if ROOTCORE_RELEASE_SERIES>24
  setenv("analysisExecutablesDir",gSystem->ExpandPathName("${WorkDir_DIR}/bin/"),1);
#else
  setenv("analysisExecutablesDir",gSystem->ExpandPathName("${ROOTCOREBIN}/bin/x86_64-slc6-gcc49-opt/"),1);
#endif

  // Get dataset information for this production
  SampleInfo* set = getSampleInfo( m_groupName );

  // Formats sample tables in LateX
  dumpSampleInfo(m_tag);

  // Reset
  m_mergeSampleSets.clear();

  // 
  for( auto& sampleConfig : set->sampleConfig ){

    // TODO: Once integrated, this will replace SampleConfig,
    // and we will instead have a loop over list of SampleSet
    // for now, testing its usage
    SampleSet* sampleSet = new SampleSet();
    sampleSet->setName( sampleConfig->process );
    sampleSet->isData     = sampleConfig->isData;
    sampleSet->isTruth    = sampleConfig->isTruthOnly;
    sampleSet->isAF2      = sampleConfig->isAF2;
    sampleSet->process    = sampleConfig->process;
    sampleSet->deepConfig = set->deepConfigFile;
    sampleSet->baseDir    = m_baseDir;
    sampleSet->groupSet   = m_groupName;
    sampleSet->tag        = m_tag;
    sampleSet->lumi       = m_lumi;
    sampleSet->nEvtAMIMap = std::map<TString, int>();

    MsgLog::INFO("SampleHandler::merge","Starting merging of process: %s", sampleConfig->process.Data() );

    // Store the number of events from AMI in a map for later comparison
    if (!m_disableMetaDataCheck) {
      for(auto& sample : sampleConfig->samples ){
        // Get number of events from AMI
        MsgLog::INFO("SampleHandler::merge","Checking sample %s...",sample.Data());
        int nEvt_AMI = getNumEventsAMI(sample);

        // add this sample to the map, and store the AMI nEvent count
        sampleSet->nEvtAMIMap[sampleConfig->process] += nEvt_AMI;

        // and also by dsid/runNumber
        TString dsidString = ((TObjString*)sample.Tokenize(".")->At(1))->String();
        if (sampleSet->nEvtAMIMap.find(dsidString) == sampleSet->nEvtAMIMap.end()) {
          sampleSet->nEvtAMIMap[dsidString] = 0;
        }
        sampleSet->nEvtAMIMap[dsidString] += nEvt_AMI;
      }
    }

    // When the user submit to their local batch system
    // the trees are stored into their m_localTreeDir
    // and are named with their process. So the
    // sample name is simply their process__*.root
    if( m_mergeByProcessName ){

      // Cache the input files into the
      // mergeInputs list of SampleConfig
      if( !getInputFiles(m_localTreeDir,m_tag,sampleConfig->process,sampleConfig,sampleSet) ){
        MsgLog::WARNING("SampleHandler::merge","No samples found for process %s when merging by process name! Your merged set will be incomplete!!",sampleConfig->process.Data() );
        continue;
      }

    }
    // Collect all samples from a given list of input
    // samples in a group set
    else{

      for(auto& sample : sampleConfig->samples ){

        // Search for a tag. Uses m_tag, unless
        // the user provided priority tags
        TString sampleTag = findTagForSample(sample,sampleConfig);

        // Ensure proper tree directory, if the
        // sampleTag is not the same as m_tag
        TString treeDir = m_baseDir + "/" + m_groupName + "/" + sampleTag  + "/trees/" ;

	// Ensure the samples exists and
	// cache all found input files
	// into the mergeInputs list of SampleConfig
	if( !getInputFiles(treeDir,sampleTag,sample,sampleConfig,sampleSet) ){
          // To be extra safe, we abort here. But we could add this as an option...
          MsgLog::WARNING("SampleHandler::merge","No files were found for sample %s !!! Your merged set will be incomplete!!",sample.Data() );
          MsgLog::WARNING("SampleHandler::merge","aborting!");
          //abort();
          /**
          MsgLog::ERROR("SampleHandler::merge","No files were found for sample %s !!! Your merged set will be incomplete!!",sample.Data() );
          MsgLog::ERROR("SampleHandler::merge","aborting!");
          abort();
          */
	}


      } // Loop over samples
    }

    //
    sampleSet->printMergeInputs();
    sampleSet->write( m_mergeDir );

    // Cache
    m_mergeSampleSets.push_back(sampleSet);

  } // Loop over processes


}
// ---------------------------------------------------------------------- //
void SampleHandler::extractMetaData(MetaData*& metaData,TString sample)
{

  if( metaData ) return;

  TFile* file = TFile::Open( sample.Data(), "READ" );

  if( !file->IsOpen() ){
    Warning("SampleHandler::extractMetaData","Cannot extract metaData from sample %s ", sample.Data() );
    metaData = 0;
    return;
  }

  // Retrieve it!
  TTree* metaTree = dynamic_cast<TTree*>( file->Get("MetaTree") );

  if( !metaTree ){
    Warning("SampleHandler::extractMetaData","No MetaTree in sample %s ", sample.Data() );
    metaData = 0;
    file->Close();
    return;
  }

  //
  metaTree->SetBranchAddress("metaData", &metaData);

  if( metaTree->GetEntries()>=1 ){
    metaTree->GetEntry(0);
    metaData->print();
  }

}
// ---------------------------------------------------------------------- //
void SampleHandler::dumpSampleInfo(TString tag)
{

  //
  if( m_skipSampleInfoDump ) return;

  //
  SampleInfo* set = getSampleInfo( m_groupName );

  // This has the effect of grouping the signal samples
  // incase the user has them automatically seperated
  // for merging
  bool bulkSubmission = m_bulkSubmission;
  this->isBulkSubmission(true);

  // Remake the sample info, without seperated signal samples
  SampleInfo* sampleInfoSet = new SampleInfo();
  sampleInfoSet->groupName  = m_groupName + "_ungrouped_sampleInfoSet";
  sampleInfoSet->SUSYToolsConfig = set->SUSYToolsConfig;

  TString path = getPath(m_groupName,m_groupSetDir,m_groupSetDir);

  addSamples(sampleInfoSet,set->dirName,path);
  addGroup(sampleInfoSet);

  // Clean
  gSystem->Exec("rm -rf " + m_sampleInfoDir + "/*" );

  for(auto& sampleConfig : sampleInfoSet->sampleConfig ){

    TString processName = sampleConfig->process;
    Info("SampleHandler::dumpSampleInfo","Dumping sample info for process name %s",processName.Data());

    // Store normalization into a map
    std::map<int,double> norm;
    norm.clear();

    for(auto& sample : sampleConfig->samples ){

      // DSID by DSID
      TString outputDSID = m_treeDir + "/" + TString::Format("%s*.%s",m_prefix.Data(),getDSName(sample,tag,"",sampleConfig->isData).Data() )  + "*/";
      gSystem->Exec( "find -L " + outputDSID + " -type f -name '*.root*' | sort > " + m_sampleInfoDir + "/temp.txt");

      // Open file
      std::ifstream f ( m_sampleInfoDir + "/temp.txt" );

      if( f.is_open() ){

	TString line;
	while( f ){

	  line.ReadLine(f);

	  // Ignore whitespaces,hidden and backup files (~)
	  if( line=="" ) continue;
	  // if( line.BeginsWith(".") ) continue; // Here, this is not good - paths returned by ls can start with "./"
	  if( line.Contains("~") ) continue;

	  TFile* file = TFile::Open( line.Data(), "READ" );
	  if( !file || !file->IsOpen() ){
	    Warning("SampleHandler::dumpSampleInfo","Cannot open file for %s",line.Data());
	    continue;
	  }

	  TH1F* hist = dynamic_cast<TH1F*>( file->Get("unweighted__AOD") );
	  if( !hist ){
	    Warning("SampleHandler::dumpSampleInfo","Cannot get unweighted__AOD histogram in file %s",line.Data());
	    continue;
	  }

	  // Extract all relevant numbers
	  for( int bin=0; bin<hist->GetNbinsX(); bin++){
	    TString label = hist->GetXaxis()->GetBinLabel(bin);
	    if(label!=""){

	      double binContent = hist->GetBinContent(bin);
	      int dsid = label.Atoi();

	      // Save into map
	      auto it = norm.find(dsid);
	      if( it != norm.end() ){
		it->second += binContent;
	      }
	      else{
		norm.insert(std::pair<int,double>(dsid,binContent) );
	      }

	    }
	  } // Loop over bins

	  file->Close();

	}
      } // Loop over files associated to a DSID

      f.close();

      // Clean up
      gSystem->Exec( "rm -f " + m_sampleInfoDir + "/temp.txt");

    } // Loop over samples

    // Extract information and output text files
    RetrieveInfo* info = new RetrieveInfo();
    info->setOutputDir( m_sampleInfoDir );
    info->getXsec( processName, norm,sampleConfig->samples, m_eventObject );
    delete info;

  } // Loop over processes

  // Restore original configuration
  this->isBulkSubmission(bulkSubmission);

}
// ----------------------------------------------------------------------------------------------------------- //
void SampleHandler::linkPRWFriendTree(TString filePath,TString process,TFile*& master_file,TTree*& master_tree)
{

  //
  TFile* file = TFile::Open( filePath.Data(), "UPDATE");
  if( !file->IsOpen() ){
    std::cout << "<SampleHandler::linkPRWFriendTree> Cannot open PRW file: " << filePath << std::endl;
    return;
  }

  TTree* prwTree = dynamic_cast<TTree*>( file->Get("prwTree") );
  if( !prwTree ){
    std::cout << "<SampleHandler::merge> ERROR No prw tree found!" << std::endl;
    return;
  }


  // --- TEMP fix --- //
  // Fix nan's

  master_file->cd();
  TTree* prw = prwTree->CloneTree(0);
  TString PRW_tree_name = process + "__PRWFriendTree";
  prw->SetName( PRW_tree_name.Data() );
  prw->SetTitle( PRW_tree_name.Data() );
  //prw->SetDirectory( master_file );

  Float_t pileupWeightOnFly;
  prwTree->SetBranchAddress("pileupWeightOnFly",&pileupWeightOnFly);

  for( int e=0; e<prwTree->GetEntries(); e++){

    int getEntry = prwTree->GetEntry(e);
    if(getEntry==-1){
      Error("SampleHandler::linkPRWFriendTree", "GetEntry() I/O occurred error!");
      abort();
    }
    else if(getEntry==0){
      // This is the return code for an invalid entry, so skip it
      continue;
    }

    if( TMath::IsNaN( pileupWeightOnFly ) ){
      Info("linkPRWFriendTree","Detected problematic weight, setting to zero!!!!");

      pileupWeightOnFly = 0;
    }
    prw->Fill();
  }

  file->Close();

  TChain* prwChain = new TChain("prwTree");
  prwChain->Add( filePath.Data() );

  master_file->cd();
  Info("linkPRWFriendTree","Adding %s tree to %s as a friend",PRW_tree_name.Data(), master_file->GetName() );
  prwChain->BuildIndex("PRWHash");

  // Finally link these two trees
  master_file->cd();
  master_tree->AddFriend( prwChain );
  //prw->Write();

  return;

}
// ----------------------------------------------------------------------------------------------------------- //
void SampleHandler::countDuplicateEvents(DuplicateEventChecker*& dec, const SampleSet* sampleSet, TTree*& tree)
{

  if( !tree ){
    MsgLog::WARNING("SampleHandler::countDuplicateEvents","Null or corrupted tree tree! Cannot check for events duplicates!");
    return;
  }

  // Lets start!
  unsigned long long EventNumber = 0;
  Int_t DatasetNumber = 0;
  Float_t truthValue = 0;
  Float_t recoValue = 0;

  TObjArray* listOfBranches = tree->GetListOfBranches();
  if( !sampleSet->isData ){
    if(listOfBranches->FindObject("GenHt")){
      tree->SetBranchAddress("GenHt", &truthValue);
    }
    else if(listOfBranches->FindObject("TrueHt")){
      tree->SetBranchAddress("TrueHt", &truthValue);
    }
    else{
      MsgLog::WARNING("SampleHandler::countDuplicateEvents","Could not find GenHt or TrueHt branch for the truth level duplicate event counting!");
      return;
    }
    tree->SetBranchAddress("DatasetNumber", &DatasetNumber);
  }
  else{
    truthValue = -1.0;
    tree->SetBranchAddress("RunNumber", &DatasetNumber);
  }

  // FIXME should just configure this with a CentralDB field
  if(listOfBranches->FindObject("met")){
    tree->SetBranchAddress("met", &recoValue);
  }
  else if(listOfBranches->FindObject("met_Et")){
    tree->SetBranchAddress("met_Et", &recoValue);
  }
  else{
    MsgLog::WARNING("SampleHandler::countDuplicateEvents","Could not find met or met_Et branch for the reco level duplicate event counting!");
    return;
  }

  tree->SetBranchAddress("EventNumber", &EventNumber);

  //
  for (int i=0; i<tree->GetEntries(); i++) {

    //
    int getEntry = tree->GetEntry(i);
    if(getEntry==-1){
      Error("SampleHandler::countDuplicateEvents", "GetEntry() I/O occurred error!");
      abort();
    }
    else if(getEntry==0){
      // This is the return code for an invalid entry, so skip it
      continue;
    }

    // Count, internally
    dec->check(DatasetNumber,EventNumber,truthValue,recoValue);

  } // Loop over TTree

}
// ----------------------------------------------------------------------------------------------------------- //
void SampleHandler::checkMissingEvents(DuplicateEventChecker*& dec,const SampleSet* sampleSet, TChain* _cbkChain )
{

  TChain* cbkChain = 0;
  if(_cbkChain) cbkChain = (TChain*) _cbkChain->Clone();

  if( !cbkChain ){
    MsgLog::WARNING("SampleHandler::checkMissingEvents","Null or corrupted cbkChain! Cannot check for missing events!");
    return;
  }

  // TODO rather than rely on the process name, we could consider
  // checking based on RunNumber (data) or DSID (MC)
  TString process = sampleSet->process;
  int nEvtAMI = sampleSet->nEvtAMIMap.at(process);
  int nEvtCBK = 0;

  TString derivationName = "";
  CentralDB::retrieve(CentralDBFields::DXAODKERNEL,derivationName);

  int nEvtCBKPerEntry = 0;
  TString nEventsBranchName = derivationName+"_nAcceptedEvents";
  int res = cbkChain->SetBranchAddress(nEventsBranchName.Data(), &nEvtCBKPerEntry);
  if (res < 0) {
    Error("SampleHandler::checkMissingEvents", "Branch %s in cbk tree doesn't exist - can't check for missing events", nEventsBranchName.Data());
    abort();
  }

  unsigned int runNumber;
  unsigned int datasetNumber;
  cbkChain->SetBranchAddress("DatasetNumber", &datasetNumber);
  cbkChain->SetBranchAddress("RunNumber", &runNumber);

  std::map<TString, int> nEvtCBKMap;
  // Safely loop over cbkChain
  for (int i=0; i<cbkChain->GetEntries(); i++) {

    int getEntry = cbkChain->GetEntry(i);
    if(getEntry==-1){
      Error("SampleHandler::checkMissingEvents", "GetEntry() I/O occurred error!");
      abort();
    }
    else if(getEntry==0){
      // This is the return code for an invalid entry, so skip it
      continue;
    }

    TString cbkKey;
    if (sampleSet->isData) {
      cbkKey = TString::Format("%08d", runNumber);
    } else {
      cbkKey = TString::Format("%d", datasetNumber);
    }

    if (nEvtCBKMap.find(cbkKey) == nEvtCBKMap.end()) {
      nEvtCBKMap[cbkKey] = 0;
    }

    nEvtCBKMap[cbkKey] += nEvtCBKPerEntry;

    nEvtCBK += nEvtCBKPerEntry;
  } // Loop over TTree

  if(nEvtCBK != nEvtAMI){
    MsgLog::WARNING("SampleHandler::checkMissingEvents","Missing events! Check log file!!");
    TString missingEventsMessage = TString::Format("Missing events in process %s: nEvtCBK is %i while nEvtAMI is %i", process.Data(), nEvtCBK, nEvtAMI);
    dec->writeAdditional(missingEventsMessage);
  }
  else{
    TString missingEventsMessage = TString::Format("No missing events in process %s: nEvtCBK is %i and nEvtAMI is %i", process.Data(), nEvtCBK, nEvtAMI);
    dec->writeAdditional(missingEventsMessage);
  }

  // Now per dsid/runNumber
  // Loop over ami dict to find also samples where cbk is missing
  for (auto amiTuple : sampleSet->nEvtAMIMap) {
    if (amiTuple.first == process) {
      // already got the merged one above
      continue;
    }
    int nEvtAMI = amiTuple.second;
    if (nEvtCBKMap.find(amiTuple.first) == nEvtCBKMap.end()) {
      TString missingEventsMessage = TString::Format("No cbk entry for DatasetNumber/RunNumber %s - Probably this sample was not processed! (nEvtAMI: %i)", amiTuple.first.Data(), nEvtAMI);
      dec->writeAdditional(missingEventsMessage);
      continue;
    }
    int nEvtCBK = nEvtCBKMap.at(amiTuple.first);
    if (nEvtCBK != nEvtAMI) {
      TString missingEventsMessage = TString::Format("Missing events for DatasetNumber/RunNumber %s: nEvtCBK is %i while nEvtAMI is %i", amiTuple.first.Data(), nEvtCBK, nEvtAMI);
      dec->writeAdditional(missingEventsMessage);
    }
  }

}
// ---------------------------------------------------------------------- //
TString SampleHandler::doPileupOnTheFly(TString outputDir,TString listOfDSID, bool isAF2, int var)
{

  MsgLog::WARNING("SampleHandler::doPileupOnTheFly","This method requires updating if you are to use it!!!");
  (void)isAF2;

  Info("doPileupOnTheFly","Calling tool with list of DSIDs %s ",listOfDSID.Data () );

  TString rootFilePath = m_eventObject->makeWeightTree(outputDir,listOfDSID,"PRWHash","pileupWeightOnFly",var);

  //
  return rootFilePath;

}
// ---------------------------------------------------------------------- //
void SampleHandler::submitByGroupSet(TString selector,BATCH_TYPE batch_type)
{

  if( m_configured < 2 ){
    Error("<SampleHandler::submitByGroupSet","SampleHandler not configured, please call SampleHandler::initialize(...)" );
    return;
  }

  int totalNumberOfJobs = 0;
  int localJobCounter   = 0;

  // Get dataset information for this production
  SampleInfo* set = getSampleInfo( m_groupName );

  // Loop over sample-sets
  for( auto& sampleConfig : set->sampleConfig ){

    //
    TString processName = sampleConfig->process;
    Info("SampleHandler::submitByGroupSet","Collecting and submitting samples for process %s...",processName.Data() );

    localJobCounter=0;

    // Properties of the job
    int totalEventsInJob=0;
    std::vector<TString> samples;
    samples.clear();

    // Actual samples
    for(auto& sample : sampleConfig->samples ){

      std::ifstream file;

      // Try to open a txt file containing the paths/urls to the skims
      file.open(TString::Format("%s/%s.txt", m_skimDir.Data(), getDSName(sample, m_tag, "", sampleConfig->isData).Data()));

      // Otherwise try to wildcard search the root files in that directory
      if (!file.is_open()) {
        // Complete dataset name
        TString input = TString::Format("%s/%s*.%s*/*.root*",m_skimDir.Data(), m_prefix.Data(), getDSName(sample,m_tag,"",sampleConfig->isData).Data() );

        // List all input files, complain if they are not found
        if( gSystem->Exec( TString::Format("ls %s > temp.txt",input.Data() ).Data() ) != 0 ){
          Warning("SampleHandler::submitByGroupSet","Could not find any samples for %s!!!!", sample.Data() );
          continue;
        }

        // Open file
        file.open( "temp.txt" );

        if( !file.is_open() ){
          Error("SampleHandler::submitByGroupSet","Cannot open file temp.txt");
          continue;
        }
      }

      // Loop over samples
      TString line;
      while( file ){
        line.ReadLine(file, kTRUE);
        if( line.IsWhitespace() ) continue;

	TFile* file = TFile::Open(line.Data(),"READ");

	if( !file->IsOpen() ){
	  Warning("SampleHandler::submitByGroupSet","Cannot open file %s",line.Data() );
	  continue;
	}

	TTree* tree = dynamic_cast<TTree*>( file->Get("skim_NoSys") );

	if( !tree ){
	  Warning("SampleHandler::submitByGroupSet","Cannot get skim_NoSys tree from %s. Excluding this file!",line.Data() );
	  file->Close();
	  continue;
	}

	// Add in samples
	// At any point in this nested loop,
	// if the number of events is greater than
	// the allowed number of events, submit to the batch system
	int nEvents = tree->GetEntries();
	totalEventsInJob += nEvents;
	samples.push_back(line);
	file->Close();

	if( totalEventsInJob<m_maxEventsPerJob ){
          // Note: If this is the last file in the list the
          // next file will be from another dsid (so the job will run on
          // a TChain that contains multiple dsids). This is ok, since
          // the whole submission still gets called for each process
          // individually (and the sumwhists are merged by bin labels).
	  continue;
	}
	else{

	  // Submit!
          if (totalEventsInJob > m_maxEventsPerJob*1.5 && m_splitLargeFiles) {
            // If more than 50% more than maxentries split up further
            for (int i=0; i<float(totalEventsInJob)/float(m_maxEventsPerJob); i++) {
              submitToBatch( samples,TString::Format("%s/%s__%i_%i",m_localTreeDir.Data(),processName.Data(),localJobCounter,i),selector,batch_type,i*m_maxEventsPerJob, m_maxEventsPerJob);
            }
          } else {
            submitToBatch( samples,TString::Format("%s/%s__%i",m_localTreeDir.Data(),processName.Data(),localJobCounter),selector,batch_type );
          }

	  // Update global info
	  samples.clear();
	  totalEventsInJob=0;
	  totalNumberOfJobs++;
          localJobCounter++;

	}

      } // Loop over temp file

    } // Loop over samples

    // Submit!
    submitToBatch( samples,TString::Format("%s/%s__%i",m_localTreeDir.Data(),processName.Data(),totalNumberOfJobs),selector,batch_type );

    // Update global info
    samples.clear();
    totalEventsInJob=0;
    totalNumberOfJobs++;

    // Clean up
    gSystem->Exec("rm -rf temp.txt");

  }

  Info("SampleHandler::submitByGroupSet","Submitted a total of %i jobs to batch system",totalNumberOfJobs);

}
// ---------------------------------------------------------------------- //
void SampleHandler::submitToBatch(std::vector<TString> samples,TString output,TString selector,BATCH_TYPE batch_type, int nSkip/*=0*/, int nEvt/*=-1*/)
{

  // Nothing to submit
  if( samples.size()==0 ) return;

  // Require a selector
  if( selector=="" ){
    Error("SampleHandler::submitToBatch","No selector! Aborting...");
    abort();
  }

  // Build up input
  TString inputFiles = "";
  for( auto& s : samples ){
    if( inputFiles=="" ){
      inputFiles = s;
    }
    else{
      inputFiles += ","+s;
    }
  }

  if( m_resubmitBatchJobs ){
    bool resubmit = false;
    TFile* f = TFile::Open(output+".root","READ");
    if( !f || !f->IsOpen() ){
      MsgLog::INFO("SampleHandler::submitToBatch","Could not open output root file, resubmitting...");
      resubmit = true;
    }

    if( f && !resubmit ){
      TTree* tree = dynamic_cast<TTree*>(f->Get("tree_NoSys"));
      if( !tree ){
        MsgLog::INFO("SampleHandler::submitToBatch","Could not get TTree, resubmitting...");
        resubmit = true;
      }

      if( !resubmit ){
        int nEntries  = tree->GetEntries();
        if( nEntries==0 ){
	  MsgLog::INFO("SampleHandler::submitToBatch","Detected an empty TTree, rsubmitting...");
	  resubmit = true;
        }
      }
    }

  if( !resubmit ) return;

  } // Resubmit failed jobs

  if(batch_type==BATCH_TYPE::TORQUE){
    submitToTorque(inputFiles,output,selector,-1,nSkip,nEvt);
  }
  else if(batch_type==BATCH_TYPE::CONDOR){
    submitToCondor(inputFiles,output,selector,nSkip,nEvt);
  }
  else if(batch_type==BATCH_TYPE::LRZ){
    submitToLRZ(inputFiles,output,selector,-1,nSkip,nEvt);
  }
  else if(batch_type==BATCH_TYPE::ETP){
    submitToETP(inputFiles,output,selector,nSkip,nEvt);
  }
  else if(batch_type==BATCH_TYPE::LXPLUS){
    submitToLxPlus(inputFiles,output,selector,nSkip,nEvt);
  }
  else{
    Error("SampleHandler::submitToBatch","Unsupported batch type!! Aborting...");
    abort();
  }


}
// ---------------------------------------------------------------------- //
void SampleHandler::submitToCondor(TString input,TString output,TString selector,int nSkip, int nEvt)
{

  if(nSkip>0 || nEvt>0 ){
    MsgLog::WARNING("SampleHandler::submitToCondor","Asking for nSkip or nEvt, which is not implemented for this method yet!");
  }

  TString condor_batch_script = getPath("condor_batchScript.sh","$ROOTCOREBIN/../SusySkimMaker/scripts/batch/","SusySkimMaker");

  TString cmd = "condor_submit";
  //cmd += " -append \"log = ";
  //cmd += logDir+"/merge-"+processName+"-"+sys+".log\"";
  //cmd += " -append \"output = ";
  //cmd += logDir+"/merge-"+processName+"-"+sys+".out\"";
  //cmd += " -append \"error = ";
  //cmd += logDir+"/merge-"+processName+"-"+sys+".err\"";
  cmd += " -append \"arguments =";
  cmd += " -selector "+selector;
  cmd += " -F "+input;
  cmd += " -s "+output;
  cmd += "\"";
  cmd += " "+condor_batch_script;

  // Submit!
  if( !m_noSubmit ) gSystem->Exec( cmd.Data() );
  else{
    std::cout << cmd << std::endl;
  }

}
// ---------------------------------------------------------------------- //
void SampleHandler::submitToLxPlus(TString input, TString output, TString selector, int nSkip/*=0*/, int nEvt/*=-1*/)
{

  if(nSkip>0 || nEvt>0 ){
    MsgLog::WARNING("SampleHandler::submitToLxPlus","Asking for nSkip or nEvt, which is not implemented for this method yet!");
  }

  TString lxplus_batch_script = getPath("lxplus_batchScript.sh","$ROOTCOREBIN/../SusySkimMaker/scripts/batch/","SusySkimMaker");

  TString setup = "source "+lxplus_batch_script;
  TString cmd = "bsub ";

  // For running on SLAC
  /*
  TString hostname = TString(gSystem->GetFromPipe("hostname"));
  if(hostname.Contains("rhel6")){
    MsgLog::INFO("SampleHandler::submitToLxPlus","Detected a rhel6 machine %s!!",hostname.Data());
    //cmd += " -R \"rhel60\" ";
    cmd += " -R \"centos7\" ";
  }
  */
  //
  cmd += " -R \"select[centos7]\" ";
  cmd += " -q medium ";

  // Run!
  TString exc = TString(gSystem->GetFromPipe("which run_SkimNtRun"));
  cmd += " " + exc + " -F "+input+" -s "+output+" -selector "+selector+" ";

  // Submit!
  if( !m_noSubmit ) gSystem->Exec( cmd.Data() );
  else{
    std::cout << cmd << std::endl;
  }

}
// ---------------------------------------------------------------------- //
void SampleHandler::submitToTorque(TString input, TString output, TString selector,int totalNumberOfJobs, int nSkip/*=0*/, int nEvt/*=-1*/)
{

  if(nSkip>0 || nEvt>0 ){
    MsgLog::WARNING("SampleHandler:submitToTorque","Asking for nSkip or nEvt, which is not implemented for this method yet!");
  }

  // First check how many jobs are running
  if( m_maxQueuedJobs>0 ){

    int run    = TString( gSystem->GetFromPipe("qstat -q | grep batch | awk '{print $6}'") ).Atoi();
    int queue  = TString( gSystem->GetFromPipe("qstat -q | grep batch | awk '{print $7}'") ).Atoi();

    int total = run + queue;

    std::cout << "Total number of jobs: " << total << std::endl;

    //
    if( total >= m_maxQueuedJobs ){

      //
      std::cout << "Sleeping for a bit 60 seconds" << std::endl;
      std::this_thread::sleep_for (std::chrono::seconds(60));

      // Call this function again
      submitToTorque(input,output,selector,totalNumberOfJobs);
    }
  }

  TString torque_batch_script = getPath("torque_SkimNtRun.sh","$ROOTCOREBIN/../SusySkimMaker/scripts/batch/","SusySkimMaker");

  std::stringstream ss;
  ss << totalNumberOfJobs;

  TString cmd = "qsub -V -v job_name=SkimNtRunJobs,input=\""+input+"\",output="+output+",selector="+selector+" "+torque_batch_script;

  // Submit!
  if( !m_noSubmit ) gSystem->Exec( cmd.Data() );
  else{
    std::cout << cmd << std::endl;
  }

}
// ---------------------------------------------------------------------- //
void SampleHandler::submitToLRZ(TString input, TString output, TString selector,int totalNumberOfJobs, int nSkip/*=0*/, int nEvt/*=-1*/)
{

  // First check how many jobs are running (check every 11th job)
  if( m_maxQueuedJobs>0 && m_jobCounter == 10){

    int run    = TString( gSystem->GetFromPipe("squeue --clusters=lcg -u $USER | grep $USER | grep RUNNING | wc -l") ).Atoi();
    int queue  = TString( gSystem->GetFromPipe("squeue --clusters=lcg -u $USER | grep $USER | grep -v RUNNING | wc -l") ).Atoi();

    int total = run + queue;

    std::cout << "Total number of jobs: " << total << std::endl;

    //
    if( total >= m_maxQueuedJobs ){

      //
      std::cout << "Sleeping for a bit 60 seconds" << std::endl;
      std::this_thread::sleep_for (std::chrono::seconds(60));

      // Call this function again
      submitToLRZ(input,output,selector,totalNumberOfJobs,nSkip,nEvt);

      // avoid double execution
      return;
    }

    m_jobCounter = 0;
  }

  TString lrz_batch_script = getPath("lrzBatchScript.sh","$ROOTCOREBIN/../SusySkimMaker/scripts/batch/","SusySkimMaker");

  std::stringstream ss;
  ss << totalNumberOfJobs;

  TString nEvtnSkip = TString::Format("nEvt=%d,nSkip=%d", nEvt, nSkip);
  TString cmd = "sbatch -o $PTMP/log/SkimNtRun.out.%j --export=job_name=SkimNtRunJobs,input=\""+input+"\",output="+output+",selector="+selector+","+nEvtnSkip+",rootcorebin=$ROOTCOREBIN "+lrz_batch_script;

  // Submit!
  if( !m_noSubmit ) {
    gSystem->Exec( cmd.Data() );
  }
  m_jobCounter++;
  std::cout << cmd << std::endl;

}
// ---------------------------------------------------------------------- //
void SampleHandler::submitToETP(TString input, TString output, TString selector,int nSkip/*=0*/, int nEvt/*=-1*/)
{

  if (!m_jobcommands.is_open()) {
    m_jobcommands.open("jobcommands.sh");
  }

  TString etp_batch_script = getPath("etpBatchScript.sh","$ROOTCOREBIN/../SusySkimMaker/scripts/batch/","SusySkimMaker");

  TString nEvtnSkip = TString::Format("nEvt=%d,nSkip=%d", nEvt, nSkip);
  TString TestArea = gSystem->ExpandPathName("$TestArea");
  TString cmd = "sbatch -J SkimNtRunJobs --export=job_name=SkimNtRunJobs,input=\""+input+"\",output="+output+",selector="+selector+","+nEvtnSkip+",TestArea="+TestArea+" "+etp_batch_script;

  m_jobcommands << cmd << std::endl;

}
// --------------------------------------------------------------------------------------------------------------- //
TString SampleHandler::getDSIDFromSampleName(TString fullSample)
{

  TString datasetID = ((TObjString*)fullSample.Tokenize(".")->At(1))->String();
  return datasetID;

}
// --------------------------------------------------------------------------------------------------------------- //
void SampleHandler::checkMetaData(TString grid_sample,TString local_sample,TString histName,TString binName)
{

  // User has requested not to check the metaData
  if( m_disableMetaDataCheck ) return;

  // Get histogram with total number of events
  float nEvt_counter = getNumEventsCounterHist(local_sample,histName,binName);

  // Get number of events from AMI
  float nEvt_AMI = getNumEventsAMI(grid_sample);

  std::cout << "<SampleHandler::checkMetaData> INFO Checking sample " << grid_sample << "..." << std::endl;

  if( nEvt_counter == nEvt_AMI ){
    std::cout << "  >> Number of events against AMI >>> OKAY " << std::endl;
  }
  else{
    std::cout << "  >> Number of events against AMI >>> FAILED " << std::endl;
    printf("  >> ERROR Different number of events: AMI events %f; Counter histogram %f \n",nEvt_AMI,nEvt_counter);
    abort();
  }

}
// ----------------------------------------------------------------------------------------//
float SampleHandler::getNumEventsCounterHist(TString rootFileName,TString histName,TString binName)
{

  float nEvt_counterHist = 0;

  TH1* counterHist = getHist(rootFileName,histName);

  if( !counterHist ){
    std::cout << "<SampleHandler::getNumEventsCounterHist> Could not retrieve counter histogram: " << histName << std::endl;
    return nEvt_counterHist;
  }

  nEvt_counterHist = counterHist->GetBinContent( counterHist->GetXaxis()->FindBin( binName.Data() ) );

  // Return
  return nEvt_counterHist;

}
// ---------------------------------------------------------------------- //
TH1* SampleHandler::getHist(TString rootFileName,TString histName)
{

  TFile* file = new TFile(rootFileName,"READ");

  if( !file->IsOpen() ){
    std::cout << "<SampleHandler::getHist> WARNING Cannot open file : " << rootFileName << std::endl;
    return (TH1*)NULL;
  }

  // Get histogram
  TH1* hist = (TH1*)file->Get( histName.Data() );
  hist->SetDirectory(0);

  file->Close();

  return hist;

}
// ---------------------------------------------------------------------- //
int SampleHandler::getNumEventsAMI(TString sample)
{

  // pyAMI doesn't user sample scope identifier as rucio, infact will give an error
  if(sample.Contains(":")) sample.ReplaceAll("mc15_13TeV:","");

  // Get number of events
  TString command = TString::Format("ami show dataset info %s | grep totalEvents",sample.Data());
  TString numberOfEventsAMI = gSystem->GetFromPipe( command.Data() );

  numberOfEventsAMI.ReplaceAll(" ","");
  numberOfEventsAMI.ReplaceAll("totalEvents:","");

  return numberOfEventsAMI.Atoi();

}
// ---------------------------------------------------------------------- //
TString SampleHandler::getDSName(TString fullName,TString tag,TString sysSet,bool isData,int writeSkims,int writeTrees)
{

  // Examples of the formatting:
  //  -> mc14_13TeV:mc14_13TeV.110401.PowhegPythia_P2012_ttbar_nonallhad.merge.DAOD_SUSY5.e2928_s1982_s2008_r5787_r5853_p1815/
  //  -> data15_13TeV:data15_13TeV.periodE.physics_Main.PhysCont.DAOD_SUSY5.grp15_v01_p2667/

  //
  if(fullName.Contains(":")){
    fullName.ReplaceAll("mc%i_%iTeV:","");
  }

  // TODO: Robust check of the size!
  //       Check that whitespace isn't passed

  TString dataYear  = "";
  TString datasetID = "";
  TString process   = "";
  TString taglist   = "";

  TObjArray* tokens = fullName.Tokenize(".");
  dataYear  = ((TObjString*)tokens->At(0))->String();
  if (tokens->GetLast() >= 1) datasetID = ((TObjString*)tokens->At(1))->String();
  if (tokens->GetLast() >= 2) process   = ((TObjString*)tokens->At(2))->String();
  if (tokens->GetLast() >= 5) taglist   = ((TObjString*)tokens->At(5))->String();

  if (datasetID.IsWhitespace()) {
    MsgLog::ERROR("SampleHandler::getDSName","Can't parse datasetID from \"%s\"", fullName);
    abort();
  }
  if (process.IsWhitespace()) {
    MsgLog::ERROR("SampleHandler::getDSName","Can't parse process from \"%s\"", fullName);
    abort();
  }

  // Just DSID + process + tag + GroupSet
  TString partialName = "";

  // Add in year, since period containers
  // from different years could have the same
  // name otherwise!
  if( isData ){
    partialName = dataYear + "." + datasetID + "." + process + "_" + getDSNameSuffix(tag,sysSet,writeSkims,writeTrees);
  }
  else{
    if (taglist.IsWhitespace()) {
      MsgLog::ERROR("SampleHandler::getDSName","Can't parse taglist from \"%s\"", fullName);
      abort();
    }
    partialName = datasetID + "." + taglist + "_" + getDSNameSuffix(tag,sysSet,writeSkims,writeTrees);
  }

  //
  return partialName;

}
// ---------------------------------------------------------------------- //
TString SampleHandler::getDSNameSuffix(TString tag,TString sysSet, int writeSkims, int writeTrees)
{

  TString partialName = "";

  if( !tag.IsWhitespace()    ) partialName += tag;
  if( !sysSet.IsWhitespace() ) partialName += "_" + sysSet;

  if(writeSkims>0) partialName += TString::Format("_s%i",writeSkims);
  if(writeTrees>0) partialName += TString::Format("_t%i",writeTrees);

  return partialName;


}
// ---------------------------------------------------------------------- //
StatusCode SampleHandler::check(bool submitToGrid,TString groupSet,TString selector,bool checkAllSamples)
{

  const char* APP_NAME = "SampleHandler";

  //
  SampleInfo* set = getSampleInfo(groupSet);

  // Check the configuration files used in this group set
  //CHECK( CentralDB::checkConfig( packageName, set->deepConfigFile) );
  //CHECK( ConfigMgr::checkConfigFile( packageName, set->SUSYToolsConfig ) );

  for( auto& sysConfig : set->sysConfig ){

    int writeTrees = sysConfig->writeTrees;
    int writeSkims = sysConfig->writeSkims;

    // If user has a systematic set, check it is valid
    Systematics* sysClass = new Systematics();
    CHECK( sysClass->OpenFile( sysConfig->systematic ) );
    delete sysClass;

    // When a selector is not present, the user cannot run with writeTrees>0
    if( selector.IsWhitespace() && writeTrees>0 ){
      MsgLog::ERROR("SampleHandler::check","Request to write trees without a selector! Please specify a selector!");
      return StatusCode::FAILURE;
    }

    // Valid only up to 2
    if( writeSkims>2 ){
      MsgLog::ERROR("SampleHandler::check","Cannot run with writeSkims %i",writeSkims);
      return StatusCode::FAILURE;
    }

    // Valid only up to 3
    if( writeTrees>3 ){
      MsgLog::ERROR("SampleHandler::check","Cannot run with writeTrees %i",writeTrees);
      return StatusCode::FAILURE;
    }

    if( writeTrees>2 && writeSkims>0){
      MsgLog::ERROR("SampleHandler::check","Cannot run with writeTrees %i and writeSkims %i",writeTrees,writeSkims);
      return StatusCode::FAILURE;
    }

  } // Loop over SysConfig's

  // End of local checks
  if( !submitToGrid ) return StatusCode::SUCCESS;

  // Check for grid jobs only
  CheckGridSubmission* gridCheck = new CheckGridSubmission();
  CHECK( gridCheck->initialize() );

  if (m_disablePRWCheck) {
    gridCheck->skipPRWCheck(true);
  }

  // Loop over all samples and run them through CheckGridSubmission
  for(auto& sampleConfig : set->sampleConfig ){
    if( !gridCheck->checkSamples(sampleConfig->samples, sampleConfig->isData, sampleConfig->isAF2 ) ){
      //
      MsgLog::WARNING("SampleHandler::check","Process %s has improperly prepared samples!!",sampleConfig->process.Data() );

      // Don't proceed
      if( !checkAllSamples ){
        delete gridCheck;
        return StatusCode::FAILURE;
      }

    }
  }

  CheckGridSubmission::JobStatus* badJobs = gridCheck->getBadJobs();

  // Tell the user which samples are not ready
  if( badJobs->size()>0 ){
    MsgLog::WARNING("SampleHandler::check","Detected %i samples which are not ready for grid submission!!",badJobs->size() );

    // List them
    for( int b=0; b<badJobs->size(); b++ ){
      MsgLog::WARNING("SampleHandler::check","Sample FAILING checks %s",badJobs->samples[b].Data() );
      badJobs->printStatus(b);
    }

    //
    delete gridCheck;
    return StatusCode::FAILURE;

  }

  // All checks passed
  // Return gracefully
  delete gridCheck;
  return StatusCode::SUCCESS;

}
// ---------------------------------------------------------------------- //
bool SampleHandler::getInputFiles(TString directory,TString tag,TString sample,
				  SampleConfig*& sampleConfig, SampleSet*& sampleSet, bool doWarning)
{

  // Formatting for localTreeDir, or defaultTree Dir
  TString input = "";
  if( directory==m_localTreeDir ){
    input = TString::Format("%s/%s*.root",directory.Data(), sample.Data() );
  }
  else{
    input = TString::Format("%s/%s*.%s*/",directory.Data(), m_prefix.Data(), getDSName(sample,tag,"",sampleConfig->isData).Data() );
  }

  // List all input files, complain if they are not found
  if( gSystem->Exec( TString::Format("find -L %s -type f -name '*.root*' | sort > temp.txt",input.Data() ).Data() ) != 0 ){
    if( doWarning ){
      Warning("SampleHandler::getInputFiles","Could not find any samples for %s!!!!", sample.Data() );
    }
    return false;
  }

  std::ifstream file;

  // Open file
  file.open( "temp.txt" );

  if( !file.is_open() ){
    Error("SampleHandler::getInputFiles","Cannot open file temp.txt");
    return false;
  }

  bool any_samples = false;

  // Loop over samples
  TString line;
  while( file ){
    line.ReadLine(file, kTRUE);
    if( line.IsWhitespace() ) continue;
    any_samples = true;
    //
    if( sampleSet ){
      sampleSet->addMergeInput(line);
    }
  }

  // Clean up
  gSystem->Exec("rm -rf temp.txt");
  file.close();

  return any_samples;

}
// ---------------------------------------------------------------------- //
void SampleHandler::makeDirectoryStructure(TString user_path,TString tag,TString groupName)
{

  // Get true base directory, correct for any inconsistencies user passed
  getBaseDirectory(user_path,tag,groupName);

  // Base directory
  makeDirectory( user_path );

  // Add in groupSet
  makeDirectory( user_path + "/" + groupName );

  // Add in tag
  makeDirectory( user_path + "/" + groupName + "/" + tag );

  // Now one directory for trees,skims and merged output files
  makeDirectory( user_path + "/" + groupName + "/" + tag  + "/trees/" );
  makeDirectory( user_path + "/" + groupName + "/" + tag  + "/localTrees/" );
  makeDirectory( user_path + "/" + groupName + "/" + tag  + "/skims/" );
  makeDirectory( user_path + "/" + groupName + "/" + tag  + "/merged/" );
  makeDirectory( user_path + "/" + groupName + "/" + tag  + "/evtCounters/" );
  makeDirectory( user_path + "/" + groupName + "/" + tag  + "/sampleInfo/" );
  makeDirectory( user_path + "/" + groupName + "/" + tag  + "/PRW/" );

  // Set global flags
  m_baseDir       = user_path;
  m_treeDir       = user_path + "/" + groupName + "/" + tag  + "/trees/" ;
  m_localTreeDir  = user_path + "/" + groupName + "/" + tag  + "/localTrees/" ;
  m_skimDir       = user_path + "/" + groupName + "/" + tag  + "/skims/";
  m_mergeDir      = user_path + "/" + groupName + "/" + tag  + "/merged/";
  m_sampleInfoDir = user_path + "/" + groupName + "/" + tag  + "/sampleInfo/";
  m_prwDir        = user_path + "/" + groupName + "/" + tag  + "/PRW/";


}
// ---------------------------------------------------------------------- //
bool SampleHandler::makeDirectory(TString path)
{

  gSystem->mkdir( path.Data(), kTRUE );

  // Check it exists (false means it exists...)
  if( gSystem->AccessPathName( path.Data() ) ){
    std::cout << "<SampleHandler::makeDirectory> Could not make directory : " << path << std::endl;
    return false;
  }

  // Good
  return true;

}
// ---------------------------------------------------------------------- //
TString SampleHandler::formatOutputTTreeName(TString inputTree,TString process, bool isData)
{

  TString outTreeName = "";

  if( !inputTree.Contains("tree") ){
    outTreeName = process+"_"+inputTree;
  }
  else{
    outTreeName = inputTree.ReplaceAll("tree", process);
  }

  if( isData ){
    if( outTreeName.Contains("_NoSys") ){
      outTreeName = outTreeName.ReplaceAll("_NoSys", "");
    }
  }

  // For Higgsino samples: we want to merge multiple final states into one tree.
  // This obviously isn't ideal, but it works for now...
  outTreeName.ReplaceAll("MGPy8EG_A14N23LO_SM_C1C1_", "MGPy8EG_A14N23LO_SM_Higgsino_");
  outTreeName.ReplaceAll("MGPy8EG_A14N23LO_SM_N2C1m_","MGPy8EG_A14N23LO_SM_Higgsino_");
  outTreeName.ReplaceAll("MGPy8EG_A14N23LO_SM_N2C1p_","MGPy8EG_A14N23LO_SM_Higgsino_");
  outTreeName.ReplaceAll("MGPy8EG_A14N23LO_SM_N2N1_", "MGPy8EG_A14N23LO_SM_Higgsino_");

  // Similar for NUHM2 samples...
  if( outTreeName.Contains("MGPy8EG_A14N23LO_NUHM2_m12_") && outTreeName.Contains("_2LMET50_MadSpin") ){
    outTreeName.ReplaceAll("_C1C1_","_");
    outTreeName.ReplaceAll("_N2C1m_","_");
    outTreeName.ReplaceAll("_N2C1p_","_");
    outTreeName.ReplaceAll("_N2N1_","_");
  }

  // Similar for WinoBino samples...
  else if(  ( outTreeName.Contains("MGPy8EG_A14N23LO_C1mN2_WZ_") || outTreeName.Contains("MGPy8EG_A14N23LO_C1pN2_WZ_") )
         && ( outTreeName.Contains("_2L3MET50_MadSpin") || outTreeName.Contains("_3L_3L3_MadSpin") )  ){
    outTreeName.ReplaceAll("_C1mN2_","_");
    outTreeName.ReplaceAll("_C1pN2_","_");
  }

  return outTreeName;

}
// ---------------------------------------------------------------------- //
TString SampleHandler::formatMergeOutputFileName(TString name,TString sys,bool isTruthOnly)
{

  TString outputName = name;

  if( !outputName.Contains(".root") ){
    outputName += ".root";
  }

  if( isTruthOnly ){
    outputName.ReplaceAll(".root","_processed_truthOnly.root");
  }
  else{
    outputName.ReplaceAll(".root","_processed.root");
  }

  if( sys!="" ){
    outputName.ReplaceAll(".root","_"+sys+".root");
  }

  return outputName;
}
// ---------------------------------------------------------------------- //
void SampleHandler::getBaseDirectory(TString& user_path,TString tag,TString groupName)
{

  // Expand out any env variables
  user_path = gSystem->ExpandPathName( user_path.Data() );

  if( !user_path.EndsWith("/") ) user_path += "/";

  // Start data folders, remove any trees,skims, or merged paths
  if( user_path.EndsWith("trees/")  ) user_path.ReplaceAll("trees/","");
  if( user_path.EndsWith("skims/")  ) user_path.ReplaceAll("skims/","");
  if( user_path.EndsWith("merged/") ) user_path.ReplaceAll("merged/","");
  if( user_path.EndsWith("sampleInfo/") ) user_path.ReplaceAll("sampleInfo/","");

  // Remove any tags
  if( user_path.EndsWith( tag + "/" ) ) user_path.ReplaceAll(tag+"/","");

  // Remove any group sets
  if( user_path.EndsWith( groupName + "/" ) ) user_path.ReplaceAll( groupName+"/","" );

  gSystem->mkdir( user_path.Data(), kTRUE );

  // See if it exist, otherwise make it
  if( gSystem->AccessPathName( user_path.Data() ) ){
    std::cout << "<SampleHandler::getBaseDirectory> Could not find base directory : " << user_path << std::endl;
    abort();
  }

}
// ---------------------------------------------------------------------- //
int SampleHandler::getFinalState(int DatasetNumber)
{
  // Hack this for now - could read it from files or tree later
  if ((DatasetNumber > 371350) && (DatasetNumber <371790)) {
    return 2;
  }
  if ((DatasetNumber > 372600) && (DatasetNumber < 372795)) {
    return 4;
  }
  return 0;
}
// ---------------------------------------------------------------------- //
void SampleHandler::setPriorityTags(TString tags)
{

  //
  if( tags.IsWhitespace() ){
    return;
  }

  // Tokenize
  if( tags.Contains(",") ){
    TObjArray* token_tags = tags.Tokenize(",");

    // Get the list
    for(int i=0; i<=token_tags->GetLast(); i++){
      TString tag = ( (TObjString*)token_tags->At(i) )->String().Data();
      m_priorityTags.push_back(tag);

      //
      MsgLog::INFO("SampleHandler::setPriorityTags","Added a priority tag %s. Will be chosen over anything found with %s tag",tag.Data(),m_tag.Data());
    }

  }
  else{
    m_priorityTags.push_back(tags);
    MsgLog::INFO("SampleHandler::setPriorityTags","Added a priority tag %s. Will be chosen over anything found with %s tag",tags.Data(),m_tag.Data());
  }


}
// ---------------------------------------------------------------------- //
std::vector<TString> SampleHandler::getGroupSetList()
{

  // TODO: This won't work anymore as a static method. Will need to revist when updating the test methods

  std::vector<TString> groupSetList;
  groupSetList.clear();

  TString path = getPath("samples","$ROOTCOREBIN/../SusySkimMaker/data/","SusySkimMaker" );

  DIR* dp = opendir( path.Data() );
  if(dp){

    struct dirent * de;
    while ((de = readdir(dp)) != NULL) {

      // File name
      TString fileName (de->d_name);
      // Ignore hidden and backup files (~)
      if(fileName.BeginsWith(".")) continue;
      if(fileName.Contains("~")) continue;

      groupSetList.push_back(fileName);

    }
    closedir(dp);
  }

  return groupSetList;

}
// ---------------------------------------------------------------------- //
TString SampleHandler::findTagForSample(TString sample,SampleConfig*& sampleConfig)
{

  // If the user provided a list of tags with priority
  // choose these, but ensure that the sample exists with
  // that tag. Note that nothing is cached in SampleConfig
  // when checking for the input files

  TString sampleTag = "";
  SampleSet* nullSampleSet = 0;

  for( auto& t : m_priorityTags ){
    TString treeDir = m_baseDir + "/" + m_groupName + "/" + t  + "/trees/" ;
    MsgLog::INFO("SampleHandler::merge","Checking for tag %s...",t.Data() );
    if( getInputFiles(treeDir,t,sample,sampleConfig,nullSampleSet,false) ){
      MsgLog::INFO("SampleHandler::merge","Found sample %s with tag %s. Using instead of %s tag",sample.Data(),t.Data(),m_tag.Data()  );
      sampleTag = t;
    }
  }

  // Didn't find anything, set to global tag
  if( sampleTag=="" ) sampleTag = m_tag;

  //
  return sampleTag;

}
// ---------------------------------------------------------------------- //
TString SampleHandler::getPath(TString fileName,TString RootCorePath, TString PathResolverPath)
{

  TString fullPath = "";

  // First try with RootCore
  fullPath = RootCorePath;
  if( fileName!= "" ){
    fullPath = RootCorePath + "/" + fileName;
  }

  if( std::ifstream (gSystem->ExpandPathName(fullPath.Data())).good() ) {
    MsgLog::INFO("SampleHandler::getPath","Detected RootCoreBin path: %s",fullPath.Data() );
    return fullPath;
  }

  // Now try with PathResolver
  fullPath = PathResolverPath;
  if( fileName!= "" ){
    fullPath = PathResolverPath + "/" + fileName;
  }

  std::string confFile = PathResolverFindCalibFile( fullPath.Data() );
  if( std::ifstream (confFile).good() ) {
    fullPath = TString(confFile);
    MsgLog::INFO("SampleHandler::getPath","Detected file with PathResolver: %s",fullPath.Data() );
    return fullPath;
  }

  confFile = PathResolverFindCalibDirectory( fullPath.Data() );
  if( std::ifstream (confFile).good() ) {
    fullPath = TString(confFile) + "/";
    MsgLog::INFO("SampleHandler::getPath","Detected directory with PathResolver: %s",fullPath.Data() );
    return fullPath;
  }

  return fullPath;

}
// ---------------------------------------------------------------------- //
SampleHandler::~SampleHandler()
{

  // Clean up memory
  for( auto& it : m_sampleInfo ){
    delete it.second;
  }

  if( m_objectTools ) delete m_objectTools;

  if( m_DEC ){
    delete m_DEC;
    m_DEC = NULL;
  }

  if (m_jobcommands.is_open()) {
    m_jobcommands.close();
    std::cout << std::endl << std::endl                                 \
              << "Running on ETP:"                                      \
              << std::endl                                              \
              << "To submit the jobs go outside the singularity "       \
              << "environment and source jobcommands.sh"                \
              << std::endl << std::endl;
  }

}
