
// C++
#include <string>
#include <fstream>
#include <stdlib.h> 

// ROOT
#include "TSystem.h"
#include "TObjString.h"
#include "TObjArray.h"

// Fields
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/PkgDiscovery.h"
#include "SusySkimMaker/MsgLog.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "AsgMessaging/StatusCode.h"
#include "PathResolver/PathResolver.h"

// Static data members
static std::map<TString, std::vector<TString> > m_fields;

// ------------------------------------------------------------- //
CentralDB::CentralDB()
{

}
// ------------------------------------------------------------- //
void CentralDB::check()
{

  if( m_fields.size() > 0 ) return;
  else{
    LoadConfig();
  }

}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, TString& value)
{

  //
  check();

  TString _value = find( field );

  // Don't modify value if find returned empty string
  if(_value == ""){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is " << value << std::endl;
    return;
  }

  value = _value;

}  
// ------------------------------------------------------------- //
bool CentralDB::retrieve(const TString field)
{
  check();
  bool value = false;
  retrieve(field,value);
  return value;
}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, int& value)
{
  check();

  TString _value = find( field );

  // Don't modify value if find returned empty string
  if(_value == ""){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is " << value << std::endl;
    return;
  }

  value = atoi( _value );
}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, bool& value)
{
  check();

  TString _value = find( field );

  // Don't modify value if find returned empty string
  if(_value == ""){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is " << value << std::endl;
    return;
  }

  value = atoi( _value ) > 0 ? true : false;
}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, std::string& value)
{
  check();

  TString _value = find( field );

  // Don't modify value if find returned empty string
  if(_value == ""){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is " << value << std::endl;
    return;
  }

  value = (std::string) _value;
}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, float& value)
{
  check();

  TString _value = find( field );

  // Don't modify value if find returned empty string
  if(_value == ""){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is " << value << std::endl;
    return;
  }

  value = atof( _value );
}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, double& value)
{
  check();

  TString _value = find( field );

  // Don't modify value if find returned empty string
  if(_value == ""){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is " << value << std::endl;
    return;
  }

  value = atof( _value );
}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, std::vector<TString>& value)
{
  check();

  std::vector<TString> _value = findVec( field );

  // Don't modify value if findVec returned empty vector
  if(_value.empty()){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is the vector containing: ";
    for(auto str : value){
      std::cout << str << ", ";
    }
    std::cout << std::endl;

    return;
  }

  value.clear();
  value = _value;
}
// ------------------------------------------------------------- //
void CentralDB::retrieve(const TString field, std::vector<std::string>& value)
{
  check();
  std::vector<TString> _value = findVec( field );

  // Don't modify value if findVec returned empty vector
  if(_value.empty()){
    std::cout << "<CentralDB::retrieve> INFO Default value for " << field << " is the vector containing: ";
    for(auto str : value){
      std::cout << str << ", ";
    }
    std::cout << std::endl;

    return;
  }

  value.clear();
  for(auto& s : _value){
    value.push_back( (std::string)s);
  }
}
// ------------------------------------------------------------- //
std::vector<TString> CentralDB::findVec( const TString field )
{

 auto it = m_fields.find( field );

  if( it == m_fields.end() ){
    std::cout << "<CentralDB::findVec> WARNING Cannot find field : " << field << ". Using default value" << std::endl;
    return {};
  }
  else{
    return it->second;
  }

}
// ------------------------------------------------------------- //
TString CentralDB::find( const TString field )
{

  auto it = m_fields.find( field );

  if( it == m_fields.end() ){
    std::cout << "<CentralDB::find> WARNING Cannot find field : " << field << ". Using default value" << std::endl;
    return "";
  }
  else{

    if( it->second.size() == 0 ){
      std::cout << "<CentralDB::find> WARNING Could not find requested field" << ". Using default value" << std::endl;
      return "";
    }

    if( it->second.size() > 1 ){
      std::cout << "<CentralDB::find> WARNING more than one field found! Check configuration file, returning first one found" << std::endl;
    }

    return it->second[0];

  }

}
// ------------------------------------------------------------- //
void CentralDB::LoadConfig()
{

  // Here we should all all configurations
  ReadConfig();

}
// ------------------------------------------------------------- //
void CentralDB::ReadConfig()
{

  // Full path 
  TString path = gSystem->ExpandPathName( PkgDiscovery::getDeepConfigFullPath().Data() );
  MsgLog::INFO("CentralDB::ReadConfig","Searching for configuration file here: %s",path.Data());

  // Open default configuration file
  std::ifstream file ( path.Data() ) ;

  if( !file.is_open() ){
    MsgLog::ERROR("CentralDB::ReadConfig","Could not open CentralDB steering file %s",path.Data() );
    abort();
  }

  TString _field;
  while( file ){

    // Read line, skipping whitespace
    _field.ReadLine( file, kTRUE);

    // Comments or whitespace
    if( _field.Contains("#") || _field.IsWhitespace() ) continue;

    // Check for formatting
    if( !_field.Contains(":") ){
      std::cout << "Mis-formatted line!! Expect FIELD : VALUE, emphasis on the \":\" seperating the field from its value" << std::endl;;
      std::cout << "This line will not be loaded into the DB: " <<  _field.Data() << std::endl;      
      continue;
    }

    // Tokenize, remove any whitespac
    // TODO: Small memory leak in Tokenize. Need to clean up TOS
    TString field = ((TObjString*)_field.Tokenize(":")->At(0))->String();
    TString value = ((TObjString*)_field.Tokenize(":")->At(1))->String();
    value.ReplaceAll(" ","");
    field.ReplaceAll(" ","");

    if( value.Contains("$") ) value = gSystem->ExpandPathName( value.Data() ) ;

    // Cache
    auto it = m_fields.find( field );

    if( it == m_fields.end() ){
      std::vector<TString> vec;
      vec.clear();	
      vec.push_back( value );
      m_fields.insert( std::pair<TString,std::vector<TString>>( field, vec ) );
    }
    else{
     it->second.push_back( value );
    }
  }

  //
  file.close();

}
