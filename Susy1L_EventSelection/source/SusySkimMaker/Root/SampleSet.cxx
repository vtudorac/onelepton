
// RootCore includes
#include "SusySkimMaker/SampleSet.h"
#include "SusySkimMaker/MsgLog.h"

// ROOT includes
#include "TFile.h"
#include "TKey.h"
#include "TSystem.h"

SampleSet::SampleSet() : m_name(""),
                         isData(false),
                         isTruth(false),
                         isAF2(false),
                         lumi(1.0),
                         baseDir(""),
                         groupSet(""),
                         tag(""),
                         process(""),
                         deepConfig(""),
                         nEvtAMIMap()
{

}
// ----------------------------------------------- //
std::vector<TChain*> SampleSet::getTChainList(TString treeName)
{

  std::vector<TChain*> chains;
  std::vector<TString> sysList;

  if( treeName=="" ){
    sysList = getListOfTrees();
  }
  else{
    sysList.push_back(treeName);
  }

  // Loop over all trees, and make
  // the TChains!
  for( auto& sys : sysList ){

    TChain* chain = new TChain( sys.Data() );

    // Only add in files to the list that contribute
    // Note Add returns the number of files added into the TChain
    int nChains=0;
    for( auto& input : m_mergeInputs ){
      std::cout<<"Adding "<<input.Data()<<std::endl;
      int nFiles = chain->Add( input.Data(),-1 );

      if (nFiles == 0) {
        MsgLog::WARNING("SampleSet::getTChainList","Can't add %s to the TChain - check if the root file ends with \".root\" !!!",input.Data());
        continue;
      }

      nChains += nFiles;
    }

    //
    if( nChains>0 ){
      chains.push_back(chain);
    }
    else{
      delete chain;
    }

  }

  //
  return chains;

}
// ----------------------------------------------- //
TH1F* SampleSet::getTotalHist(TString histName)
{

  TH1F* totalHist = 0;

  for( auto& input : m_mergeInputs ){

    TFile* f = TFile::Open( input.Data(), "READ" );
    if( !f || !f->IsOpen() ){
      MsgLog::WARNING("SampleSet::getTotalHist","Cannot open file %s to get histogram!",input.Data() );
      continue;
    }

    TH1F* hist = dynamic_cast<TH1F*>( f->Get( histName.Data() ) );
    if( !hist ){
      MsgLog::WARNING("SampleSet::getTotalHist","Could not get histogram %s from file %s",histName.Data(),input.Data() );
      f->Close();
      continue;
    }

    if( !totalHist ){
      totalHist = dynamic_cast<TH1F*>( hist->Clone() );
      totalHist->Reset();
      totalHist->SetStats(0);
      totalHist->SetDirectory(0);
    }

    // TH1::Add cannot deal with merging bin labels,
    // so do it ourselves
    for(int binx=0;binx<=hist->GetNbinsX()+1;binx++){
      totalHist->Fill(hist->GetXaxis()->GetBinLabel(binx),hist->GetBinContent(binx));
    }

    f->Close();

  } // Loop over mergeInputs

  return totalHist;

}
// ----------------------------------------------- //
std::vector<TString> SampleSet::getListOfTrees()
{

  std::vector<TString> treeList;
  treeList.clear();

  // Extact all tree from all input files
  // Note: some can be empty, so it's much safer
  // to search all files and get a complete unique list
  for( auto& mergeInputFile : m_mergeInputs ){

    TFile* file = TFile::Open( mergeInputFile.Data(), "READ" );
    if( !file || !file->IsOpen() ){
      MsgLog::ERROR("SampleSet::getListOfTrees","Cannot open file %s for reading!!!",mergeInputFile.Data());
      continue;
    }

    // Loop over all trees, and ignore 
    // CutBookkeepers and MetaTree named 
    // TTrees
    TIter nextkey(file->GetListOfKeys());
    while( TKey* key = (TKey*)nextkey() ) {

      TString name = key->GetName();

      // Ignore meta data and cbks in this loop
      if(name == "CutBookkeepers") {
        continue;
      }
      if(name == "MetaTree") {
        continue;
      }  

      // Don't need duplicates 
      for( auto& t : treeList ){
        if( t==name ){
          name="";
          break; 
        }
      }
      if( name.IsWhitespace() ) continue;

      //
      if( strcmp(key->GetClassName(),"TTree") == 0 ){
        treeList.push_back(name);
      }
    }
    
    //
    file->Close();

  } // Loop over all merge inputs
 
  // Means that the user has absolutely no TTrees in their input files?
  if( treeList.size()==0 ){
    MsgLog::WARNING("SampleSet::getListOfTrees","Input files don't have any TTrees!");
  }

  //
  return treeList;

}
// ----------------------------------------------- //
void SampleSet::printMergeInputs()
{

  MsgLog::INFO("SampleSet::printMergeInput","Listing merge inputs for process %s",m_name.Data() );

  for( auto& mergeInput : m_mergeInputs ){
    std::cout << mergeInput << std::endl;
  }

}
// ----------------------------------------------- //
void SampleSet::printSampleInfo()
{

  MsgLog::INFO("SampleSet::printSampleInfo","Listing sample information for process %s",m_name.Data() );

  printf("Target luminosity: %f [pb] \n",lumi);
  for( auto& nAMI: nEvtAMIMap ){
    printf("Sample %s and number of events %i\n",nAMI.first.Data(),nAMI.second);
  }

}
// ----------------------------------------------- //
void SampleSet::write(TString directory)
{

  // First make sure directory exists
  gSystem->mkdir( directory.Data(), kTRUE );

  TFile* file = new TFile( directory + "/SampleSet_" + m_name + ".root", "RECREATE" );

  if( !file->IsOpen() ){
    MsgLog::ERROR("SampleSet::write","Cannot open file in %s directory for writing...",directory.Data() );
    return;
  }

  this->Write( m_name.Data() );

  file->Close();
  delete file;

}
// ----------------------------------------------- //
SampleSet::~SampleSet()
{
  nEvtAMIMap.clear();

}
