
// RootCore
#include "SusySkimMaker/HistMaker.h"

// C++
#include <iostream>

HistMaker::HistMaker() : m_file(0)
{
  m_hists.clear();
}
// ---------------------------------------------------- //
void HistMaker::addHist(TString name,int nBins, double min, double max)
{

  auto it = m_hists.find( name );

  if( it != m_hists.end() ){
    std::cout << "HistMaker::addHist> WARNING This histogram already exists: " << name << std::endl;
    return;
  }

  // Make
  TH1D* hist = new TH1D( name.Data(), name.Data(), nBins, min, max );

  // Associate to a particular rootfile
  if( m_file )
    hist->SetDirectory( m_file );

  // Save
  m_hists.insert( std::pair<TString,TH1*>( name,hist ) );
  
}
// ---------------------------------------------------- //
bool HistMaker::retrieve(TH1*& hist,TString name)
{

  auto h = m_hists.find( name );
  
  if( h == m_hists.end() ){
    std::cout << "<HistMaker::fillHist> No known hist : " << name << std::endl;
    return false;
  }

  hist = h->second;

  return true;
  
}
// ---------------------------------------------------- //
void HistMaker::fill(TString name, double value, double weight)
{

  TH1* hist = 0;
  if( !retrieve(hist,name) ) return;

  hist->Fill( value, weight );
  
}
// ---------------------------------------------------- //
void HistMaker::fill(TString name,TString value, double weight)
{

  TH1* hist = 0;
  if( !retrieve(hist,name) ) return;

  hist->Fill( value.Data(), weight );

}
// ---------------------------------------------------- //
void HistMaker::setBinLabel(TString name,int bin,TString label)
{

  TH1* hist = 0;
  if( !retrieve(hist,name) ) return;

  if( bin<0 || bin>hist->GetNbinsX() ){
    std::cout << ">HistMaker::setBinLabel> Bin " << bin << " outside x-axis range!" << std::endl;
    return;
  }

  // Do it!
  hist->GetXaxis()->SetBinLabel( bin,label.Data() );
  
}
// ---------------------------------------------------- //
bool HistMaker::checkHistExists(TString name)
{

  auto h = m_hists.find( name );

  // Doesn't exist
  if( h == m_hists.end() ){
    return false;
  }

  // It exists!
  return true;

}
// ---------------------------------------------------- //
void HistMaker::addOutputFile(TFile*& file)
{
  m_file = file;
}
// ---------------------------------------------------- //
HistMaker::~HistMaker()
{

  // Clean up all histogram pointers
  for( auto& hist : m_hists ){
    delete hist.second;
  }

  m_hists.clear();

}

