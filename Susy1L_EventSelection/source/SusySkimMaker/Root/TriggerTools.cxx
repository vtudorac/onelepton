// RootCore includes
#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "SusySkimMaker/TriggerTools.h"
#include "SusySkimMaker/StatusCodeCheck.h"
#include "SusySkimMaker/CentralDB.h"
#include "SusySkimMaker/CentralDBFields.h"
#include "SusySkimMaker/MsgLog.h"
#include "PathResolver/PathResolver.h"

// 
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTrigger/EnergySumRoI.h"
#include "xAODTrigger/JetRoIContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/ElectronContainer.h"

#include <iostream>
#include <fstream>

using namespace Trig;
using namespace TrigConf;
using namespace xAOD;

TriggerTools::TriggerTools( bool writeAllDecisions ) :  m_event(0), m_dbg(false), 
                                                        m_muDeltaR(0.15), 
                                                        m_eleDeltaR(0.15), 
                                                        m_jetDeltaR(0.4), 
                                                        m_phoDeltaR(0.15), 
                                                        m_disableMatching(false), 
                                                        m_configHandle(0), 
  m_configTool(0), 
  m_trigDecTool(0),
  m_convertFromMeV(1.00),
  m_met_L1(0),
  m_met_HLT_pufit(0),
  m_met_HLT_cell(0),
  m_leadJetPt_L1(0),
  m_leadJetPt_HLT(0),
  m_emulateJetTriggers(true),
  m_emulateMetTriggers(true),
  m_emulateHiggsinoTriggers(true),
  m_trigFileDir("SusySkimMaker"),
  m_writeTriggerPrescales(true),
  m_upstreamTriggerMatching(false),
  m_isRun3(false),
                                                        m_upstreamMatchingPrefix("TrigMatch_")
{

  // Empty all global maps
  Reset_perSys();
  Reset_perEvt();

  m_writeAllDecisions = writeAllDecisions;

}
// ------------------------------------------------------------------------------- //
StatusCode TriggerTools::initialize(xAOD::TEvent* event)
{

  const char* APP_NAME = "TriggerTools";

  // Get units from DB
  CentralDB::retrieve(CentralDBFields::UNITS,m_convertFromMeV);
  CentralDB::retrieve(CentralDBFields::ISRUN3, m_isRun3);
  // Paths
  // Allows the specification of which package to look for
  // the trigger lists, so in principle each package can contain 
  // there own instead of a central list
  CentralDB::retrieve(CentralDBFields::TRIGFILEDIR,m_trigFileDir);

 // Disable trigger offline matching requirement?
  CentralDB::retrieve(CentralDBFields::DISABLETRIGMATCH,m_disableMatching);


  // Init all needed tools
  if (m_trigDecTool.empty()) {
    std::string toolName = "TrigConfTool_NM";
    // The configuration tool.
    if ( asg::ToolStore::contains<TrigConf::xAODConfigTool>(toolName) ) {
      m_configTool = asg::ToolStore::get<TrigConf::xAODConfigTool>(toolName);
    } else {
      TrigConf::xAODConfigTool* configTool = new TrigConf::xAODConfigTool(toolName);
      CHECK( configTool->initialize() );
      m_configTool = configTool;
    }

  
    // The decision tool
    // TODO: Get tool from SUSYTools??
    toolName = "TrigDecisionTool";
    if ( asg::ToolStore::contains<Trig::TrigDecisionTool>(toolName) ) {
      std::cout<<"Found tool in ST"<<std::endl;
      m_trigDecTool = asg::ToolStore::get<Trig::TrigDecisionTool>(toolName);
    } else {
      Trig::TrigDecisionTool* trigDecTool = new Trig::TrigDecisionTool(toolName);
      CHECK( trigDecTool->setProperty("ConfigTool", m_configTool) );
      CHECK( trigDecTool->setProperty("TrigDecisionKey", "xTrigDecision") );
      CHECK( trigDecTool->setProperty("AcceptMultipleInstance", true) );
      CHECK( trigDecTool->initialize() );
      m_trigDecTool = trigDecTool;  
    }

    if(m_isRun3){
      toolName = "TrigDRScoringTool";
      if ( asg::ToolStore::contains<Trig::IMatchScoringTool>(toolName) ) {
        m_trigDRScoringTool = asg::ToolStore::get<Trig::IMatchScoringTool>(toolName);
      }//  else {
      //       std::cout<<"a"<<std::endl;
      //       Trig::R3MatchingTool* trigDRScoringTool = new Trig::R3MatchingTool(toolName);
      // std::cout<<"b"<<std::endl;
      //       //trigDRScoringTool->setTypeAndName("Trig::DRScoringTool/TrigDRScoringTool");
      //       CHECK( trigDRScoringTool->setProperty("OutputLevel", (int) MSG::WARNING) );
      // std::cout<<"c"<<std::endl;
      //       CHECK( trigDRScoringTool->setProperty("TrigDecisionTool",m_trigDecTool));
      // std::cout<<"d"<<std::endl;
      //       CHECK( trigDRScoringTool->initialize() );
      //       std::cout<<"getting drscore tool"<<std::endl;
      //       m_trigDRScoringTool = trigDRScoringTool;  

      //     }
    }
  }

  if(m_isRun3){
    std::string toolName = "TrigR3MatchingTool";
    if ( asg::ToolStore::contains<Trig::IMatchingTool>(toolName) ) {
      m_trigMatchingTool = asg::ToolStore::get<Trig::IMatchingTool>(toolName);
    }  }else if(m_upstreamTriggerMatching){
    std::string toolName = "TrigMatchFromCompositeTool";
    if ( asg::ToolStore::contains<Trig::IMatchingTool>(toolName) ) {
      m_trigMatchingTool = asg::ToolStore::get<Trig::IMatchingTool>(toolName);
    }
  }else{
    std::string toolName = "TrigMatchingTool";
    if ( asg::ToolStore::contains<Trig::IMatchingTool>(toolName) ) {
      m_trigMatchingTool = asg::ToolStore::get<Trig::IMatchingTool>(toolName);
    }
  }
  
  // The matching tool
  // if (m_trigMatchingTool.empty()) {
  //   std::string toolName = "TrigR3MatchingTool";
  //   if ( asg::ToolStore::contains<Trig::MatchingTool>(toolName) ) {
  //     m_trigMatchingTool = asg::ToolStore::get<Trig::MatchingTool>(toolName);
  //   } else {
  //     // if matching was performed upstream, different Matching Tool has to be used
  //     // implementation taken from https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/SUSYPhys/SUSYTools/Root/SUSYToolsInit.cxx
  //     // Note: ST has also to be configured accordingly so it would be more elegant
  //     // to just read out its flags which is however not possible ...
  //     // TODO: upstream matching is performed only for a subset of the triggers
  //     // so we have make a potential time-consuming check if the associated container
  //     // exists. Could maybe somehow check before main event loop for which triggers
  //     // the matching containers are present ...
  //     CentralDB::retrieve(CentralDBFields::TRIGGERUPSTREAMMATCHING, m_upstreamTriggerMatching);
  //     CentralDB::retrieve(CentralDBFields::TRIGGERMATCHINGPREFIX, m_upstreamMatchingPrefix);
  //     int matchToolMsgLvl = (int) MSG::WARNING;
  //     CentralDB::retrieve(CentralDBFields::TRIGMATCHVERBOSITY, matchToolMsgLvl);      
  //     if (!m_upstreamTriggerMatching){
  //       Trig::MatchingTool* matchTool = new Trig::MatchingTool(toolName);
  //       CHECK(matchTool->setProperty("TrigDecisionTool", m_trigDecTool));
  //       CHECK(matchTool->setProperty("OutputLevel", (MSG::Level) matchToolMsgLvl));      
  //       CHECK(matchTool->initialize() );
  //       m_trigMatchingTool = matchTool;
  //     }
  //     else {
  //       if (true) {
  //         Trig::R3MatchingTool* matchTool = new Trig::R3MatchingTool(toolName);
  //         CHECK( matchTool->setProperty("TrigDecisionTool", m_trigDecTool) );
  //         CHECK( matchTool->setProperty("ScoringTool", m_trigDRScoringTool) );
  //         m_trigMatchingTool = matchTool;
  //       }else{
  //         Trig::MatchFromCompositeTool* matchTool = new Trig::MatchFromCompositeTool(toolName);
  //         // CHECK(matchTool->setProperty("InputPrefix", ""));
  //         CHECK(matchTool->setProperty("OutputLevel", (MSG::Level) matchToolMsgLvl));      
  //         CHECK(matchTool->initialize() );
  //         //m_trigMatchingTool = matchTool;
  //         m_trigMatchingTool = matchTool;
  //       }
  //     }
  //   }
  // }

  // Set global TEvent object
  m_event = event;

  // Set whether or not to save the trigger prescales
  CentralDB::retrieve(CentralDBFields::WRITETRIGGERPRESCALES,m_writeTriggerPrescales);

  // All all user requested trigger into queue 
  loadTriggers();
  
  // Return gracefully
  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
void TriggerTools::loadTriggers()
{

  bool m_isrun3 = false;
  CentralDB::retrieve(CentralDBFields::ISRUN3,m_isrun3);
  if(m_isrun3){
    // Load HLT muon triggers
    getTriggerListFromFile("HLT_Muon_RUN3",this->m_HLT_muons); //_p5149
    // Load HLT electron triggers
    getTriggerListFromFile("HLT_Electron_RUN3",this->m_HLT_electrons); // _p5149
    // Load HLT Met triggers  
    getTriggerListFromFile("HLT_Met_RUN3",this->m_HLT_met);
  }else{
    // Load HLT muon triggers
    getTriggerListFromFile("HLT_Muon",this->m_HLT_muons); //_p5149
    // Load HLT electron triggers
    getTriggerListFromFile("HLT_Electron",this->m_HLT_electrons); // _p5149
    // Load HLT Met triggers  
    getTriggerListFromFile("HLT_Met",this->m_HLT_met);
  }
  
  
  /**
  // Load HLT Jet/Jet+Met triggers
  getTriggerListFromFile("HLT_Jet",this->m_HLT_jet);

  // Load HLT photon triggers
  getTriggerListFromFile("HLT_Photon",this->m_HLT_photons);
  
  // Load L1 trigger bits  
  getTriggerListFromFile("L1",this->m_L1);  
  */

  // ===== Trigger scale factors ====== //
  getTriggerListFromFile("SFs_Muon",this->m_SFs_muons);
  getTriggerListFromFile("SFs_Electron",this->m_SFs_electrons);
  
}
// -------------------------------------------------------------------------- //
void TriggerTools::getTriggerListFromFile(std::string fileName,std::vector<TString>& triggerNames)
{

  // M.G. Rel. 21 : Hard coded directory for the trigger lists
  // This should be OK for the future too, but when fully 
  // migrated toi Rel.21 we can all the user to configure which 
  // package to search for the trigger lists
  std::string path = PathResolverFindCalibFile( TString::Format("%s/%s",m_trigFileDir.Data(),fileName.c_str() ).Data() );

  // Open file
  std::ifstream file ( path );

  if( file.is_open() ){

    std::cout << std::endl;
    std::cout << "Adding trigger information..." << std::endl;
    std::cout << "  Path: " << path << std::endl;
    std::cout << "  Trigger (bit/SF) list" << std::endl;

    TString trigger;
    while( file ){

      // Read line
      trigger.ReadLine(file);
      if( trigger.Contains("#") || trigger.IsWhitespace() ) continue;

      // Not yet implemented...
      // // We will add in a set at runtime, when all the trigger chains have been loaded
      // if( trigger.Contains(".*") ) continue;

      // Save and remove whitespace around trigger name
      triggerNames.push_back(trigger.Strip(TString::kBoth));
      std::cout << "     - " << trigger << std::endl;
      
    }   
  }
  else{
    std::cout << "<TriggerTools::getTriggerListFromFile> WARNING: Cannot load trigger information for file: " << path << std::endl;
    return;  
  }
    
}
// -------------------------------------------------------------------------- //
void TriggerTools::LoadEvtTriggers()
{

  // HLT triggers
  LoadEvtHLTTriggers(m_HLT_electrons);
  LoadEvtHLTTriggers(m_HLT_muons);
  LoadEvtHLTTriggers(m_HLT_met);
  LoadEvtHLTTriggers(m_HLT_photons);
  LoadEvtHLTTriggers(m_HLT_jet);  
  
  // Trigger Prescales
  if(m_writeTriggerPrescales){
    LoadEvtPrescales(m_HLT_electrons);
    LoadEvtPrescales(m_HLT_muons); 
    LoadEvtPrescales(m_HLT_met); 
    LoadEvtPrescales(m_HLT_jet);
    LoadEvtPrescales(m_HLT_photons);
  }

  // Level 1 triggers
  LoadEvtL1Triggers(m_L1);

}
// -------------------------------------------------------------------------- //
StatusCode TriggerTools::emulateMETTriggers()
{

  if( !m_emulateMetTriggers ){
    return StatusCode::SUCCESS;
  }

  //retrieve L1 met container 
  const xAOD::EnergySumRoI* l1MetObject(0);
  //retrieve hlt met container
  const xAOD::TrigMissingETContainer* mhtCont(0); //Directly from TWiki

  //If containers don't exist, return success and do not perform emualation 
  if( !m_event->retrieve(l1MetObject, "LVL1EnergySumRoI" ).isSuccess() || !m_event->retrieve(mhtCont, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht" ).isSuccess() ){
      MsgLog::WARNING("TriggerTools::emulateMETTriggers","Cannot emulate met triggers. L1 or HLT containers do not exits");
      m_emulateMetTriggers = false;
      return StatusCode::SUCCESS;
  }
  else{

    // Check the mhtCont has something, else the access below gives a seg fault
    if( mhtCont->size()==0 ){
      return StatusCode::SUCCESS;
    }

    //Met L1 trigger decision
    float l1_mex = l1MetObject->exMiss() * 0.001; //exMiss in MeV, trigger threshold is in GeV
    float l1_mey = l1MetObject->eyMiss() * 0.001;
    float l1_met = sqrt(l1_mex*l1_mex + l1_mey*l1_mey);
 
    //MET HLT trigger decision
    float hltMet = 0.;
    //For l1_met <= 35, no hltMet container is produced, set to 0
    if (l1_met <= 35.){
        hltMet = 0.;
    }
    else{
    //if( mhtCont ){
      float ex = mhtCont->front()->ex() * 0.001;
      float ey = mhtCont->front()->ey() * 0.001;
      hltMet = sqrt(ex*ex + ey*ey);
    }

    //Combined MET trigger decisions
    const bool HLT_xe50_mht_L1XE20 = hltMet > 50. && l1_met > 20;
    const bool HLT_xe90_mht_L1XE40 = hltMet > 90. && l1_met > 40;

    //insert met triggers into HLT map
    m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_xe50_mht_L1XE20", HLT_xe50_mht_L1XE20) );
    m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_xe90_mht_L1XE40", HLT_xe90_mht_L1XE40) );
  }

  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
StatusCode TriggerTools::emulateJetTriggers()
{

  if( !m_emulateJetTriggers ){
    return StatusCode::SUCCESS;
  }

  //retrieve L1 Jet container
  const xAOD::JetRoIContainer* l1_jet = 0;
  //retrieve HLT jet container
  const xAOD::JetContainer* hlt_jet = 0;
  //If containers don't exist, return success and do not perform emualation 
  if( !m_event->retrieve(l1_jet, "LVL1JetRoIs" ).isSuccess() || !m_event->retrieve(hlt_jet, "HLT_xAOD__JetContainer_a4tcemsubjesFS" ).isSuccess()  ) {
    MsgLog::WARNING("TriggerTools::emulateJetTriggers","Cannot emulate jet triggers. L1 or HLT containers do not exits");
    m_emulateJetTriggers = false;
    return StatusCode::SUCCESS;
  } 
  else{
    //Jet L1 Trigger Decision
    std::vector<float> L1JetPt40, L1JetPt50; //vectors containing pt of L1 jets that pass trigger threshold
    //clear vectors each event
    L1JetPt40.clear();
    L1JetPt50.clear();
    xAOD::JetRoIContainer::const_iterator L1jet_itr = l1_jet->begin();
    xAOD::JetRoIContainer::const_iterator L1jet_end = l1_jet->end();
    for( ; L1jet_itr != L1jet_end; ++L1jet_itr ) {
      if ( (*L1jet_itr)->et8x8() * 0.001 > 40.) { //_L1J40
        float L1jPt40 = (*L1jet_itr)->et8x8() * 0.001;
        L1JetPt40.push_back( L1jPt40);
        if ((*L1jet_itr)->et8x8() * 0.001 > 50.) { //_L1J50
          float L1jPt50 = (*L1jet_itr)->et8x8() * 0.001;
          L1JetPt50.push_back( L1jPt50);
        }
      }
    }

    //Jet HLT Trigger Decision
    std::vector<float> HLTJetPt85, HLTJetPt125; //vectors containing pt of HLT jets that pass trigger threshold
    //clear vectors each event
    HLTJetPt85.clear();
    HLTJetPt125.clear();
    const xAOD::Jet *hlt_jet_obj = 0;
    for(unsigned int HLTjet_itr = 0; HLTjet_itr < hlt_jet->size(); ++HLTjet_itr){
      hlt_jet_obj = hlt_jet->at(HLTjet_itr);
      if ( hlt_jet_obj->pt() * 0.001 > 85.) {
        HLTJetPt85.push_back( hlt_jet_obj->pt() * 0.001);  
        if ( hlt_jet_obj->pt() * 0.001 > 125.) {
          HLTJetPt125.push_back( hlt_jet_obj->pt() * 0.001 );
        }
      }
    }

    //Combined jet trigger decisions
    const bool HLT_j85_L1J40 = HLTJetPt85.size() > 0 && L1JetPt40.size() > 0;
    const bool HLT_j125_L1J50 = HLTJetPt125.size() > 0 && L1JetPt50.size() > 0;

    //insert jet triggers into HLT trigger map
    m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_j85_L1J40", HLT_j85_L1J40) ); 
    m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_j125_L1J50", HLT_j125_L1J50) ); 
  }

/*
  std::cout << "begin loop in trigger tools" << std::endl;
  std::map<TString, bool>::iterator iter;
  for ( iter = m_HLT_evtDecision.begin(); iter!=m_HLT_evtDecision.end(); iter++){
      std::cout << "***" << std::endl;
      std::cout << iter->first << ":" << iter->second << std::endl;
  }
  std::cout << "END loop in trigger tools" << std::endl;
*/  

  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
StatusCode TriggerTools::emulateHiggsinoTriggers()
{

  if( !m_emulateHiggsinoTriggers ){
    return StatusCode::SUCCESS;
  }

  // try to emulate
  // HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30
  // HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI-J20s2XE30
  // HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30
  // HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50
  // HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50

  //retrieve L1 and HLT met containers
  const xAOD::EnergySumRoI* l1MetObject(0);
  const xAOD::TrigMissingETContainer* pufitCont(0);

  //If containers don't exist, return and do not perform emualation
  if( !m_event->retrieve(l1MetObject, "LVL1EnergySumRoI" ).isSuccess() || !m_event->retrieve(pufitCont, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_topocl_PUC" ).isSuccess() ){
    MsgLog::WARNING("TriggerTools::emulateHiggsinoTriggers","Cannot emulate jet triggers. L1 or HLT containers do not exist");
    m_emulateHiggsinoTriggers = false;
    return StatusCode::SUCCESS;
  }

  //retrieve L1 and HLT jet containers
  const xAOD::JetRoIContainer* l1_jet = 0;
  const xAOD::JetContainer* hlt_jet = 0;

  if( !m_event->retrieve(l1_jet, "LVL1JetRoIs" ).isSuccess() || !m_event->retrieve(hlt_jet, "HLT_xAOD__JetContainer_a4tcemsubjesFS" ).isSuccess()  ) {
    MsgLog::WARNING("TriggerTools::emulateHiggsinoTriggers","Cannot emulate MET triggers. L1 or HLT containers do not exist");
    m_emulateHiggsinoTriggers = false;
    return StatusCode::SUCCESS;
  }

  // get L1 MET
  float l1_mex = l1MetObject->exMiss(); // exMiss in MeV
  float l1_mey = l1MetObject->eyMiss();
  float l1_met = sqrt(l1_mex*l1_mex + l1_mey*l1_mey);
  float l1_met_phi = atan2(l1_mey, l1_mex);
  if (l1MetObject->energyT()>0) {
    l1_met_phi = atan2(l1_mey, l1_mex);
  }
  else {
    l1_met = -999;
    l1_met_phi = -999;
  }
  TLorentzVector l1met_vector;
  l1met_vector.SetPtEtaPhiE(l1_met,0,l1_met_phi,l1_met);

  // get HLT MET
  float hlt_mex = pufitCont->front()->ex();
  float hlt_mey = pufitCont->front()->ey();
  float hlt_met = sqrt(hlt_mex*hlt_mex + hlt_mey*hlt_mey);
  float hlt_met_phi = atan2(hlt_mey, hlt_mex);
  TLorentzVector hltmet_vector;
  hltmet_vector.SetPtEtaPhiE(hlt_met,0,hlt_met_phi,hlt_met);

  // get leading and subleading L1 jet
  TLorentzVector leading_l1jet;
  leading_l1jet.SetPtEtaPhiM(0,0,0,0);
  TLorentzVector sublead_l1jet;
  sublead_l1jet.SetPtEtaPhiM(0,0,0,0);
  TLorentzVector thisjet;
  for (auto p : * l1_jet) {
    //L1 jet with pT > 20 GeV, and |Eta| < 3.1
    thisjet.SetPtEtaPhiM(p->et8x8(),p->eta(),p->phi(),0);
    if(TMath::Abs(p->eta())<3.1){
      if(p->et8x8() > leading_l1jet.Pt()){
        sublead_l1jet=leading_l1jet;
        leading_l1jet = thisjet;
      }
      else if(p->et8x8() > sublead_l1jet.Pt()){
        sublead_l1jet=thisjet;
      }
    }
  }

  // get leading and subleading HLT jet
  const xAOD::Jet* ldg = 0;
  const xAOD::Jet* subldg = 0;
  for (auto j : *hlt_jet){
    // require pT > 20 GeV and |eta| < 3.2 according to
    // https://svnweb.cern.ch/trac/atlasoff/browser/Trigger/TrigHypothesis/TrigHLTJetHypo/trunk/src/TrigEFDPhiMetJetAllTE.cxx
    if ( j->pt() < 20e3 || TMath::Abs(j->eta()) > 3.2 ){
      continue;
    }
    if (!ldg || j->pt() > ldg->pt()) {
      subldg = ldg;
      ldg = j;
    }
    else if (!subldg || j->pt() > subldg->pt()){
      subldg = j;
    }
  }
  TLorentzVector leading_hltjet;
  TLorentzVector sublead_hltjet;
  if (ldg) {
    leading_hltjet = TLorentzVector(ldg->p4());
  }
  if (subldg){
    sublead_hltjet = TLorentzVector(subldg->p4());
  }

  // get L1Topo decision
  float minDPHI_2J20XE=9999;
  if(leading_l1jet.Pt()>20e3 && fabs(leading_l1jet.DeltaPhi(l1met_vector))<minDPHI_2J20XE) minDPHI_2J20XE=fabs(leading_l1jet.DeltaPhi(l1met_vector));
  if(sublead_l1jet.Pt()>20e3 && fabs(sublead_l1jet.DeltaPhi(l1met_vector))<minDPHI_2J20XE) minDPHI_2J20XE=fabs(sublead_l1jet.DeltaPhi(l1met_vector));
  bool L1_DPHI_2J20XE = (minDPHI_2J20XE>=1.0);

  // similar on HLT level
  float min2dphi10=9999;
  if(leading_hltjet.Pt()>20e3 && fabs(leading_hltjet.DeltaPhi(hltmet_vector))<min2dphi10) min2dphi10=fabs(leading_hltjet.DeltaPhi(hltmet_vector));
  if(sublead_hltjet.Pt()>20e3 && fabs(sublead_hltjet.DeltaPhi(hltmet_vector))<min2dphi10) min2dphi10=fabs(sublead_hltjet.DeltaPhi(hltmet_vector));
  bool HLT_2dphi10 = (min2dphi10>=1.0);

  // try to emulate HLT_2mu4_invm1
  bool HLT_2mu4_invm1 = false;
  if (getHLTDecFromMap("HLT_2mu4")) {
    auto cg = m_trigDecTool->getChainGroup("HLT_2mu4");
    auto fc = cg->features();
    auto muFeatureContainers = fc.containerFeature<xAOD::MuonContainer>();

    float minv_max = 0;
    // loop over all muon containers
    for (unsigned int c1=0; c1<muFeatureContainers.size(); c1++) {
      // loop over all muons in this container
      for (unsigned int m1=0; m1<muFeatureContainers.at(c1).cptr()->size(); m1++){
        auto muon1 = muFeatureContainers.at(c1).cptr()->at(m1);
        // loop over all other muon containers
        for (unsigned int c2=c1+1; c2<muFeatureContainers.size(); c2++){
          // loop over all muons in this other container
          for (unsigned int m2=0; m2<muFeatureContainers.at(c2).cptr()->size(); m2++){
            auto muon2 = muFeatureContainers.at(c2).cptr()->at(m2);
            float minv = (muon1->p4()+muon2->p4()).M()/1000.0;
            // check if this pairing has the largest invariant mass
            minv_max = std::max(minv, minv_max);
          }
        }
      }
    }
    if (minv_max > 1){
      HLT_2mu4_invm1 = true;
    }
  }
  m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_2mu4_invm1_emul", HLT_2mu4_invm1) );

  // Now check the combined triggers:

  // HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI-J20s2XE30
  bool HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul = false;
  if (HLT_2mu4_invm1 && leading_hltjet.Pt()>20e3 && hlt_met>40e3 && HLT_2dphi10 && leading_l1jet.Pt()>20e3 && l1_met>30e3 && L1_DPHI_2J20XE) {
    HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul = true;
  }

  // HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI-J20s2XE30
  bool HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul = false;
  if (getHLTDecFromMap("HLT_mu4") && leading_hltjet.Pt()>90e3 && hlt_met>90e3 && HLT_2dphi10 && leading_l1jet.Pt()>50e3 && l1_met>50e3 && L1_DPHI_2J20XE) {
    HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul = true;
  }

  // FIXME once we have the right triggers in SUSY16...
  // HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI-J20s2XE30
  // sadly HLT_e5_lhvloose_nod0 is not in the TriggerList for SUSY16,
  // so we ask for HLT_e5_lhmedium_nod0_mu4_xe40_mht_L1MU4_J20_XE30_DPHI-J20sXE30 instead
  bool HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul = false;
  if (getHLTDecFromMap("HLT_e5_lhmedium_nod0_mu4_xe40_mht_L1MU4_J20_XE30_DPHI-J20sXE30") && getHLTDecFromMap("HLT_mu4") && leading_hltjet.Pt()>30e3 && hlt_met>40e3 && HLT_2dphi10 && leading_l1jet.Pt()>30e3 && l1_met>40e3 && L1_DPHI_2J20XE) {
    HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul = true;
  }

  // FIXME once we have the right triggers in SUSY16...
  // HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50
  // sadly HLT_2e5_lhvloose_nod0 is not in the trigger menu, so we ask for
  // HLT_2e5_lhmedium_nod0_j50_xe80_mht_L1J40_XE50_DPHI-J20sXE50 instead
  bool HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul = false;
  if (getHLTDecFromMap("HLT_2e5_lhmedium_nod0_j50_xe80_mht_L1J40_XE50_DPHI-J20sXE50") && leading_hltjet.Pt()>40e3 && hlt_met>70e3 && HLT_2dphi10 && leading_l1jet.Pt()>40e3 && l1_met>50e3 && L1_DPHI_2J20XE) {
    HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul = true;
  }

  // FIXME once we have the right triggers in SUSY16...
  // HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI-J20s2XE50
  // sadly HLT_e5_lhloose_nod0 is not in the TriggerList for SUSY16
  // no other trigger usable for emulation :/
  bool HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul = false;
  if (getHLTDecFromMap("HLT_e5_lhloose_nod0") && leading_hltjet.Pt()>50e3 && hlt_met>70e3 && HLT_2dphi10 && leading_l1jet.Pt()>40e3 && l1_met>50e3 && L1_DPHI_2J20XE) {
    HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul = true;
  }

  //insert Higgsino triggers into HLT trigger map
  m_L1_evtDecision.insert( std::pair<TString,bool>("L1_DPHI_2J20XE_emul", L1_DPHI_2J20XE) );
  m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_2dphi10_emul", HLT_2dphi10) );
  m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul", HLT_2mu4_invm1_j20_xe40_pufit_2dphi10_L12MU4_J20_XE30_DPHI_J20s2XE30_emul) ); 
  m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul", HLT_mu4_j90_xe90_pufit_2dphi10_L1MU4_J50_XE50_DPHI_J20s2XE30_emul) ); 
  m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul", HLT_e5_lhvloose_nod0_mu4_j30_xe40_pufit_2dphi10_L1MU4_J30_XE40_DPHI_J20s2XE30_emul) ); 
  m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul", HLT_2e5_lhvloose_nod0_j40_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul) ); 
  m_HLT_evtDecision.insert( std::pair<TString,bool>("HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul", HLT_e5_lhloose_nod0_j50_xe70_pufit_2dphi10_L1J40_XE50_DPHI_J20s2XE50_emul) ); 

  m_met_L1 = l1_met * m_convertFromMeV;
  m_met_HLT_pufit = hlt_met * m_convertFromMeV;
  m_leadJetPt_L1 = leading_l1jet.Pt() * m_convertFromMeV;
  m_leadJetPt_HLT = leading_hltjet.Pt() * m_convertFromMeV;
  
  // add the HLT cell-MET for some trigger optimization studies
  const xAOD::TrigMissingETContainer* cellCont(0);
  if (!m_event->retrieve(cellCont, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET" ).isSuccess()) {
    MsgLog::WARNING("TriggerTools::emulateHiggsinoTriggers","Cannot emulate cell MET. HLT container does not exist");
    return StatusCode::SUCCESS;
  }

  float ex_cell=0;
  float ey_cell=0;
  if(cellCont->front()){
    ex_cell = cellCont->front()->ex();
    ey_cell = cellCont->front()->ey();
  }
  m_met_HLT_cell = sqrt(ex_cell*ex_cell + ey_cell*ey_cell) * m_convertFromMeV;  

  return StatusCode::SUCCESS;

}
// -------------------------------------------------------------------------- //
void TriggerTools::LoadEvtHLTTriggers(std::vector<TString> HLTChains)
{

  for( const auto& trigChainName : HLTChains ){
    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );
    //
    bool isPassed = cg->isPassed(); 
    m_HLT_evtDecision.insert( std::pair<TString,bool>(trigChainName,isPassed) );
  }
  
}
// -------------------------------------------------------------------------- //
void TriggerTools::LoadEvtL1Triggers(std::vector<TString> L1Chains)
{

  for( const auto& trigChainName : L1Chains ){
    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );
    //
    bool isPassed = cg->isPassed(); 
    m_L1_evtDecision.insert( std::pair<TString,bool>(trigChainName,isPassed) );
  }
  
}
// -------------------------------------------------------------------------- //
void TriggerTools::LoadEvtPrescales(std::vector<TString> HLTChains)
{

  for( const auto& trigChainName : HLTChains ){
    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );
    float prescale = cg->getPrescale();
    //std::cout << "Ruo, prescale = " << prescale << std::endl;
    m_prescale.insert( std::pair<TString,float>(trigChainName,prescale) );
  }

}
// -------------------------------------------------------------------------- //
const std::map<TString,bool> TriggerTools::matchMetTriggers()
{

  std::map<TString,bool> metTrigDec;
  metTrigDec.clear();

  for( const auto& trigChainName : m_HLT_met ){
    //
    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );
    // 
    bool isPassed = cg->isPassed(); 
    // Only save triggers that passed
    if( !isPassed && !m_writeAllDecisions ) continue;
    metTrigDec.insert( std::pair<TString,bool>(trigChainName,isPassed) );
  }

  return metTrigDec;

}
// -------------------------------------------------------------------------- //
const std::map<TString,bool> TriggerTools::matchLeptonTriggers(const xAOD::IParticle* p, ST::SUSYObjDef_xAOD& susyObj)
{

  // Check if this is a muon:
  if( p->type() == xAOD::Type::Muon ) {

    // Cast it to a muon
    const xAOD::Muon* offlineMuon = dynamic_cast< const xAOD::Muon* >( p );

    // Do the matching for all trigger bits and return the decision
    return matchMuonTriggers(offlineMuon,susyObj);

  }
  else if( p->type() == xAOD::Type::Electron ){
   
    // Cast it to a electron
    const xAOD::Electron* offlineElectron = dynamic_cast< const xAOD::Electron* >( p );

    // Do the matching for all trigger bits and return the decision
    return matchElectronTriggers(offlineElectron,susyObj);
 
  }
  else{
    std::cout << "<TriggerTools::matchLeptonTriggers> WARNING Unknown particle type, returning an empty trigger map!" << std::endl;
    std::map<TString,bool> empty;
    empty.clear();
    return empty;
  }

}
// -------------------------------------------------------------------------- //
const std::map<TString,bool> TriggerTools::matchMuonTriggers(const xAOD::Muon* offlineMuon, ST::SUSYObjDef_xAOD& susyObj)
{

  if( !offlineMuon ){
    std::cout << "<TriggerTools::matchMuonTriggers> FATAL ERROR Could not cast particle to muon" << std::endl;
    abort();
  }

  std::map<TString,bool> muonTrigDec;
  muonTrigDec.clear();

  bool isDAODPHYS = false;
  bool passTM=false;
  TString kernel = "";
  CentralDB::retrieve(CentralDBFields::DXAODKERNEL, kernel);
  if(kernel.Contains("PHYS"))isDAODPHYS = true;
  if(isDAODPHYS){
    // Loop over all trigger bits
    for( const auto& trigChainName : this->m_HLT_muons ){
      //printf("MUON: Kernel is %s, doing matching of %s\n",kernel.Data(),trigChainName.Data());
      passTM=false;
      std::string trigname;
      trigname += trigChainName.Data();
      //if(!susyObj.IsTrigPassed(trigname))continue;
      // Link is broken in mc21, uncomment trigger matching for now9
      
      passTM = (susyObj.IsTrigPassed(trigname) && susyObj.IsTrigMatched(offlineMuon, trigname));
      //printf("MUON: Trigger %s : %s\n",kernel.Data(),(passTM ? "PASSED" : "FAILED"));
      if(passTM)
        muonTrigDec.insert( std::pair<TString,bool>(trigChainName,passTM) );
    }
    return muonTrigDec;
  }
  
  
  // Loop over all trigger bits
  for( const auto& trigChainName : this->m_HLT_muons ){

    //std::cout<<"Looking at trigger "<<trigChainName<<std::endl;

    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );

    // Check if it passed: Event level bit
    bool isPassed = cg->isPassed(); 
    //
    if(m_dbg) std::cout << "Muon trigger: " << trigChainName << " : " << isPassed << std::endl;

    // Save nothing if it didn't pass
    if( !isPassed && !m_writeAllDecisions ) continue;

    if( m_disableMatching ) continue;

    // Do matching
    bool onlineMatch = m_trigMatchingTool->match(*offlineMuon, (std::string)trigChainName, m_muDeltaR);
    /*
    // Match to this particle
    bool onlineMatch=false;
    auto fc = cg->features();
    auto muFeatureContainers = fc.containerFeature<MuonContainer>();
    for(auto mucont : muFeatureContainers) {
      for (auto onlineMuon : *mucont.cptr()) {

	//
	TLorentzVector online;
	TLorentzVector offline;
	
	if(m_dbg){
	  printf("Trigger object offline pt,eta,phi (%f,%f,%f) \n",offlineMuon->pt(),offlineMuon->eta(),offlineMuon->phi() );
	  printf("Trigger object online pt,eta,phi (%f,%f,%f)  \n",onlineMuon->pt(),onlineMuon->eta(),onlineMuon->phi() );
	}
	
	// Set TLVs
	online.SetPtEtaPhiM( onlineMuon->pt(),onlineMuon->eta(),onlineMuon->phi(),onlineMuon->m() );
	offline.SetPtEtaPhiM( offlineMuon->pt(),offlineMuon->eta(),offlineMuon->phi(),offlineMuon->m() );
	
	// Find DeltaR between them
	float deltaR = online.DeltaR(offline);
	
	// Check if satisfies our criteria
	if(deltaR<m_muDeltaR){
	  if(m_dbg) printf("Found a online to offline muon match, deltaR == %f, and required to be deltaR<%f \n",deltaR,m_muDeltaR); 
	  onlineMatch=true;
	  break;
	}

      } // Online muons
    } // FeatureContainers
    */

    if(onlineMatch){
      // Save trigger matching result		      
      muonTrigDec.insert( std::pair<TString,bool>(trigChainName,onlineMatch) );
      //
      if(m_dbg) std::cout << "Found a matched muon!" << std::endl;
    }

  } // Trigger bits

  return muonTrigDec;

}


// -------------------------------------------------------------------------- //
const std::map<TString,bool> TriggerTools::matchJetTriggers(const xAOD::Jet* offlineJet=0)
{

  // Not sure if this is the correct way to do jet trigger matching
  // Seems to give results for HLT_j300, HLT_3j175 but nothing else
  // Only matches one jet

  if( !offlineJet ){
    std::cout << "TriggerTools::matchJetTriggers FATAL ERROR Could not cast particle to jet" << std::endl;
    abort();
  }

  std::map<TString,bool> jetTrigDec;
  jetTrigDec.clear();

  return jetTrigDec;

  // Loop over all trigger bits
  for( const auto& trigChainName : this->m_HLT_jet ){

    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );

    // Check if it passed: Event level bit
    bool isPassed = cg->isPassed();
    //
    if(m_dbg) std::cout << "Jet trigger: " << trigChainName << " : " << isPassed << std::endl;

    // Save nothing if it didn't pass
    if( !isPassed && !m_writeAllDecisions ) continue;

    if( m_disableMatching ) continue;

    // Match to this particle
    bool onlineMatch=false;
    auto fc = cg->features();
    auto jetFeatureContainers = fc.containerFeature<JetContainer>();
    for(auto jetcont : jetFeatureContainers) {
      for (auto onlineJet : *jetcont.cptr()) {

        //
        TLorentzVector online;
        TLorentzVector offline;

        if(m_dbg){
          printf("Trigger object for %s offline pt,eta,phi (%f,%f,%f) \n",trigChainName.Data(),offlineJet->pt(),offlineJet->eta(),offlineJet->phi() );
          printf("Trigger object for %s online pt,eta,phi (%f,%f,%f)  \n",trigChainName.Data(),onlineJet->pt(),onlineJet->eta(),onlineJet->phi() );
        }

        // Set TLVs
        online.SetPtEtaPhiM( onlineJet->pt(),onlineJet->eta(),onlineJet->phi(),onlineJet->m() );
        offline.SetPtEtaPhiM( offlineJet->pt(),offlineJet->eta(),offlineJet->phi(),offlineJet->m() );

        // Find DeltaR between them
        float deltaR = online.DeltaR(offline);

        // Check if satisfies our criteria
        if(deltaR<m_jetDeltaR){
          if(m_dbg) printf("Found a online to offline jet match for %s, deltaR == %f, and required to be deltaR<%f \n",trigChainName.Data(),deltaR,m_jetDeltaR);
          onlineMatch=true;
          break;
        }

      } // Online jets
    } // FeatureContainers

    if(onlineMatch){
      // Save trigger matching result
      jetTrigDec.insert( std::pair<TString,bool>(trigChainName,onlineMatch) );
      //
      if(m_dbg) std::cout << "Found a matched jet!" << std::endl;
    }

  } // Trigger bits

  return jetTrigDec;

}

// -------------------------------------------------------------------------- //
const std::map<TString,bool> TriggerTools::matchElectronTriggers(const xAOD::Electron* offlineElectron, ST::SUSYObjDef_xAOD& susyObj)
{

  if( !offlineElectron ){
    std::cout << "TriggerTools::matchElectronTriggers FATAL ERROR Could not cast particle to electron" << std::endl;
    abort();
  }

  std::map<TString,bool> electronTrigDec;
  electronTrigDec.clear();

  bool isDAODPHYS = false;
  TString kernel = "";
  bool passTM=false;
  CentralDB::retrieve(CentralDBFields::DXAODKERNEL, kernel);
  if(kernel.Contains("PHYS"))isDAODPHYS = true;
  if(isDAODPHYS){
    // Loop over all trigger bits
    for( const auto& trigChainName : this->m_HLT_electrons ){
      //      printf("ELEC: Kernel is %s, doing matching of %s<----\n",kernel.Data(),trigChainName.Data());
      passTM=false;
      std::string trigname;
      trigname += trigChainName.Data();
      //if(!susyObj.IsTrigPassed(trigname))continue;
      // Link is broken in mc21, uncomment trigger matching for now
      passTM = (susyObj.IsTrigPassed(trigname) && susyObj.IsTrigMatched(offlineElectron, trigname));
      //printf("ELEC: Trigger %s : %s\n",kernel.Data(),(passTM ? "PASSED" : "FAILED"));
      if(passTM)
        electronTrigDec.insert( std::pair<TString,bool>(trigChainName,passTM) );
    }
    return electronTrigDec;
  }


  for( const auto& trigChainName : this->m_HLT_electrons ){

    //std::cout<<"Looking at trigger "<<trigChainName<<std::endl;

     
    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );

    // Check if it passed: Event level bit
    bool isPassed = cg->isPassed(); 
    //
    if(m_dbg) std::cout << "Electron trigger: " << trigChainName << " : " << isPassed << std::endl;

    // Save nothing if it didn't pass
    if( !isPassed && !m_writeAllDecisions) continue;

    if( m_disableMatching ) continue;

    // Do matching
    bool onlineMatch = m_trigMatchingTool->match(*offlineElectron, (std::string)trigChainName, m_eleDeltaR);
    /*
    bool onlineMatch=false;
    auto fc = cg->features();
    auto eleFeatureContainers = fc.containerFeature<TrigElectronContainer>();
    for(auto econt : eleFeatureContainers) {
      for (auto onlineElectron : *econt.cptr()) {

	//
	TLorentzVector online;
	TLorentzVector offline;
	
	if(m_dbg){
	  printf("Trigger object offline pt,eta,phi (%f,%f,%f) \n",offlineElectron->pt(),offlineElectron->eta(),offlineElectron->phi() );
	  printf("Trigger object online pt,eta,phi (%f,%f,%f)  \n",onlineElectron->pt(),onlineElectron->eta(),onlineElectron->phi() );
	}
	
	// Set TLVs
	online.SetPtEtaPhiM( onlineElectron->pt(),onlineElectron->eta(),onlineElectron->phi(),onlineElectron->m() );
	offline.SetPtEtaPhiM( offlineElectron->pt(),offlineElectron->eta(),offlineElectron->phi(),offlineElectron->m() );
	
	// Find DeltaR between them
	float deltaR = online.DeltaR(offline);
	
	// Check if satisfies our criteria
	if(deltaR<m_eleDeltaR){
	  if(m_dbg) printf("Found a online to offline electron match, deltaR == %f, and required to be deltaR<%f \n",deltaR,m_muDeltaR); 
	  onlineMatch=true;
	  break;
	}
      }
    }
    */
    if(onlineMatch){
      // Save trigger matching result		      
      electronTrigDec.insert( std::pair<TString,bool>(trigChainName,onlineMatch) );
      //
      if(m_dbg) std::cout << "Found a matched electron!" << std::endl;
    }
  }


  return electronTrigDec;

}

// -------------------------------------------------------------------------- //
const std::map<TString,bool> TriggerTools::matchPhotonTriggers(const xAOD::Photon* offlinePhoton)
{

  if( !offlinePhoton ){
    std::cout << "TriggerTools::matchPhotonTriggers FATAL ERROR Could not cast particle to photon" << std::endl;
    abort();
  }

  std::map<TString,bool> photonTrigDec;
  photonTrigDec.clear();

  for( const auto& trigChainName : this->m_HLT_photons ){
     
    auto cg = this->m_trigDecTool->getChainGroup( trigChainName.Data() );

    // Check if it passed: Event level bit
    bool isPassed = cg->isPassed(); 
    //
    if(m_dbg) std::cout << "Photon trigger: " << trigChainName << " : " << isPassed << std::endl;

    // Save nothing if it didn't pass
    if( !isPassed && !m_writeAllDecisions) continue;

    if( m_disableMatching ) continue;

    // Do matching
    bool onlineMatch = false;
    if (!m_upstreamTriggerMatching || m_event->contains<xAOD::TrigCompositeContainer>( (TString("TrigMatch_") + trigChainName).Data() )){
       onlineMatch = m_trigMatchingTool->match(*offlinePhoton, (std::string)trigChainName, m_phoDeltaR);
    }    

    if(onlineMatch){
      // Save trigger matching result		      
      photonTrigDec.insert( std::pair<TString,bool>(trigChainName,onlineMatch) );
      //
      if(m_dbg) std::cout << "Found a matched photon!" << std::endl;
    }
  }


  return photonTrigDec;

}
// ------------------------------------------------------------------------------- //
bool TriggerTools::requireYear(TString chain,int year)
{

  // TODO: This should eventually be controlled by the trigger text files themselves
  // Implemented below as follows: if year is 2015, and any 2016 triggers are found, return false. 
  // Same for 2016 (reverse logic)

  if( year==2015 ){
    if( chain=="HLT_mu26_ivarmedium_OR_HLT_mu50" ) return false;
    else{
      return true;
    }
  }
  //
  else if( year==2016 || year==2017 || year==2018 ){
    if( chain=="HLT_mu20_iloose_L1MU15_OR_HLT_mu40" ) return false;
    else{
      return true;
    }

  }
  else{
    std::cout << "TriggerTools::requireYear Unknown year : " << year << std::endl;
    return false;
  }

}
// ------------------------------------------------------------------------------- //
bool TriggerTools::hasEvtHLTBit()
{

  //std::ofstream myfile;
  //myfile.open("./example.txt",std::ios_base::app);

  // Checks
  if( m_HLT_evtDecision.size()==0 ){
    Warning("TriggerTools::hasEvtHLTBit","No trigger bits loaded, make sure you call this function AFTER loadEvtTriggers!!!");
    return false;
  }
  
  // Any trigger bit found, return true;
  for( auto& HLT : m_HLT_evtDecision ){
    //myfile << HLT.first<<" - "<<HLT.second<<"\n";
    //std::cout<<"here"<<std::endl;
    if( HLT.second ) return true;
  }

  //myfile.close();
  
  // Nothing fired
  return false;

}
// ------------------------------------------------------------------------------- //
bool TriggerTools::getHLTDecFromMap(const std::string trigChainName)
{

  auto it = m_HLT_evtDecision.find(trigChainName);

  if( it != this->m_HLT_evtDecision.end() ){
    if( it->second ) return true;
    else             return false;
  }
  else{
    return false;
  }

}
// ------------------------------------------------------------------------------- //
std::vector<std::string> TriggerTools::GetTriggerList(std::string& chain){
  return m_trigDecTool->getListOfTriggers(chain.c_str());
}
// ------------------------------------------------------------------------------- //
void TriggerTools::Reset_perEvt()
{
  m_L1_evtDecision.clear();
  m_HLT_evtDecision.clear(); 
  m_prescale.clear();
}
// ------------------------------------------------------------------------------- //
void TriggerTools::Reset_perSys()
{
  m_HLT_dec_electrons.clear();
  m_HLT_dec_muons.clear();
  m_HLT_dec_met.clear();
  m_L1_dec.clear();
}
// ------------------------------------------------------------------------------- //
TriggerTools::~TriggerTools()
{

  // TODO: If we clean this up, SUSYTools seg faults in its destructor
  //DeleteTool(m_trigDecTool);
  /*
  if(m_configTool)   delete m_configTool;
  if(m_trigDecTool)  delete m_trigDecTool;
  */
  // Clear the dic of all trigger bits we are considering
  this->Reset_perEvt();
  this->Reset_perSys();

}
