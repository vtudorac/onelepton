#include "SusySkim1LInclusive/ttbarModelling.h"


// ------------------------------------------------------------------------------------------ //
ttbarModelling::ttbarModelling() : BaseUser("SusySkim1LInclusive","ttbarModelling")
{

}
// ------------------------------------------------------------------------------------------ //
void ttbarModelling::setup(ConfigMgr*& configMgr)
{

  // Use object def from SUSYTools
  configMgr->obj->useSUSYToolsSignalDef(true);

  // Muon cuts
  configMgr->obj->setBaselineMuonPt(6.0);
  configMgr->obj->setBaselineMuonEta(2.50);
  configMgr->obj->setSignalMuonPt(6.0);
  configMgr->obj->setSignalMuonEta(2.50);
 
  // Electrons
  configMgr->obj->setBaselineElectronEt(7.0);
  configMgr->obj->setBaselineElectronEta(2.47);
  configMgr->obj->setSignalElectronEt(7.0);
  configMgr->obj->setSignalElectronEta(2.47);
  
  // Jets
  configMgr->obj->setCJetPt(25.0);
  configMgr->obj->setCJetEta(2.80);
  configMgr->obj->setFJetPt(30.0);
  configMgr->obj->setFJetEtaMin(2.80);
  configMgr->obj->setFJetEtaMax(4.50);

  // MET Triggers
  // Defined by run number <start,end>; -1 means ignore that bound (e.g. 2016 runs are open ended)
  configMgr->addTriggerAna("HLT_xe70_mht",276262,284484,"metTrig");
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",296939,302872,"metTrig");
  configMgr->addTriggerAna("HLT_xe100_mht_L1XE50",302919,303892,"metTrig");
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",303943,-1,"metTrig");

  // Single lepton triggers
  // Stores the trigger decision and corresponding SFs ( and their systematics )
  // Defined by run number <start,end>; -1 means ignore that bound (e.g. 2016 runs are open ended)
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu40",276262,284484,"singleLepTrig");
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",297730,-1,"singleLepTrig");
  configMgr->addTriggerAna("SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",276262,-1, "singleLepTrig");

  //
  // ---- Output trees ---- //
  //

  // Truth 
  configMgr->treeMaker->addFloatVariable("truthTopMass", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthAntiTopMass", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthTopPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthAntiTopPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthTopEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthAntiTopEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthTopPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthAntiTopPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthWlepPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthWlepEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthWlepPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthWlepMass", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthlep1Pt", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthlep1Eta", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthlep1Phi", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthNuPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthNuPz", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthNuEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("truthNuPhi", 0.0 );

  // Reco
  configMgr->treeMaker->addFloatVariable("topMass",0.0);
  configMgr->treeMaker->addFloatVariable("topPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("topEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("topPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("WlepPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("WlepEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("WlepPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("WlepMass", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadMass", 0.0 );
  configMgr->treeMaker->addFloatVariable("tophadPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("tophadEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("tophadPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("tophadMass", 0.0 );
  configMgr->treeMaker->addFloatVariable("BhadPt",  0.0 );
  configMgr->treeMaker->addFloatVariable("BhadEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("BhadPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("blepPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("blepEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("blepPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("NuPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("NuPz", 0.0 );
  configMgr->treeMaker->addFloatVariable("NuEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("NuPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("ttbarPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("ttbarEta", 0.0 );
  configMgr->treeMaker->addFloatVariable("ttbarPhi", 0.0 );
  configMgr->treeMaker->addFloatVariable("ttbarMass", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadJet1Pt", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadJet1Eta", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadJet1Phi", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadJet2Pt", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadJet2Eta", 0.0 );
  configMgr->treeMaker->addFloatVariable("WhadJet2Phi", 0.0 );

  configMgr->treeMaker->addFloatVariable("ISRJet1Pt", 0.0 );
  configMgr->treeMaker->addFloatVariable("ISRJet1Eta", 0.0 );
  configMgr->treeMaker->addFloatVariable("ISRJet1Phi", 0.0 );
  configMgr->treeMaker->addFloatVariable("ISRJet2Pt", 0.0 );
  configMgr->treeMaker->addFloatVariable("ISRJet2Eta", 0.0 );
  configMgr->treeMaker->addFloatVariable("ISRJet2Phi", 0.0 );

  // Angular information
  configMgr->treeMaker->addFloatVariable("truthWBDeltaR", 0.0 );
  configMgr->treeMaker->addFloatVariable("deltaR_truthReco_lepbjet", -999.0 );
  configMgr->treeMaker->addFloatVariable("deltaR_truthReco_nu", 0.0);
  configMgr->treeMaker->addFloatVariable("deltaR_truthReco_lep", 0.0 );
  configMgr->treeMaker->addFloatVariable("deltaR_truthReco_WjetLeading", 0.0 );
  configMgr->treeMaker->addFloatVariable("deltaR_truthReco_WjetSubLeading", 0.0 );
  configMgr->treeMaker->addFloatVariable("deltaR_truthReco_BHad", 0.0 );

  // Generator filtering
  configMgr->treeMaker->addFloatVariable("GenHt", -1.0);
  configMgr->treeMaker->addFloatVariable("GenMET", -1.0);

}
// ------------------------------------------------------------------------------------------ //
bool ttbarModelling::doAnalysis(ConfigMgr*& configMgr)
{

  // Skims events by imposing any cuts you define in this method below
  if( !passCuts(configMgr) ) return false;


  // ---------------------------------- //
  //     Reconstruction information     //
  // ---------------------------------- //

  TLorentzVector lepTop;
  TLorentzVector lepW;
  TLorentzVector lepNu;
 
  TLorentzVector lepB;
  TLorentzVector hadTop;
  TLorentzVector hadW;
  TLorentzVector hadB;

  int wJet_idx1 = -1;
  int wJet_idx2 = -1;
  
  // Do it !
  if( !reconstructTop(configMgr,lepTop,lepNu,lepW,lepB,hadTop,hadW,hadB,wJet_idx1,wJet_idx2) ) return false;

  // 
  configMgr->treeMaker->setFloatVariable("topPt",lepTop.Pt() );
  configMgr->treeMaker->setFloatVariable("topEta",lepTop.Eta() );
  configMgr->treeMaker->setFloatVariable("topPhi",lepTop.Phi() );
  configMgr->treeMaker->setFloatVariable("topMass", lepTop.M() );
  configMgr->treeMaker->setFloatVariable("WlepPt", lepW.Pt() );
  configMgr->treeMaker->setFloatVariable("WlepEta", lepW.Eta() );
  configMgr->treeMaker->setFloatVariable("WlepPhi", lepW.Phi() );
  configMgr->treeMaker->setFloatVariable("WlepMass", lepW.M() );
  configMgr->treeMaker->setFloatVariable("blepPt", lepB.Pt() );
  configMgr->treeMaker->setFloatVariable("blepEta", lepB.Eta() );
  configMgr->treeMaker->setFloatVariable("blepPhi", lepB.Phi() );
  configMgr->treeMaker->setFloatVariable("NuPt", lepNu.Pt() );
  configMgr->treeMaker->setFloatVariable("NuPz", lepNu.Pz() );
  configMgr->treeMaker->setFloatVariable("NuEta", lepNu.Eta() );
  configMgr->treeMaker->setFloatVariable("NuPhi", lepNu.Phi() );
  configMgr->treeMaker->setFloatVariable("WhadPt",  hadW.Pt() );
  configMgr->treeMaker->setFloatVariable("WhadEta", hadW.Eta() );
  configMgr->treeMaker->setFloatVariable("WhadPhi", hadW.Phi() );
  configMgr->treeMaker->setFloatVariable("WhadMass", hadW.M() );
  configMgr->treeMaker->setFloatVariable("tophadPt", hadTop.Pt() );
  configMgr->treeMaker->setFloatVariable("tophadEta", hadTop.Eta() );
  configMgr->treeMaker->setFloatVariable("tophadPhi", hadTop.Phi() );
  configMgr->treeMaker->setFloatVariable("tophadMass", hadTop.M() );
  configMgr->treeMaker->setFloatVariable("BhadPt",  hadB.Pt() );
  configMgr->treeMaker->setFloatVariable("BhadEta", hadB.Eta() );
  configMgr->treeMaker->setFloatVariable("BhadPhi", hadB.Phi() );
  configMgr->treeMaker->setFloatVariable("ttbarPt", (hadTop+lepTop).Pt() );
  configMgr->treeMaker->setFloatVariable("ttbarEta", (hadTop+lepTop).Eta() );
  configMgr->treeMaker->setFloatVariable("ttbarPhi", (hadTop+lepTop).Phi() );
  configMgr->treeMaker->setFloatVariable("ttbarMass", (hadTop+lepTop).M() );
  configMgr->treeMaker->setFloatVariable("WhadJet1Pt", configMgr->obj->cJets[wJet_idx1]->Pt() );
  configMgr->treeMaker->setFloatVariable("WhadJet1Eta", configMgr->obj->cJets[wJet_idx1]->Eta() );
  configMgr->treeMaker->setFloatVariable("WhadJet1Phi", configMgr->obj->cJets[wJet_idx1]->Phi() );
  configMgr->treeMaker->setFloatVariable("WhadJet2Pt", configMgr->obj->cJets[wJet_idx2]->Pt() );
  configMgr->treeMaker->setFloatVariable("WhadJet2Eta", configMgr->obj->cJets[wJet_idx2]->Eta() );
  configMgr->treeMaker->setFloatVariable("WhadJet2Phi", configMgr->obj->cJets[wJet_idx2]->Phi() );
  
  JetVector ISR_jets;
  for( unsigned int i=0; i<configMgr->obj->cJets.size(); i++ ){
    JetVariable* jet = configMgr->obj->cJets[i];
    if( jet->bjet || i==unsigned(wJet_idx1) || i==unsigned(wJet_idx2) ) continue;
    ISR_jets.push_back(jet);
  }


  float ISRJet1_pt  = ISR_jets.size()>0 ? ISR_jets[0]->Pt() : 0.0;
  float ISRJet1_eta = ISR_jets.size()>0 ? ISR_jets[0]->Eta() : 0.0;
  float ISRJet1_phi = ISR_jets.size()>0 ? ISR_jets[0]->Phi() : 0.0;

  float ISRJet2_pt  = ISR_jets.size()>1 ? ISR_jets[1]->Pt() : 0.0;
  float ISRJet2_eta = ISR_jets.size()>1 ? ISR_jets[1]->Eta() : 0.0;
  float ISRJet2_phi = ISR_jets.size()>1 ? ISR_jets[1]->Phi() : 0.0;


  configMgr->treeMaker->setFloatVariable("ISRJet1Pt", ISRJet1_pt );
  configMgr->treeMaker->setFloatVariable("ISRJet1Eta", ISRJet1_eta );
  configMgr->treeMaker->setFloatVariable("ISRJet1Phi", ISRJet1_phi );
 
  configMgr->treeMaker->setFloatVariable("ISRJet2Pt", ISRJet2_pt );
  configMgr->treeMaker->setFloatVariable("ISRJet2Eta", ISRJet2_eta );
  configMgr->treeMaker->setFloatVariable("ISRJet2Phi", ISRJet2_phi );
 


  // ------------------------- //
  //     Truth information     //
  // ------------------------- //

  if( configMgr->treeMaker->getTreeState()=="" && configMgr->obj->truthEvent.TTbarTLVs.size()>0 ){

    const TLorentzVector* top        = configMgr->obj->truthEvent.getTTbarTLV("top");
    const TLorentzVector* antitop    = configMgr->obj->truthEvent.getTTbarTLV("antitop");
    const TLorentzVector* WbosonLep  = configMgr->obj->truthEvent.getTTbarTLV("w1");
    const TLorentzVector* WbosonHad  = configMgr->obj->truthEvent.getTTbarTLV("w2");
    const TLorentzVector* Blep       = configMgr->obj->truthEvent.getTTbarTLV("b1");
    const TLorentzVector* BHad       = configMgr->obj->truthEvent.getTTbarTLV("b2");
    const TLorentzVector* lep1       = configMgr->obj->truthEvent.getTTbarTLV("l11");
    const TLorentzVector* nu1        = configMgr->obj->truthEvent.getTTbarTLV("l12");
    const TLorentzVector* q1         = configMgr->obj->truthEvent.getTTbarTLV("l21");
    const TLorentzVector* q2         = configMgr->obj->truthEvent.getTTbarTLV("l22");

    configMgr->treeMaker->setFloatVariable("truthTopPt", top->Pt() );
    configMgr->treeMaker->setFloatVariable("truthTopEta", top->Eta() );
    configMgr->treeMaker->setFloatVariable("truthTopPhi", top->Phi()  );
    configMgr->treeMaker->setFloatVariable("truthTopMass", top->M()  );
    configMgr->treeMaker->setFloatVariable("truthAntiTopPt",antitop->Pt() );
    configMgr->treeMaker->setFloatVariable("truthAntiTopEta", antitop->Eta() );
    configMgr->treeMaker->setFloatVariable("truthAntiTopPhi", antitop->Phi() );
    configMgr->treeMaker->setFloatVariable("truthAntiTopMass", antitop->M() );
    configMgr->treeMaker->setFloatVariable("truthWlepPt", WbosonLep->Pt());
    configMgr->treeMaker->setFloatVariable("truthWlepEta", WbosonLep->Eta() );
    configMgr->treeMaker->setFloatVariable("truthWlepPhi", WbosonLep->Phi() );
    configMgr->treeMaker->setFloatVariable("truthWlepMass", WbosonLep->M() );
    configMgr->treeMaker->setFloatVariable("truthlep1Pt", lep1->Pt() );
    configMgr->treeMaker->setFloatVariable("truthlep1Eta", lep1->Eta() );
    configMgr->treeMaker->setFloatVariable("truthlep1Phi", lep1->Phi() );
    configMgr->treeMaker->setFloatVariable("truthNuPt", nu1->Pt() );
    configMgr->treeMaker->setFloatVariable("truthNuPz", nu1->Pz());
    configMgr->treeMaker->setFloatVariable("truthNuEta", nu1->Eta() );
    configMgr->treeMaker->setFloatVariable("truthNuPhi", nu1->Phi() );

    // Angular variables -- to assess quality of the reconstruction
    configMgr->treeMaker->setFloatVariable("deltaR_truthReco_lepbjet", Blep->DeltaR( lepB ) );
    configMgr->treeMaker->setFloatVariable("deltaR_truthReco_nu", nu1->DeltaR( lepNu ) );
    configMgr->treeMaker->setFloatVariable("deltaR_truthReco_lep", configMgr->obj->baseLeptons[0]->DeltaR( *lep1 ) );
    configMgr->treeMaker->setFloatVariable("deltaR_truthReco_WjetLeading", getMinDR(configMgr->obj->cJets[wJet_idx1],q1,q2) );
    configMgr->treeMaker->setFloatVariable("deltaR_truthReco_WjetSubLeading", getMinDR(configMgr->obj->cJets[wJet_idx2],q1,q2) );
    configMgr->treeMaker->setFloatVariable("deltaR_truthReco_BHad", BHad->DeltaR( hadB ) );

    // Truth vs truth
    configMgr->treeMaker->setFloatVariable("truthWBDeltaR", Blep->DeltaR( *WbosonLep)  );

  }


  // Generator filtering
  configMgr->treeMaker->setFloatVariable("GenHt", configMgr->obj->evt.GenHt);
  configMgr->treeMaker->setFloatVariable("GenMET", configMgr->obj->evt.GenMET);

  //
  configMgr->doWeightSystematics();

  // Fill the output tree
  configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

  return true;


}
// ------------------------------------------------------------------------------------------ //
float ttbarModelling::getMinDR(JetVariable* jet,const TLorentzVector* cand1,const TLorentzVector* cand2)
{

  float minDr = -1.0;

  float minDr_cand1 = cand1->DeltaR( *jet );
  float minDr_cand2 = cand2->DeltaR( *jet );

  minDr = minDr_cand1 < minDr_cand2 ? minDr_cand1  : minDr_cand2;

  return minDr;

}
// ------------------------------------------------------------------------------------------ //
bool ttbarModelling::reconstructTop(ConfigMgr*& configMgr,
				   TLorentzVector& lepTop,TLorentzVector& lepNu,TLorentzVector& lepW,TLorentzVector& lepB,
				   TLorentzVector& hadTop,TLorentzVector& hadW,TLorentzVector& hadB,
				   int& wJet_idx1, int& wJet_idx2)
{

  //
  LeptonVariable* slep = configMgr->obj->baseLeptons[0];

  //
  const TLorentzVector* met = dynamic_cast<const TLorentzVector*>(&(configMgr->obj->met));

  // Solve for the z-component of the neutrinos momentum!
  double A = 1.0 / ( 2.0 * ( pow(slep->E(),2) - pow(slep->Pz(),2) ) );
  double p_W_xy = ( slep->Px() * met->Px() ) + ( slep->Py() * met->Py() );
  double B = slep->Pz() * ( 2.0*p_W_xy + 80.1*80.1 );
  double C = TMath::Sqrt( pow(slep->E(),2) * pow( ( 2.0*p_W_xy + 80.1*80.1 ),2 ) - 4.0 * pow(met->E(),2) * ( pow(slep->E(),2) - pow(slep->Pz(),2) ) ) ;
  double sol_up = A*(B + C);
  double sol_dn = A*(B - C);

  
  /*
  std::cout << "Neutrino information..." << std::endl;
  std::cout << "  >> Px :     " << met->Px() << std::endl;
  std::cout << "  >> Py :     " << met->Py() << std::endl;
  std::cout << "  >> Lep Px : " << slep->Px() << std::endl;
  std::cout << "  >> Lep Py : " << slep->Py() << std::endl;
  std::cout << "  >> Lep Pz : " << slep->Pz() << std::endl;
  std::cout << "  >> Lep E :  " << slep->E() << std::endl;
  std::cout << "  >> A :      " << A << std::endl;
  std::cout << "  >> B :      " << B << std::endl;
  std::cout << "  >> C :      " << C << std::endl;
  std::cout << "  >> Pz (+) : " << A*(B + C) << std::endl;
  std::cout << "  >> Pz (-) : " << A*(B - C) << std::endl;
 */

  TLorentzVector nu_TLV_up;
  nu_TLV_up.SetPxPyPzE( met->Px(),  met->Py(), sol_up, TMath::Sqrt( pow(met->Px(),2) + pow(met->Py(),2) + pow(sol_up,2) ) );
 
  TLorentzVector nu_TLV_dn;
  nu_TLV_dn.SetPxPyPzE( met->Px(),  met->Py(), sol_dn, TMath::Sqrt( pow(met->Px(),2) + pow(met->Py(),2) + pow(sol_dn,2) ) );

  
  //std::cout << "nu-mass up : " << nu_TLV_up.M() << std::endl;
  //std::cout << "nu-mass dn : " << nu_TLV_dn.M() << std::endl;

  //std::cout << "W-mass up : " << (*slep+nu_TLV_up).M() << std::endl;
  //std::cout << "W-mass dn : " << (*slep+nu_TLV_dn).M() << std::endl;
  

  double diff_mass = 9999999.0;

  int idx = -1;
  int counter = 0;
  for( auto& bjet : configMgr->obj->bJets ){

    double mass_up = (*bjet+*slep+nu_TLV_up).M();
    double mass_dn = (*bjet+*slep+nu_TLV_dn).M();

    // ?? Angular constraint ??
    if( fabs(mass_up-172.1) < diff_mass ){
      lepTop = (*bjet+*slep+nu_TLV_up);
      lepB   = (*bjet);
      lepW   = (*slep+nu_TLV_up);
      lepNu  = nu_TLV_up;
      diff_mass = fabs(mass_up-172.1);
      idx = counter;
    }
    if( fabs(mass_dn-172.1) < diff_mass ){
      lepTop = (*bjet+*slep+nu_TLV_dn);
      lepB   = (*bjet);
      lepW   = (*slep+nu_TLV_dn);
      lepNu  = nu_TLV_dn; 
      diff_mass = fabs(mass_dn-172.1);
      idx = counter;
    }

    counter++;
  }

  if( idx<0 ) return false;
  

  //std::cout << "For leptonically decaying top, we chose idx : " << idx << std::endl;
  //std::cout << "Momentum of the lep b-jet: " << configMgr->obj->bJets[idx]->Pt() << std::endl;

  int had_bjet_idx = idx==0 ? 1 : 0;

  //std::cout << "Momentum of the had b-jet: " << configMgr->obj->bJets[had_bjet_idx]->Pt() << std::endl;

  hadB = ( *configMgr->obj->bJets[had_bjet_idx] );

  //std::cout << "Njets : " << configMgr->obj->cJets.size() << std::endl;

  float WmassDiff = 999999999;
  float best_Wmass = 0.0;

  wJet_idx1 = -1;
  wJet_idx2 = -1;

  for( unsigned int i1=0; i1<configMgr->obj->cJets.size(); i1++ ){
    for( unsigned int i2=i1+1; i2<configMgr->obj->cJets.size(); i2++ ){

      // Only consider light jets for W
      if( i1==i2 ) continue;
      const JetVariable* jet1 = configMgr->obj->cJets[i1];
      const JetVariable* jet2 = configMgr->obj->cJets[i2];

      if( jet1->bjet || jet2->bjet ) continue;
      
      float Wmass = (*jet1+*jet2).M();

      if( fabs(Wmass-80.1) < WmassDiff ){
	hadW = (*jet1+*jet2);
	WmassDiff = fabs(Wmass-80.1);
	wJet_idx1 = i1;
	wJet_idx2 = i2;
      }
    }
  }

  if( wJet_idx1<0 || wJet_idx2<0 ) return false;

  //std::cout << "Best W-mass candidate : " << hadW.M() << std::endl;

  hadTop = ( hadB + hadW );

  
  return true;

}
// ------------------------------------------------------------------------------------------ //
bool ttbarModelling::passCuts(ConfigMgr*& configMgr)
{

  /*
   This method is used to apply any cuts you wish before writing
   the output trees
  */

  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  // Apply all recommended event cleaning cuts
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  if( getNBJets( configMgr->obj, 25.0 ) != 2 ) return false;
  configMgr->cutflow->bookCut("cutFlow","At least one b-tagged jet",weight );

  if( getNJets(configMgr->obj, 30.0) < 4 ) return false;
  configMgr->cutflow->bookCut("cutFlow","Four or more jets",weight );

  if( configMgr->obj->baseLeptons.size() < 1 ) return false;
  configMgr->cutflow->bookCut("cutFlow","More than one lepton",weight );

  return true;

}
// ------------------------------------------------------------------------------------------ //
void ttbarModelling::finalize(ConfigMgr*& configMgr)
{

  (void)configMgr;

}
// ------------------------------------------------------------------------------------------ //
