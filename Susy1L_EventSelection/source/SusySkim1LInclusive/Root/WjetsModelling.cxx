#include "SusySkim1LInclusive/WjetsModelling.h"


// ------------------------------------------------------------------------------------------ //
WjetsModelling::WjetsModelling() : BaseUser("SusySkim1LInclusive","WjetsModelling")
{

}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::setup(ConfigMgr*& configMgr)
{

  // Make a cutflow stream
  configMgr->cutflow->defineCutFlow("cutFlow",configMgr->treeMaker->getFile("tree"));
  configMgr->cutflow->defineCutFlow("TruthSelection",configMgr->treeMaker->getFile("tree"));

  // Use object def from SUSYTools
  configMgr->obj->useSUSYToolsSignalDef(true);
  //configMgr->obj->doUserJetTauOR(true);

  // Muon cuts
  configMgr->obj->setBaselineMuonPt(6.0);
  configMgr->obj->setBaselineMuonEta(2.50);
  configMgr->obj->setSignalMuonPt(6.0);
  configMgr->obj->setSignalMuonEta(2.50);

  // Electrons
  configMgr->obj->setBaselineElectronEt(7.0);
  configMgr->obj->setBaselineElectronEta(2.47);
  configMgr->obj->setSignalElectronEt(7.0);
  configMgr->obj->setSignalElectronEta(2.47);

  // Jets
  configMgr->obj->setCJetPt(25.0);
  configMgr->obj->setCJetEta(2.80);
  configMgr->obj->setFJetPt(30.0);
  configMgr->obj->setFJetEtaMin(2.80);
  configMgr->obj->setFJetEtaMax(4.50);

  // This toggles how many jets to save
  // Saves reconstructed jet pT,eta,phi,
  // and if a truth link exists the 
  // same truth quantities
  m_saveNJets = 8;

  // MET Triggers
  // Defined by run number <start,end>; -1 means ignore that bound (e.g. 2016 runs are open ended)
  configMgr->addTriggerAna("HLT_xe70_mht",276262,284484,"metTrig");
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",296939,302872,"metTrig");
  configMgr->addTriggerAna("HLT_xe100_mht_L1XE50",302919,303892,"metTrig");
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",303943,-1,"metTrig");

  // Single lepton triggers
  // Stores the trigger decision and corresponding SFs ( and their systematics )
  // Defined by run number <start,end>; -1 means ignore that bound (e.g. 2016 runs are open ended)
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu40",276262,284484,"singleLepTrig");
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",297730,-1,"singleLepTrig");
  configMgr->addTriggerAna("SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",276262,-1, "singleLepTrig");

  // Event selectors
  configMgr->treeMaker->addIntVariable("WAnalysis",false);
  configMgr->treeMaker->addIntVariable("ZAnalysis",false);

  // Truth variables
  configMgr->treeMaker->addFloatVariable("DeltaPhiTruthWJetClosest",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRTruthLepJetClosest",0.0);

  // Bool variables
  configMgr->treeMaker->addBoolVariable("isReco",false);
  configMgr->treeMaker->addBoolVariable("isTruth",false);

  // ttbar Truth variables
  configMgr->treeMaker->addFloatVariable("truthTopPt",0.0);
  configMgr->treeMaker->addFloatVariable("truthAntiTopPt",0.0);
  configMgr->treeMaker->addFloatVariable("truthTtbarPt",0.0);
  configMgr->treeMaker->addFloatVariable("truthTtbarM",0.0);
  configMgr->treeMaker->addDoubleVariable("ttbarNNLOWeight",1.0);
  configMgr->treeMaker->addIntVariable("DecayModeTTbar",-1);

  // Ouptut trees
  configMgr->treeMaker->addFloatVariable("met",0.0);
  configMgr->treeMaker->addFloatVariable("metPhi",0.0);
  configMgr->treeMaker->addFloatVariable("mt",0.0);
  configMgr->treeMaker->addFloatVariable("meffInc30",0.0);
  configMgr->treeMaker->addFloatVariable("Ht30",0.0);
  configMgr->treeMaker->addFloatVariable("zPt",0.0);
  configMgr->treeMaker->addDoubleVariable("mll", 0.0 );

  configMgr->treeMaker->addFloatVariable("DeltaRZJetClosest",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRZJet1",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRZJet2",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRZJet3",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiZJetClosest",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiZJet1",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiZJet2",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiZJet3",0.0);

  configMgr->treeMaker->addIntVariable("n_DeltaPhiWJetClosest",0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiWJetClosest",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiWJet1",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiWJet2",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiWJet3",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRLepJetClosest",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRLepJet1",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRLepJet2",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRLepJet3",0.0);

  configMgr->treeMaker->addFloatVariable("DeltaPhiTruthWJet1",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaPhiTruthWJet2",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRTruthLepJet1",0.0);
  configMgr->treeMaker->addFloatVariable("DeltaRTruthLepJet2",0.0);

  // W-system
  configMgr->treeMaker->addFloatVariable("wPt",0.0);
  configMgr->treeMaker->addFloatVariable("wEta",0.0);
  configMgr->treeMaker->addFloatVariable("wPhi",0.0);
  configMgr->treeMaker->addFloatVariable("wTruthPt",0.0);
  configMgr->treeMaker->addFloatVariable("wTruthEta",0.0);
  configMgr->treeMaker->addFloatVariable("wTruthPhi",0.0);

  // Lepton information
  configMgr->treeMaker->addIntVariable("AnalysisType",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Pt",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Et",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Eta",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Phi",0.0);
  configMgr->treeMaker->addDoubleVariable("lep2Pt", 0.0);
  configMgr->treeMaker->addDoubleVariable("lep2Eta", 0.0 );
  configMgr->treeMaker->addDoubleVariable("lep2Phi", 0.0 );
  configMgr->treeMaker->addFloatVariable("lep1TruthPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("lep1TruthEta", 0.0 );
  configMgr->treeMaker->addIntVariable("lep1TruthOrigin", 0.0 );
  configMgr->treeMaker->addIntVariable("lep1TruthType", 0.0 );
  configMgr->treeMaker->addFloatVariable("lep2TruthPt", 0.0 );
  configMgr->treeMaker->addFloatVariable("lep2TruthEta", 0.0 );
  configMgr->treeMaker->addIntVariable("lep2TruthOrigin", 0.0 );
  configMgr->treeMaker->addIntVariable("lep2TruthType", 0.0 );

  // Jets
  configMgr->treeMaker->addIntVariable("nJet25",0.0);
  configMgr->treeMaker->addIntVariable("nBJet25",0.0);

  // Jet kinematics
  for( unsigned int j=1; j<=m_saveNJets; j++ ){
    configMgr->treeMaker->addFloatVariable( TString::Format("jet%iPt",j) , 0.0 );
    configMgr->treeMaker->addFloatVariable( TString::Format("jet%iEta",j) , 0.0 );
    configMgr->treeMaker->addFloatVariable( TString::Format("jet%iPhi",j) , 0.0 );
    configMgr->treeMaker->addFloatVariable( TString::Format("jet%iTruthPt",j) , 0.0 );
    configMgr->treeMaker->addFloatVariable( TString::Format("jet%iTruthEta",j) , 0.0 );
    configMgr->treeMaker->addFloatVariable( TString::Format("jet%iTruthPhi",j) , 0.0 );
  }


  // Weights
  configMgr->treeMaker->addDoubleVariable("pileupWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("leptonWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("eventWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("jvtWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("bTagWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightUp",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightDown",1.0);
  configMgr->treeMaker->addDoubleVariable("SherpaVjetsNjetsWeight",1.0);

  // Truth MET
  configMgr->treeMaker->addDoubleVariable("truthMET_Et",0.0);
  configMgr->treeMaker->addDoubleVariable("truthMET_Phi",0.0);
  configMgr->treeMaker->addDoubleVariable("truthRecoMETPull_Et",0.0);
  configMgr->treeMaker->addDoubleVariable("truthRecoMETPull_Phi",0.0);

  // PDF variables
  configMgr->treeMaker->addFloatVariable("x1", -1.0);
  configMgr->treeMaker->addFloatVariable("x2", -1.0);
  configMgr->treeMaker->addFloatVariable("pdf1", -1.0);
  configMgr->treeMaker->addFloatVariable("pdf2", -1.0);
  configMgr->treeMaker->addFloatVariable("scalePDF", -1.0);
  configMgr->treeMaker->addIntVariable("id1", 0);
  configMgr->treeMaker->addIntVariable("id2", 0);

  // Generator filtering
  configMgr->treeMaker->addFloatVariable("GenHt", -1.0);
  configMgr->treeMaker->addFloatVariable("GenMET", -1.0);

  // Set lumi
  configMgr->objectTools->setLumi(1.0);

}
// ------------------------------------------------------------------------------------------ //
bool WjetsModelling::doAnalysis(ConfigMgr*& configMgr)
{

  // We need to flag to save that tells us if the event
  // was reconstruted because of pile-up.
  //
  //
  // The minDPhiJetW should be redefined 
  // into something more concrete for a measurement
  //
  // Perhaps we want to choose the sub-leading jet in n-jet events?
  // Don't think so,...



  // Reconstructed selection and variables
  bool passReco = passRecoCuts(configMgr);


  //if( passReco && !passTruth ){
    //passRecoByPileup = true; // TODO
    // Check inclusiveness of the jets
  //}

  //configMgr->treeMaker->setBoolVariable("isReco",passReco);
//  configMgr->treeMaker->setBoolVariable("isTruth",passTruth);

/*
  if( passReco && !passTruth ){
    std::cout << "Found a reconstructed event, but truth event!!! Listing some information..." << std::endl;

    std::cout << "Truth met is : " << configMgr->obj->met.getTruthTLV().Et() << std::endl;

    std::cout << "Listing lepton information..." << std::endl;
    for( auto& lep : configMgr->obj->truthLeptons ){
      lep->print();
    }

    std::cout << "Listing jet information..." << std::endl;
    for( auto& jet : configMgr->obj->truthJets ){
      jet->print();
    }
  }
*/
  if( passReco ){
    configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");
  }

  return true;

}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::fillTruthBranches(ConfigMgr*& configMgr)
{

  const TruthVariable* truthLep  = configMgr->obj->truthLeptons.size()>=1 ? configMgr->obj->truthLeptons[0] : 0;
  const TruthVariable* truthJet1 = configMgr->obj->truthJets.size()>=1 ? configMgr->obj->truthJets[0] : 0;
  const TruthVariable* truthJet2 = configMgr->obj->truthJets.size()>=2 ? configMgr->obj->truthJets[1] : 0;

  if( truthLep ){

    float minDphi = 999999;
    for( auto& jet : configMgr->obj->truthJets ){
      float dPhi = TMath::Abs( jet->DeltaPhi( (*truthLep+configMgr->obj->met.getTruthTLV()) ) );
      if( dPhi<minDphi ) minDphi = dPhi;
    }

    configMgr->treeMaker->setFloatVariable("lep1TruthPt", truthLep->Pt() );
    configMgr->treeMaker->setFloatVariable("wTruthPt", (*truthLep+configMgr->obj->met.getTruthTLV()).Pt() );
    //configMgr->treeMaker->setFloatVariable("DeltaPhiTruthWJetClosest",minDphi);
  }

  if( truthJet1 ){
    configMgr->treeMaker->setFloatVariable("jet1TruthPt", truthJet1->Pt() );
  }

  if( truthJet2 ){
    configMgr->treeMaker->setFloatVariable("jet2TruthPt", truthJet2 ? truthJet2->Pt() : 0 );
  }

  // Variables that do not depend on selections
  configMgr->treeMaker->setIntVariable("nTruthJets25", configMgr->obj->truthJets.size() );


}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::fillRecoBranches(ConfigMgr*& configMgr)
{

  /*
      Software truth ideas
      Observables getTruthLink(JetVariable); -> Search
 
 */

  // Split into W and Z selections
  int nLep = configMgr->obj->signalLeptons.size();

  if( nLep==1 ){
    configMgr->treeMaker->setIntVariable("WAnalysis",true);
    do_WAnalysis(configMgr);
  }
  // TODO
  else if( nLep==2 ){
    configMgr->treeMaker->setIntVariable("ZAnalysis",true);
  //  do_ZAnalysis(configMgr);
  }

  // Finally
  fillCommonBranches(configMgr);

}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::do_ZAnalysis(ConfigMgr*& configMgr)
{

  const LeptonVariable* leading_lep    = configMgr->obj->baseLeptons[0];
  const LeptonVariable* subleading_lep = configMgr->obj->baseLeptons[1];

  if( configMgr->obj->signalLeptons.size()==2 ){
    configMgr->treeMaker->setDoubleVariable("mll", getMll(configMgr->obj) );
    configMgr->treeMaker->setDoubleVariable("lep2Pt", configMgr->obj->signalLeptons[1]->Pt() );
    configMgr->treeMaker->setDoubleVariable("lep2Eta", configMgr->obj->signalLeptons[1]->Eta() );
    configMgr->treeMaker->setDoubleVariable("lep2Phi", configMgr->obj->signalLeptons[1]->Phi() );

    configMgr->treeMaker->setFloatVariable("zPt",(*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]).Pt() );
    configMgr->treeMaker->setFloatVariable("DeltaRZJet1",configMgr->obj->cJets[0]->DeltaR( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) ) );
    configMgr->treeMaker->setFloatVariable("DeltaRZJet2",configMgr->obj->cJets[1]->DeltaR( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) ) );
    configMgr->treeMaker->setFloatVariable("DeltaPhiZJet1",configMgr->obj->cJets[0]->DeltaPhi( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) ) );
    configMgr->treeMaker->setFloatVariable("DeltaPhiZJet2",configMgr->obj->cJets[1]->DeltaPhi( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) ) );


    if( configMgr->obj->cJets.size()>=3 ){
      configMgr->treeMaker->setFloatVariable("DeltaRZJet3",configMgr->obj->cJets[2]->DeltaR( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) ) );
      configMgr->treeMaker->setFloatVariable("DeltaPhiZJet3",configMgr->obj->cJets[2]->DeltaPhi( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) ) );
    }


    float minDeltaR = 99999;
    float minDeltaPhi = 99999;
    for( auto& jet : configMgr->obj->cJets ){
      float _minDeltaR = jet->DeltaR( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) );
      float _minDeltaPhi = fabs(jet->DeltaPhi( (*configMgr->obj->signalLeptons[0]+*configMgr->obj->signalLeptons[1]) ));
      if( _minDeltaR < minDeltaR ) { minDeltaR = _minDeltaR; }
      if( _minDeltaPhi < minDeltaPhi ) { minDeltaPhi = _minDeltaPhi; }
    }
    configMgr->treeMaker->setFloatVariable("DeltaRZJetClosest",minDeltaR);
    configMgr->treeMaker->setFloatVariable("DeltaPhiZJetClosest",minDeltaPhi);

  }


}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::do_WAnalysis(ConfigMgr*& configMgr)
{

  // We requested exactly one lepton
  // and two jets
  LeptonVariable* lep  = configMgr->obj->baseLeptons[0];
  JetVariable* leadjet = configMgr->obj->cJets[0];
  JetVariable* subjet  = configMgr->obj->cJets[1];

  //
  int AnalysisType = lep->isMu() ? 2 : 1;

  float minDeltaR_jetlep = 99999;
  float minDeltaR_jetW_truth = 99999;
  float minDeltaPhi_jetW = 999999;
  int n_DeltaPhi_jetW = 0;

  JetVariable* closest_jet = 0;
  // Loop over all jets and find the reconstructed jet
  // that's closest to the W(lep+met) system in deltaPhi
  // and the deltaR between the lepton and jet  
  // Also store the truth quantity 
  for( unsigned int j=0; j<configMgr->obj->cJets.size(); j++ ){
    auto jet = configMgr->obj->cJets[j];

    // Minimum dR between lepton and jet
    float _minDeltaR_jetlep = jet->DeltaR( *lep );
    if( _minDeltaR_jetlep < minDeltaR_jetlep ){ 
      minDeltaR_jetlep = _minDeltaR_jetlep; 
     }

    // Minimum deltaPhi between jet and reconstructed W boson
    float _minDeltaPhi_jetW = fabs(jet->DeltaPhi( (*lep+configMgr->obj->met) ));
    if( _minDeltaPhi_jetW < minDeltaPhi_jetW ){ 
      minDeltaPhi_jetW = _minDeltaPhi_jetW; 
      minDeltaR_jetW_truth = fabs( jet->truthTLV.DeltaPhi( lep->truthTLV+configMgr->obj->met.getTruthTLV() ) );
      n_DeltaPhi_jetW = j;
      closest_jet = jet;
    }
  }

  // Closest jet angular information
  configMgr->treeMaker->setFloatVariable("DeltaRLepJetClosest",minDeltaR_jetlep);
  configMgr->treeMaker->setFloatVariable("DeltaPhiWJetClosest",minDeltaPhi_jetW);
  configMgr->treeMaker->setIntVariable("n_DeltaPhiWJetClosest",n_DeltaPhi_jetW+1);
  
  // Save leading and subleading too
  configMgr->treeMaker->setFloatVariable("DeltaRLepJet1", leadjet->DeltaR( *lep ) );
  configMgr->treeMaker->setFloatVariable("DeltaRLepJet2", subjet->DeltaR( *lep ) );
  configMgr->treeMaker->setFloatVariable("DeltaPhiWJet1", leadjet->DeltaPhi( (*lep+configMgr->obj->met) ));
  configMgr->treeMaker->setFloatVariable("DeltaPhiWJet2", subjet->DeltaPhi( (*lep+configMgr->obj->met) ));

  // Lepton information
  configMgr->treeMaker->setIntVariable("AnalysisType",AnalysisType);
  configMgr->treeMaker->setFloatVariable("lep1Pt",lep->Pt() );
  configMgr->treeMaker->setFloatVariable("lep1Et",lep->isMu() ? lep->Pt() : lep->Et() );
  configMgr->treeMaker->setFloatVariable("lep1Eta",lep->Eta() );
  configMgr->treeMaker->setFloatVariable("lep1Phi",lep->Phi() );

  // W-system information
  configMgr->treeMaker->setFloatVariable("wPt", (*lep+configMgr->obj->met).Pt() );
  configMgr->treeMaker->setFloatVariable("wEta", (*lep+configMgr->obj->met).Eta() );
  configMgr->treeMaker->setFloatVariable("wPhi", (*lep+configMgr->obj->met).Phi() );

  // Now the corresponding truth information!
  if( lep->hasTruthLink() ){

    configMgr->treeMaker->setFloatVariable("lep1TruthPt", lep->truthLink->Pt() );
    configMgr->treeMaker->setFloatVariable("lep1TruthEta", lep->truthLink->Eta() );
    configMgr->treeMaker->setIntVariable("lep1TruthOrigin", lep->origin );
    configMgr->treeMaker->setIntVariable("lep1TruthType", lep->type  );

    configMgr->treeMaker->setFloatVariable("wTruthPt", (*lep->truthLink+configMgr->obj->met.getTruthTLV() ).Pt() );
    configMgr->treeMaker->setFloatVariable("wTruthEta", (*lep->truthLink+configMgr->obj->met.getTruthTLV() ).Eta() );
    configMgr->treeMaker->setFloatVariable("wTruthPhi", (*lep->truthLink+configMgr->obj->met.getTruthTLV() ).Phi() );

    // Closest jet
    if( closest_jet && closest_jet->hasTruthLink() ){
      configMgr->treeMaker->setFloatVariable("DeltaRTruthLepJetClosest", lep->truthLink->DeltaR( *closest_jet->truthLink ) );
      configMgr->treeMaker->setFloatVariable("DeltaPhiTruthWJetClosest",fabs( closest_jet->truthLink->DeltaPhi( *lep->truthLink+configMgr->obj->met.getTruthTLV() ) ) );
    }

    // Leading jet
    if( leadjet->hasTruthLink() ){
      configMgr->treeMaker->setFloatVariable("DeltaRTruthLepJet1", leadjet->truthLink->DeltaR( *lep->truthLink ) );
      configMgr->treeMaker->setFloatVariable("DeltaPhiTruthWJet1", leadjet->truthLink->DeltaPhi( (*lep->truthLink+configMgr->obj->met) ));
    }

    // Sub-leading jet
    if( subjet->hasTruthLink() ){
      configMgr->treeMaker->setFloatVariable("DeltaRTruthLepJet2", subjet->truthLink->DeltaR( *lep ) );
      configMgr->treeMaker->setFloatVariable("DeltaPhiTruthWJet2", subjet->truthLink->DeltaPhi( (*lep->truthLink+configMgr->obj->met) ));
    }

  } // Lepton has truth link

}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::fillCommonBranches(ConfigMgr*& configMgr)
{

  // Transverse mass
  float mT  = getMt(configMgr->obj);
 
  // Inclusive Meff, all jets with pT>30 GeV
  float meffInc30 = getMeff( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Ht: Scalar sum of jets
  float Ht30 = getHt( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Number of jets
  int nJet25 = getNJets(configMgr->obj, 25.0);
  int nBJet25 = getNBJets(configMgr->obj, 25.0);

  int intTTbarDecayMode = -1;
  if( configMgr->obj->truthEvent.TTbarTLVs.size()>0 ){

    TruthEvent::TTbarDecayMode decayMode = configMgr->obj->truthEvent.decayMode;
    intTTbarDecayMode = (int)decayMode;

    const TLorentzVector* top     = configMgr->obj->truthEvent.getTTbarTLV("top");
    const TLorentzVector* antitop = configMgr->obj->truthEvent.getTTbarTLV("antitop");

    configMgr->treeMaker->setFloatVariable("truthTopPt", top->Pt()  );
    configMgr->treeMaker->setFloatVariable("truthAntiTopPt", antitop->Pt() );
    configMgr->treeMaker->setFloatVariable("truthTtbarPt", (*top+*antitop).Pt()  );
    configMgr->treeMaker->setFloatVariable("truthTtbarM", (*top+*antitop).M()  );

    float avgPt = (top->Pt() + antitop->Pt() ) / 2.0;

    float NNLOWeight = configMgr->objectTools->getTtbarNNLOWeight( avgPt*1000.0,configMgr->obj->evt.dsid,true);

    configMgr->treeMaker->setDoubleVariable("ttbarNNLOWeight", NNLOWeight );
    configMgr->treeMaker->setIntVariable("DecayModeTTbar",intTTbarDecayMode);
  }

  auto truthMET_TLV = configMgr->obj->met.getTruthTLV();

  configMgr->treeMaker->setDoubleVariable("truthMET_Et", truthMET_TLV.Et() );
  configMgr->treeMaker->setDoubleVariable("truthMET_Phi", truthMET_TLV.Phi() );
  configMgr->treeMaker->setDoubleVariable("truthRecoMETPull_Et", (truthMET_TLV.Et() - configMgr->obj->met.Et) / truthMET_TLV.Et() );
  configMgr->treeMaker->setDoubleVariable("truthRecoMETPull_Phi", (truthMET_TLV.Phi() - configMgr->obj->met.phi) / truthMET_TLV.Phi() );

  // Key observables
  configMgr->treeMaker->setFloatVariable("met",configMgr->obj->met.Et );
  configMgr->treeMaker->setFloatVariable("metPhi", configMgr->obj->met.phi );
  configMgr->treeMaker->setFloatVariable("mt",mT);
  configMgr->treeMaker->setFloatVariable("meffInc30",meffInc30);
  configMgr->treeMaker->setFloatVariable("Ht30",Ht30);

  //
  fillJetKinematics(configMgr);
  configMgr->treeMaker->setIntVariable("nJet25",nJet25);
  configMgr->treeMaker->setIntVariable("nBJet25",nBJet25);

  // Weights
  configMgr->treeMaker->setDoubleVariable("leptonWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::LEP));
  configMgr->treeMaker->setDoubleVariable("bTagWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::BTAG));
  configMgr->treeMaker->setDoubleVariable("pileupWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PILEUP));
  configMgr->treeMaker->setDoubleVariable("eventWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT));
  configMgr->treeMaker->setDoubleVariable("genWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("jvtWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::JVT));
  configMgr->treeMaker->setDoubleVariable("genWeightUp",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("genWeightDown",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("SherpaVjetsNjetsWeight",configMgr->obj->evt.getSherpaVjetsNjetsWeight());

  // PDF information
  if (configMgr->treeMaker->getTreeState()=="") {
    configMgr->treeMaker->setFloatVariable("x1", configMgr->obj->evt.x1);
    configMgr->treeMaker->setFloatVariable("x2", configMgr->obj->evt.x2);
    configMgr->treeMaker->setFloatVariable("pdf1", configMgr->obj->evt.pdf1);
    configMgr->treeMaker->setFloatVariable("pdf2", configMgr->obj->evt.pdf2);
    configMgr->treeMaker->setFloatVariable("scalePDF", configMgr->obj->evt.scalePDF);
    configMgr->treeMaker->setIntVariable("id1", configMgr->obj->evt.id1);
    configMgr->treeMaker->setIntVariable("id2", configMgr->obj->evt.id2);
  }

  // Filtering information
  configMgr->treeMaker->setFloatVariable("GenHt", configMgr->obj->evt.GenHt);
  configMgr->treeMaker->setFloatVariable("GenMET", configMgr->obj->evt.GenMET);

}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::fillJetKinematics(ConfigMgr*& configMgr)
{

  for( unsigned int index=0; index<m_saveNJets; index++ ){

    float jetNPt = 0.0; 
    float jetNEta = 0.0; 
    float jetNPhi = 0.0;
    float jetNTruthPt = 0.0; 
    float jetNTruthEta = 0.0; 
    float jetNTruthPhi = 0.0;
    if( configMgr->obj->cJets.size()>=index+1 ){
      jetNPt  = configMgr->obj->cJets[index]->Pt();
      jetNEta = configMgr->obj->cJets[index]->Eta();
      jetNPhi = configMgr->obj->cJets[index]->Phi();

      if( configMgr->obj->cJets[index]->hasTruthLink() ){
        jetNTruthPt  = configMgr->obj->cJets[index]->truthLink->Pt();
        jetNTruthEta = configMgr->obj->cJets[index]->truthLink->Eta();
        jetNTruthPhi = configMgr->obj->cJets[index]->truthLink->Phi();
      }
    }

    configMgr->treeMaker->setFloatVariable( TString::Format("jet%iPt",index+1), jetNPt );
    configMgr->treeMaker->setFloatVariable( TString::Format("jet%iEta",index+1), jetNEta );
    configMgr->treeMaker->setFloatVariable( TString::Format("jet%iPhi",index+1), jetNPhi );
    configMgr->treeMaker->setFloatVariable( TString::Format("jet%iTruthPt",index+1), jetNTruthPt );
    configMgr->treeMaker->setFloatVariable( TString::Format("jet%iTruthEta",index+1), jetNTruthEta );
    configMgr->treeMaker->setFloatVariable( TString::Format("jet%iTruthPhi",index+1), jetNTruthPhi );
  
  }

}
// ------------------------------------------------------------------------------------------ //
bool WjetsModelling::passRecoCuts(ConfigMgr*& configMgr)
{

  /*
     Reconstructed level cuts
  */

  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  // Fill cutflow histograms
  configMgr->cutflow->bookCut("cutFlow","allEvents",weight );

  // Apply all recommended event cleaning cuts
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  // At least two jets
  if( getNJets(configMgr->obj, 25.0) < 2 ) return false;
  configMgr->cutflow->bookCut("cutFlow", "At least two jets with pT>25 GeV", weight);

  // Exactly one-lepton selection
  if( configMgr->obj->signalLeptons.size() == 1 ){
    configMgr->cutflow->bookCut("cutFlow","Exactly one signal lepton", weight );

    if( getMt(configMgr->obj) < 40.0 ) return false;
    configMgr->cutflow->bookCut("cutFlow","mT > 40 GeV", weight );

    if( configMgr->obj->met.Et < 200.0 ) return false;
    configMgr->cutflow->bookCut("cutFlow","MET > 200 GeV", weight );

  }
  // Exactly two-lepton selection
  else if( configMgr->obj->signalLeptons.size() == 2 ){

    configMgr->cutflow->bookCut("cutFlow","Exactly two signal leptons", weight );

    // SFOS pair
    if( !isSFOS(configMgr->obj->signalLeptons[0],configMgr->obj->signalLeptons[1]) ) return false;
    configMgr->cutflow->bookCut("cutFlow","Opposite signal leptons", weight );

    // No b-tagged jets with pT >25 GeV
    if( getNBJets( configMgr->obj, 25.0 ) > 0  ) return false;
    configMgr->cutflow->bookCut("cutFlow","B-jet veto (pt>25 GeV)", weight );
 
  }
  else{
    return false;
  }

  // Fill output trees
  fillRecoBranches(configMgr);

  // Return gracefully
  return true;

}
// ------------------------------------------------------------------------------------------ //
bool WjetsModelling::passTruthCuts(ConfigMgr*& configMgr)
{

  //
  if( configMgr->obj->evt.isData() ) return false;

  //
  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  //
  configMgr->cutflow->bookCut("TruthSelection","allEvents",weight );

  //
  if( getNTruthJets(configMgr->obj, 10.0 ) < 2 )
  configMgr->cutflow->bookCut("TruthSelection","At least two jets with pT > 10 GeV",weight );

  //
  if( configMgr->obj->truthLeptons.size() != 1 ) return false;
  configMgr->cutflow->bookCut("TruthSelection","Exactly one signal lepton",weight );

  //
  if( configMgr->obj->met.Et_truth < 50.0 ) return false;
  configMgr->cutflow->bookCut("TruthSelection","MET>50 GeV",weight );

  // Fill trees
  fillTruthBranches(configMgr);

  return true;

}
// ------------------------------------------------------------------------------------------ //
void WjetsModelling::finalize(ConfigMgr*& configMgr)
{

}
// ------------------------------------------------------------------------------------------ //
