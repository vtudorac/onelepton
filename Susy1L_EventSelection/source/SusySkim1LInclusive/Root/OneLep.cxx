#include "SusySkim1LInclusive/OneLep.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"

// ------------------------------------------------------------------------------------------ //
OneLep::OneLep() : BaseUser("SusySkim1LInclusive","OneLep")
{

}
// ------------------------------------------------------------------------------------------ //
void OneLep::setup(ConfigMgr*& configMgr)
{
  // Make a cutflow stream
  configMgr->cutflow->defineCutFlow("cutFlow",configMgr->treeMaker->getFile("tree"));
  //configMgr->cutflow->defineCutFlow("cutFlow_dibosonWW",configMgr->treeMaker->getFile("tree"));
  //configMgr->cutflow->defineCutFlow("cutFlow_MET",configMgr->treeMaker->getFile("tree"));
  //configMgr->cutflow->defineCutFlow("cutFlow_HardLepton",configMgr->treeMaker->getFile("tree"));
  //configMgr->cutflow->defineCutFlow("cutFlow_SoftLepton",configMgr->treeMaker->getFile("tree"));
  //configMgr->cutflow->defineCutFlow("cutFlow_Multijet",configMgr->treeMaker->getFile("tree"));

 // Use object def from SUSYTools
 configMgr->obj->useSUSYToolsSignalDef(true);

 // Turn on bad muon veto
 configMgr->obj->applyBadMuonVeto(true);
 configMgr->obj->applyCosmicMuonVeto(true);

 // Muon cuts
 configMgr->obj->setBaselineMuonPt(6.0);
 configMgr->obj->setBaselineMuonEta(2.50);
 configMgr->obj->setSignalMuonPt(6.0);
 configMgr->obj->setSignalMuonEta(2.50);

 // Electrons
 configMgr->obj->setBaselineElectronEt(7.0);
 configMgr->obj->setBaselineElectronEta(2.47);
 configMgr->obj->setSignalElectronEt(7.0);
 configMgr->obj->setSignalElectronEta(2.47);

// Need to repeat above baselinemuon pt for objecttools, this is not yet propagated automatically
// Only applied if DoCombinationBaseline : 1 in deep config!
// configMgr->objectTools->setBaselineMuonPt(6.0);
// configMgr->objectTools->setBaselineMuonEta(2.50);
// configMgr->objectTools->setBaselineElectronEt(7.0);
// configMgr->objectTools->setBaselineElectronEta(2.47);
// configMgr->objectTools->setCombiHighPtThresh(8.0);

 // Jets
 configMgr->obj->setCJetPt(30.0);
 configMgr->obj->setCJetEta(2.80);
 configMgr->obj->setFJetPt(30.0);
 configMgr->obj->setFJetEtaMin(2.50);
 configMgr->obj->setFJetEtaMax(4.50);


  configMgr->addTriggerAna("HLT_xe70_mht",276262,284484,"metTrig");
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",296939,302872,"metTrig");
  configMgr->addTriggerAna("HLT_xe100_mht_L1XE50",302919,303892,"metTrig");
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",303943,320000,"metTrig");
  // 2017
  configMgr->addTriggerAna("HLT_xe110_pufit_L1XE55",320000,341649,"metTrig");
  // 2018
  configMgr->addTriggerAna("HLT_xe110_pufit_xe70_L1XE50",348836,-1,"metTrig");



  // Single lepton triggers
  // Stores the trigger decision and corresponding SFs ( and their systematics )
  // Defined by run number <start,end>; -1 means ignore that bound (e.g. 2016 runs are open ended)
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu50",276262,284484,"singleLepTrig");
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",297730,-1,"singleLepTrig");
  configMgr->addTriggerAna("SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",276262,-1, "singleLepTrig");

  //
  // ---- Output trees ---- //
  //

  // Int variables
  configMgr->treeMaker->addIntVariable("nLep_base",0);
  configMgr->treeMaker->addIntVariable("nLep_combiBase",0);
  configMgr->treeMaker->addIntVariable("nLep_combiBaseHighPt",0);
  configMgr->treeMaker->addIntVariable("nLep_signal",0);
  configMgr->treeMaker->addFloatVariable("mu",0.0);
  configMgr->treeMaker->addFloatVariable("actual_mu",0.0);
  configMgr->treeMaker->addIntVariable("AnalysisType",0.0);
  configMgr->treeMaker->addIntVariable("lep1Charge",0);
  configMgr->treeMaker->addIntVariable("nVtx",0.0);

  configMgr->treeMaker->addIntVariable("nFatjets",0.0);
  configMgr->treeMaker->addIntVariable("nJet30",0.0);
  configMgr->treeMaker->addIntVariable("nJet25",0.0);
  configMgr->treeMaker->addIntVariable("nTotalJet",0.0);
  // configMgr->treeMaker->addIntVariable("nBJet25_DL1",0.0);
  configMgr->treeMaker->addIntVariable("nBJet30_DL1",0.0);
  configMgr->treeMaker->addIntVariable("nBJet30_DL1_FixedCutBEff_60",0.0);
  configMgr->treeMaker->addIntVariable("nBJet30_DL1_FixedCutBEff_70",0.0);
  configMgr->treeMaker->addIntVariable("nBJet30_DL1_FixedCutBEff_85",0.0);

  // Float variables
  configMgr->treeMaker->addFloatVariable("met",0.0);
  configMgr->treeMaker->addFloatVariable("mt",0.0);
  configMgr->treeMaker->addFloatVariable("mct",0.0);
  configMgr->treeMaker->addFloatVariable("mbb",-1.0);
  // configMgr->treeMaker->addFloatVariable("mct2",0.0);
  configMgr->treeMaker->addFloatVariable("mljj",0.0);
  configMgr->treeMaker->addFloatVariable("meffInc30",0.0);
  configMgr->treeMaker->addFloatVariable("Ht30",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Pt",0.0);
  configMgr->treeMaker->addFloatVariable("lep1M",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Eta",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Phi",0.0);
  configMgr->treeMaker->addIntVariable("lep1IFFTruthClassifier",0);
  configMgr->treeMaker->addFloatVariable("lep2Pt",0.0);
  configMgr->treeMaker->addFloatVariable("lep2Eta",0.0);
  configMgr->treeMaker->addFloatVariable("lep2Phi",0.0);
  configMgr->treeMaker->addIntVariable("lep2IFFTruthClassifier",0);
  configMgr->treeMaker->addFloatVariable("LepAplanarity",0.0);
  configMgr->treeMaker->addFloatVariable("JetAplanarity",0.0);
  configMgr->treeMaker->addFloatVariable("dRJet",0.0);
  configMgr->treeMaker->addFloatVariable("dPhiJet",0.0);
  configMgr->treeMaker->addFloatVariable("jetDL1",-0.0);

  configMgr->treeMaker->addFloatVariable("jet1Pt",0.0);
  configMgr->treeMaker->addFloatVariable("jet1M",0.0);
  configMgr->treeMaker->addFloatVariable("jet1Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("jet1Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("jet1DL1",-0.0);

  configMgr->treeMaker->addFloatVariable("jet2Pt",0.0);
  configMgr->treeMaker->addFloatVariable("jet2M",0.0);
  configMgr->treeMaker->addFloatVariable("jet2Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("jet2Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("jet2DL1",-0.0);

  configMgr->treeMaker->addFloatVariable("jet3Pt",0.0);
  configMgr->treeMaker->addFloatVariable("jet3M",0.0);
  configMgr->treeMaker->addFloatVariable("jet3Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("jet3Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("jet3DL1",-0.0);

  configMgr->treeMaker->addFloatVariable("jet4Pt",0.0);
  configMgr->treeMaker->addFloatVariable("jet4M",0.0);
  configMgr->treeMaker->addFloatVariable("jet4Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("jet4Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("jet4DL1",-0.0);

  configMgr->treeMaker->addFloatVariable("jet5Pt",0.0);
  configMgr->treeMaker->addFloatVariable("jet6Pt",0.0);

  configMgr->treeMaker->addFloatVariable("fatjet1Pt",0.0);
  configMgr->treeMaker->addFloatVariable("fatjet1M",0.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Wtagged",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Ztagged",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1D2",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Tau32",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1XbbscoreQCD",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1XbbscoreTop",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1XbbscoreHiggs",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1nTrk",-999.0);

  configMgr->treeMaker->addFloatVariable("fatjet2Pt",0.0);
  configMgr->treeMaker->addFloatVariable("fatjet2M",0.0);
  configMgr->treeMaker->addFloatVariable("fatjet2Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2D2",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2Tau32",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2Wtagged",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2Ztagged",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2XbbscoreQCD",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2XbbscoreTop",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2XbbscoreHiggs",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2nTrk",-999.0);

  // TST Cleaning information
  configMgr->treeMaker->addFloatVariable("TST_Et",0.0);
  configMgr->treeMaker->addFloatVariable("TST_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_Signif",0.0);
  configMgr->treeMaker->addFloatVariable("deltaPhi_MET_TST_Phi", 0.0);
  // adding terms of MET - implementation starts
  configMgr->treeMaker->addFloatVariable("met_px",0.0);
  configMgr->treeMaker->addFloatVariable("met_py",0.0);
  configMgr->treeMaker->addFloatVariable("TST_Et_px",0.0);
  configMgr->treeMaker->addFloatVariable("TST_Et_py",0.0);
  configMgr->treeMaker->addFloatVariable("met_jet",0.0);
  configMgr->treeMaker->addFloatVariable("met_jet_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_jet_px",0.0);
  configMgr->treeMaker->addFloatVariable("met_jet_py",0.0);
  configMgr->treeMaker->addFloatVariable("met_truth",0.0);
  configMgr->treeMaker->addFloatVariable("met_truth_px",0.0);
  configMgr->treeMaker->addFloatVariable("met_truth_py",0.0);
  configMgr->treeMaker->addFloatVariable("met_muon",0.0);
  configMgr->treeMaker->addFloatVariable("met_muon_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_muon_px",0.0);
  configMgr->treeMaker->addFloatVariable("met_muon_py",0.0);
  configMgr->treeMaker->addFloatVariable("met_ele",0.0);
  configMgr->treeMaker->addFloatVariable("met_ele_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_ele_px",0.0);
  configMgr->treeMaker->addFloatVariable("met_ele_py",0.0);
  // terms of MET - implementation ends

  // Doubles
  configMgr->treeMaker->addDoubleVariable("pileupWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("leptonWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("eventWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("jvtWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("bTagWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightUp",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightDown",1.0);
  configMgr->treeMaker->addDoubleVariable("SherpaVjetsNjetsWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("polWeight",1.0);

  // Write these trigger chains
  // The list doesn't include lepton triggers - they are already defined by addTriggerAna
  this->triggerChains =  {
    "HLT_xe70",
    "HLT_xe70_mht",
    "HLT_xe70_mht_wEFMu",
    "HLT_xe70_tc_lcw",
    "HLT_xe70_tc_lcw_wEFMu",
    "HLT_xe80_tc_lcw_L1XE50",
    "HLT_xe90_tc_lcw_L1XE50",
    "HLT_xe90_mht_L1XE50",
    "HLT_xe90_tc_lcw_wEFMu_L1XE50",
    "HLT_xe90_mht_wEFMu_L1XE50",
    "HLT_xe100_L1XE50",
    "HLT_xe100_wEFMu_L1XE50",
    "HLT_xe100_tc_lcw_L1XE50",
    "HLT_xe100_mht_L1XE50",
    "HLT_xe100_tc_lcw_wEFMu_L1XE50",
    "HLT_xe100_mht_wEFMu_L1XE50",
    "HLT_xe110_L1XE50",
    "HLT_xe110_tc_em_L1XE50",
    "HLT_xe110_wEFMu_L1XE50",
    "HLT_xe110_tc_em_wEFMu_L1XE50",
    "HLT_xe110_mht_L1XE50", // emulated
    "HLT_xe110_pufit_L1XE55", // 2017
    "HLT_xe110_pufit_xe70_L1XE50", //2018
    "HLT_noalg_L1J400"
  };
  for (TString& chainName : this->triggerChains) {
    configMgr->treeMaker->addBoolVariable(chainName.Data(), false);
  }

  // For now, also write out the matching flags in single lepton events
  this->singleLepTriggers = {
    "HLT_mu60_0eta105_msonly",
    "HLT_e300_etcut",
    "HLT_e24_lhmedium_L1EM20VH", //single electron - 2015
    "HLT_e60_lhmedium", //single electron - 2015
    "HLT_e120_lhloose", //single electron - 2015
    "HLT_mu20_iloose_L1MU15", //single muon - 2015
    "HLT_mu50", //single muon - 2015 - 2018
    "HLT_e26_lhtight_nod0_ivarloose", //single electron - 2016 - 2018
    "HLT_e60_lhmedium_nod0", //single electron - 2016 - 2018
    "HLT_e140_lhloose_nod0", //single electron - 2016 - 2018
    "HLT_mu26_ivarmedium" //single muon - 2016 - 2018
  };
  for (TString& chainName : this->singleLepTriggers) {
    TString branchName = "match_"+chainName;
    configMgr->treeMaker->addBoolVariable(branchName.Data(), false);
    this->singleLepTriggerBranches.push_back(branchName);
  }

  // PDF variables
  configMgr->treeMaker->addFloatVariable("x1", -1.0);
  configMgr->treeMaker->addFloatVariable("x2", -1.0);
  configMgr->treeMaker->addFloatVariable("pdf1", -1.0);
  configMgr->treeMaker->addFloatVariable("pdf2", -1.0);
  configMgr->treeMaker->addFloatVariable("scalePDF", -1.0);
  configMgr->treeMaker->addIntVariable("id1", 0);
  configMgr->treeMaker->addIntVariable("id2", 0);

  // Truth matching
  configMgr->treeMaker->addIntVariable("lep1Type", -1);
  configMgr->treeMaker->addIntVariable("lep1Origin", -1);

  // Isolation
  configMgr->treeMaker->addFloatVariable("ptcone20", -1.0);
  configMgr->treeMaker->addBoolVariable("IsoFCTight", 0);
  configMgr->treeMaker->addBoolVariable("IsoFCLoose", 0);
  configMgr->treeMaker->addBoolVariable("IsoGradient", 0);
  configMgr->treeMaker->addBoolVariable("IsoFCTightTrackOnly", 0);
  configMgr->treeMaker->addBoolVariable("IsoFixedCutHighPtTrackOnly", 0);
  configMgr->treeMaker->addBoolVariable("IsoFCHighPtCaloOnly", 0);

  // Generator filtering
  // configMgr->treeMaker->addFloatVariable("GenHt", -1.0);
  // configMgr->treeMaker->addFloatVariable("GenMET", -1.0);

  // Set lumi
  configMgr->objectTools->setLumi(1.0);

}
// ------------------------------------------------------------------------------------------ //
bool OneLep::doAnalysis(ConfigMgr*& configMgr)
{
  // Pre-selection cuts
  if( !passCuts(configMgr) ) return false;

  // We requested exactly one lepton
  const LeptonVariable* slep = configMgr->obj->signalLeptons[0];

//  int nCombiBaseLeptons = configMgr->objectTools->m_evtProperties->nCombiBaseLeptons;
//  int nCombiHighPtBaseLeptons = configMgr->objectTools->m_evtProperties->nCombiHighPtBaseLeptons;
//  int nBaseLeptons = configMgr->objectTools->m_evtProperties->nBaseLeptons;
//  int nSignalLeptons = configMgr->objectTools->m_evtProperties->nSignalLeptons;

  // std::cout << "nCombiBaseLeptons: " << nCombiBaseLeptons << ", nCombiHighPtBaseLeptons: " << nCombiHighPtBaseLeptons << ", nBaseLeptons: " << nBaseLeptons << ", nSignalLeptons: " << nSignalLeptons << std::endl;
  // std::cout << "A nSignal: " << configMgr->obj->signalLeptons.size() << ", nBase: " << configMgr->obj->baseLeptons.size() << std::endl;
  // std::cout << "B nSignal: " << nSignalLeptons << ", nBase: " << nBaseLeptons << std::endl;

  // for (auto& l : configMgr->obj->baseLeptons) {
  //     std::cout << "Base lepton pT: " << l->Pt() << std::endl;
  // }

  //
  int AnalysisType   = slep->isMu() ? 2 : 1;

  // --------- Key observables ---------- //

  // Second lepton information
  float lep2Pt   = -999.0;
  float lep2Eta  = -999.0;
  float lep2Phi  = -999.0;
  int lep2IFFTruthClassifier = -1;
  //
  if( configMgr->obj->baseLeptons.size() >= 2 ){
    lep2Pt     = configMgr->obj->baseLeptons[1]->Pt();
    lep2Eta    = configMgr->obj->baseLeptons[1]->Eta();
    lep2Phi    = configMgr->obj->baseLeptons[1]->Phi();
    lep2IFFTruthClassifier = configMgr->obj->baseLeptons[1]->iff_type;
  }

  int DibosonMode = 0;
  int nWTruth = configMgr->obj->truthEvent.Wdecays.size();
  int nZTruth = configMgr->obj->truthEvent.Zdecays.size();

  if     ( nWTruth==1 && nZTruth==1 ) DibosonMode = 1;
  else if( nWTruth==0 && nZTruth==2 ) DibosonMode = 2;
  else if( nWTruth==2 && nZTruth==0 ) DibosonMode = 3;
  else if( nWTruth>=3 || nZTruth>=3 ) DibosonMode = 4;
  else{
    DibosonMode = 5;
  }

  // TTbar2LTagger
  float ttReso = 0.;
  float TopMass1 = 0.;
  float TopMass2 = 0.;
  float WMass1 = 0.;
  float WMass2 = 0.;
  int Npairs = getTTbar2LTagger( configMgr->obj, WMass1, WMass2, TopMass1, TopMass2, ttReso);

  // Hadronic mass and boost
  float wPt = 0.0;
  float wMass = getHadWMass( configMgr->obj, wPt );

  // Leptonic W mass
  const TLorentzVector* lep = configMgr->obj->signalLeptons[0];
  const TLorentzVector* met = dynamic_cast<const TLorentzVector*>(&(configMgr->obj->met));
  float lepWPt = (*lep+*met).Pt();

  // Transverse mass
  float mT  = getMt(configMgr->obj);

  // mljj
  float mljj = configMgr->obj->cJets.size()>=2 ? getMlj(configMgr->obj, 2, false): -999.0;

  // Mct variables
  float mct = getMct(configMgr->obj);
  float mct2 = getMct2(configMgr->obj);

  // Topness
  float topNess = getTopness(configMgr->obj);

  // aMT2 variables
  float amt2        = getMt2(configMgr->obj, AMT2);
  float amt2b       = getMt2(configMgr->obj, AMT2B);
  float amt2bWeight = getMt2(configMgr->obj, AMT2BWEIGHT);
  float mt2tau      = getMt2(configMgr->obj, MT2TAU);
  float mt2lj       = getMt2(configMgr->obj, MT2LJ);

  // Inclusive Meff, all jets with pT>30 GeV
  float meffInc30 = getMeff( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Ht: Scalar sum of jets
  float Ht30 = getHt( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Aplanarity
  float JetAplanarity = getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),0.0, 30.0 );
  float LepAplanarity = getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),configMgr->obj->signalLeptons.size(), 30.0 );

  //PV
  int nVtx           = configMgr->obj->evt.nVtx;

  // Jet multiplicity
  int nJet30  = getNJets( configMgr->obj, 30.0 );
  int nBJet30 = getNBJets( configMgr->obj, 30.0 );
  // int nBJet25 = getNBJets( configMgr->obj, 25.0 );
  // int nBJet20 = getNBJets( configMgr->obj, 20.0 );

  const int nFatJets = configMgr->obj->cFatjets.size();


  // Jet Kinematics
  float j1M = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->M() : 0.0;
  float j2M = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->M() : 0.0;
  float j3M = configMgr->obj->cJets.size()>=3 ? configMgr->obj->cJets[2]->M() : 0.0;
  float j4M = configMgr->obj->cJets.size()>=4 ? configMgr->obj->cJets[3]->M() : 0.0;
  float j5M = configMgr->obj->cJets.size()>=5 ? configMgr->obj->cJets[4]->M() : 0.0;
  float j6M = configMgr->obj->cJets.size()>=6 ? configMgr->obj->cJets[5]->M() : 0.0;
  float j7M = configMgr->obj->cJets.size()>=7 ? configMgr->obj->cJets[6]->M() : 0.0;
  float j8M = configMgr->obj->cJets.size()>=8 ? configMgr->obj->cJets[7]->M() : 0.0;

  float j1Pt = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Pt() : 0.0;
  float j2Pt = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->Pt() : 0.0;
  float j3Pt = configMgr->obj->cJets.size()>=3 ? configMgr->obj->cJets[2]->Pt() : 0.0;
  float j4Pt = configMgr->obj->cJets.size()>=4 ? configMgr->obj->cJets[3]->Pt() : 0.0;
  float j5Pt = configMgr->obj->cJets.size()>=5 ? configMgr->obj->cJets[4]->Pt() : 0.0;
  float j6Pt = configMgr->obj->cJets.size()>=6 ? configMgr->obj->cJets[5]->Pt() : 0.0;
  float j7Pt = configMgr->obj->cJets.size()>=7 ? configMgr->obj->cJets[6]->Pt() : 0.0;
  float j8Pt = configMgr->obj->cJets.size()>=8 ? configMgr->obj->cJets[7]->Pt() : 0.0;


  float j1Eta = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Eta(): -999.0;
  float j1Phi = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Phi(): -999.0;
  float j2Eta = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->Eta(): -999.0;
  float j2Phi = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->Phi(): -999.0;
  float j3Eta = configMgr->obj->cJets.size()>=3 ? configMgr->obj->cJets[2]->Eta(): -999.0;
  float j3Phi = configMgr->obj->cJets.size()>=3 ? configMgr->obj->cJets[2]->Phi(): -999.0;
  float j4Eta = configMgr->obj->cJets.size()>=4 ? configMgr->obj->cJets[3]->Eta(): -999.0;
  float j4Phi = configMgr->obj->cJets.size()>=4 ? configMgr->obj->cJets[3]->Phi(): -999.0;
  float j5Eta = configMgr->obj->cJets.size()>=5 ? configMgr->obj->cJets[4]->Eta(): -999.0;
  float j5Phi = configMgr->obj->cJets.size()>=5 ? configMgr->obj->cJets[4]->Phi(): -999.0;
  float j6Eta = configMgr->obj->cJets.size()>=6 ? configMgr->obj->cJets[5]->Eta(): -999.0;
  float j6Phi = configMgr->obj->cJets.size()>=6 ? configMgr->obj->cJets[5]->Phi(): -999.0;
  float j7Eta = configMgr->obj->cJets.size()>=7 ? configMgr->obj->cJets[6]->Eta(): -999.0;
  float j7Phi = configMgr->obj->cJets.size()>=7 ? configMgr->obj->cJets[6]->Phi(): -999.0;
  float j8Eta = configMgr->obj->cJets.size()>=8 ? configMgr->obj->cJets[7]->Eta(): -999.0;
  float j8Phi = configMgr->obj->cJets.size()>=8 ? configMgr->obj->cJets[7]->Phi(): -999.0;

  float dRJet = sqrt((j1Phi-j2Phi)*(j1Phi-j2Phi)+(j1Eta-j2Eta)*(j1Eta-j2Eta));
  float dPhiJet = fabs(j1Phi-j2Phi);

  int nBJet_FixedCutBEff_60=0;
  int nBJet_FixedCutBEff_70=0;
  int nBJet_FixedCutBEff_85=0;

  float j1DL1 = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->dl1 : 0.0;
  float j2DL1 = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->dl1 : 0.0;
  float j3DL1 = configMgr->obj->cJets.size()>=3 ? configMgr->obj->cJets[2]->dl1 : 0.0;
  float j4DL1 = configMgr->obj->cJets.size()>=4 ? configMgr->obj->cJets[3]->dl1 : 0.0;

  float jetDL1 = 0;
  for ( auto& jet: configMgr->obj->cJets )
    jetDL1 = jet->dl1;

  for( auto& jet : configMgr->obj->cJets ){

    // if( fabs(jet->Eta()) > 2.5 || jet->Pt()<30.0 ) continue;

    // Different WPs for AntiKt4EMPFlowJets
    if( jet->dl1 >=  2.75 ) nBJet_FixedCutBEff_60++;
    if( jet->dl1 >=  2.01)  nBJet_FixedCutBEff_70++;
    if( jet->dl1 >=  0.41)  nBJet_FixedCutBEff_85++;

  }

  float fatjet1M = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->M() : 0.0;
  float fatjet2M = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->M() : 0.0;

  float fatjet1Pt = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Pt() : 0.0;
  float fatjet2Pt = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Pt() : 0.0;

  float fatjet1Eta = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Eta(): -999.0;
  float fatjet1Phi = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Phi(): -999.0;
  float fatjet2Eta = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Eta(): -999.0;
  float fatjet2Phi = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Phi(): -999.0;

  float fatjet1D2 = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->D2 : 0.0;
  float fatjet2D2 = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->D2 : 0.0;

  float fatjet1Tau32 = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Tau32 : 0.0;
  float fatjet2Tau32 = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Tau32 : 0.0;

  float fatjet1Wtagged = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->passWTag50 : -999.0;
  float fatjet2Wtagged = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->passWTag50 : -999.0;

  float fatjet1Ztagged = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->passZTag50 : -999.0;
  float fatjet2Ztagged = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->passZTag50 : -999.0;

  float fatjet1XbbscoreHiggs = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->XbbScoreHiggs : -999.0;
  float fatjet2XbbscoreHiggs = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->XbbScoreHiggs : -999.0;

  float fatjet1XbbscoreTop = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->XbbScoreTop : -999.0;
  float fatjet2XbbscoreTop = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->XbbScoreTop : -999.0;

  float fatjet1XbbscoreQCD = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->XbbScoreQCD : -999.0;
  float fatjet2XbbscoreQCD = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->XbbScoreQCD : -999.0;

  float fatjet1nTrk = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->nTrk : -1.0;
  float fatjet2nTrk = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->nTrk : -1.0;

  // Leading 2 B-jet invariant mass
  float mbb = -1.0;

  if (configMgr->obj->bJets.size()>=2) {
    mbb = (*(configMgr->obj->bJets[0])+*(configMgr->obj->bJets[1])).M();
  }

  // ++++++++++++++++++++++++++++++ //
  //        FAT JETS                //
  // ++++++++++++++++++++++++++++++ //
  // Reclustered, trimmed fat jets
  std::vector<Observables::FatJet> rc_t_100jets = getReclusteredTrimmedFatJets(configMgr->obj, 1.0, 0.05);
  std::vector<Observables::FatJet> rc_t_80jets = getReclusteredTrimmedFatJets(configMgr->obj, 0.8, 0.10);

  // Total fat jet mass using R=0.8 and 0.1 pt fraction
  float rc_t_jet_mtot = 0.;
  for (unsigned int i=0; i<rc_t_80jets.size(); i++) {
    if (i >=4) {
      break;
    }
    rc_t_jet_mtot += rc_t_80jets[i].jet.M();
  }

  float rc_t_jet1_m = rc_t_100jets.size() > 0 ? rc_t_100jets[0].jet.M() : -999.;
  float rc_t_jet1_pt = rc_t_100jets.size() > 0 ? rc_t_100jets[0].jet.Pt() : -999.;
  float rc_t_jet2_m = rc_t_100jets.size() > 1 ? rc_t_100jets[1].jet.M() : -999.;
  float rc_t_jet2_pt = rc_t_100jets.size() > 1 ? rc_t_100jets[1].jet.Pt() : -999.;
  int rc_t_jet1_nsubjets = rc_t_100jets.size() > 0 ? rc_t_100jets[0].nSubJets: -999;
  int rc_t_jet2_nsubjets = rc_t_100jets.size() > 1 ? rc_t_100jets[1].nSubJets: -999;


  //
  // -------- Fill the output tree variables ---------- //
  //

  // +++++++++++++++++++++++++++++++++++++ //
  //        WZ MASS RECONSTRUCTION         //
  // +++++++++++++++++++++++++++++++++++++ //

  RecoBoson Wboson;
  RecoBoson Zboson;


  // ----------------------------- //

  // Int actual_mu
  configMgr->treeMaker->setFloatVariable("mu",configMgr->obj->evt.avg_mu);
  configMgr->treeMaker->setFloatVariable("actual_mu",configMgr->obj->evt.actual_mu);
  configMgr->treeMaker->setIntVariable("AnalysisType",AnalysisType);
  configMgr->treeMaker->setIntVariable("lep1Charge",slep->q);
  configMgr->treeMaker->setIntVariable("nLep_signal",configMgr->obj->signalLeptons.size());
  configMgr->treeMaker->setIntVariable("nLep_base",configMgr->obj->baseLeptons.size());
//  configMgr->treeMaker->setIntVariable("nLep_combiBase",nCombiBaseLeptons);
//  configMgr->treeMaker->setIntVariable("nLep_combiBaseHighPt",nCombiHighPtBaseLeptons);
  configMgr->treeMaker->setIntVariable("nJet30",nJet30);
  configMgr->treeMaker->setIntVariable("nFatjets",nFatJets);
  configMgr->treeMaker->setIntVariable("nJet25",getNJets( configMgr->obj, 25.0 ));
  configMgr->treeMaker->setIntVariable("nTotalJet",getNJets( configMgr->obj, 25.0 ) + configMgr->obj->fJets.size() );
  configMgr->treeMaker->setIntVariable("nBJet30_DL1",nBJet30);
  configMgr->treeMaker->setIntVariable("nBJet30_DL1_FixedCutBEff_60",nBJet_FixedCutBEff_60);
  configMgr->treeMaker->setIntVariable("nBJet30_DL1_FixedCutBEff_70",nBJet_FixedCutBEff_70);
  configMgr->treeMaker->setIntVariable("nBJet30_DL1_FixedCutBEff_85",nBJet_FixedCutBEff_85);
  configMgr->treeMaker->setIntVariable("nVtx",nVtx);
  configMgr->treeMaker->setIntVariable("lep1Type", configMgr->obj->signalLeptons.size()>0 ? configMgr->obj->signalLeptons[0]->type : -1);
  configMgr->treeMaker->setIntVariable("lep1Origin", configMgr->obj->signalLeptons.size()>0 ? configMgr->obj->signalLeptons[0]->origin : -1);

  // Float

  configMgr->treeMaker->setFloatVariable("mct",mct2);
  configMgr->treeMaker->setFloatVariable("mljj",mljj);
  configMgr->treeMaker->setFloatVariable("met",configMgr->obj->met.Et);
  configMgr->treeMaker->setFloatVariable("TST_Et",configMgr->obj->met.Et_soft );
  configMgr->treeMaker->setFloatVariable("TST_Phi",configMgr->obj->met.phi_soft );
  // adding terms of MET - implementation starts
  configMgr->treeMaker->setFloatVariable("met_px",configMgr->obj->met.px );
  configMgr->treeMaker->setFloatVariable("met_py",configMgr->obj->met.py );
  configMgr->treeMaker->setFloatVariable("TST_Et_px",configMgr->obj->met.px_soft );
  configMgr->treeMaker->setFloatVariable("TST_Et_py",configMgr->obj->met.py_soft );
  configMgr->treeMaker->setFloatVariable("met_jet",configMgr->obj->met.Et_jet );
  configMgr->treeMaker->setFloatVariable("met_jet_Phi",configMgr->obj->met.phi_jet );
  configMgr->treeMaker->setFloatVariable("met_jet_px",configMgr->obj->met.px_jet );
  configMgr->treeMaker->setFloatVariable("met_jet_py",configMgr->obj->met.py_jet );
  configMgr->treeMaker->setFloatVariable("met_truth",configMgr->obj->met.Et_truth );
  configMgr->treeMaker->setFloatVariable("met_truth_px",configMgr->obj->met.px_truth );
  configMgr->treeMaker->setFloatVariable("met_truth_py",configMgr->obj->met.py_truth );
  configMgr->treeMaker->setFloatVariable("met_muon",configMgr->obj->met.Et_muon );
  configMgr->treeMaker->setFloatVariable("met_muon_Phi",configMgr->obj->met.phi_muon );
  configMgr->treeMaker->setFloatVariable("met_muon_px",configMgr->obj->met.px_muon );
  configMgr->treeMaker->setFloatVariable("met_muon_py",configMgr->obj->met.py_muon );
  configMgr->treeMaker->setFloatVariable("met_ele",configMgr->obj->met.Et_ele );
  configMgr->treeMaker->setFloatVariable("met_ele_Phi",configMgr->obj->met.phi_ele );
  configMgr->treeMaker->setFloatVariable("met_ele_px",configMgr->obj->met.px_ele );
  configMgr->treeMaker->setFloatVariable("met_ele_py",configMgr->obj->met.py_ele );
  // terms of MET - implementation ends
  configMgr->treeMaker->setFloatVariable("met_Phi",configMgr->obj->met.phi );
  configMgr->treeMaker->setFloatVariable("met_Signif",configMgr->obj->met.metSignif);
  configMgr->treeMaker->setFloatVariable("deltaPhi_MET_TST_Phi", TVector2::Phi_mpi_pi(configMgr->obj->met.phi_soft-configMgr->obj->met.phi) );
  configMgr->treeMaker->setFloatVariable("mt",mT);
  configMgr->treeMaker->setFloatVariable("meffInc30",meffInc30);
  configMgr->treeMaker->setFloatVariable("Ht30",Ht30);
  configMgr->treeMaker->setFloatVariable("lep1Pt",slep->Pt() );
  configMgr->treeMaker->setFloatVariable("lep1Eta",slep->Eta() );
  configMgr->treeMaker->setFloatVariable("lep1Phi",slep->Phi() );
  configMgr->treeMaker->setIntVariable("lep1IFFTruthClassifier",slep->iff_type);
  configMgr->treeMaker->setFloatVariable("lep1M",slep->M() );
  configMgr->treeMaker->setFloatVariable("lep2Pt",lep2Pt);
  configMgr->treeMaker->setFloatVariable("lep2Eta",lep2Eta);
  configMgr->treeMaker->setFloatVariable("lep2Phi",lep2Phi);
  configMgr->treeMaker->setIntVariable("lep2IFFTruthClassifier",lep2IFFTruthClassifier);
  configMgr->treeMaker->setFloatVariable("LepAplanarity",LepAplanarity);
  configMgr->treeMaker->setFloatVariable("JetAplanarity",JetAplanarity);
  configMgr->treeMaker->setFloatVariable("mbb",mbb);

  /// Jet information
  configMgr->treeMaker->setFloatVariable("jetDL1",jetDL1);
  configMgr->treeMaker->setFloatVariable("dRJet",dRJet);
  configMgr->treeMaker->setFloatVariable("dPhiJet",dPhiJet);
  configMgr->treeMaker->setFloatVariable("jet1Pt",j1Pt);
  configMgr->treeMaker->setFloatVariable("jet1Eta",j1Eta);
  configMgr->treeMaker->setFloatVariable("jet1Phi",j1Phi);
  configMgr->treeMaker->setFloatVariable("jet1M",j1M);
  configMgr->treeMaker->setFloatVariable("jet1DL1",j1DL1);

  configMgr->treeMaker->setFloatVariable("jet2Pt",j2Pt);
  configMgr->treeMaker->setFloatVariable("jet2Eta",j2Eta);
  configMgr->treeMaker->setFloatVariable("jet2Phi",j2Phi);
  configMgr->treeMaker->setFloatVariable("jet2M",j2M);
  configMgr->treeMaker->setFloatVariable("jet2DL1",j2DL1);

  configMgr->treeMaker->setFloatVariable("jet3Pt",j3Pt);
  configMgr->treeMaker->setFloatVariable("jet3Eta",j3Eta);
  configMgr->treeMaker->setFloatVariable("jet3Phi",j3Phi);
  configMgr->treeMaker->setFloatVariable("jet3M",j3M);
  configMgr->treeMaker->setFloatVariable("jet3DL1",j3DL1);

  configMgr->treeMaker->setFloatVariable("jet4Pt",j4Pt);
  configMgr->treeMaker->setFloatVariable("jet4Eta",j4Eta);
  configMgr->treeMaker->setFloatVariable("jet4Phi",j4Phi);
  configMgr->treeMaker->setFloatVariable("jet4M",j4M);
  configMgr->treeMaker->setFloatVariable("jet4DL1",j4DL1);

  configMgr->treeMaker->setFloatVariable("jet5Pt",j5Pt);
  configMgr->treeMaker->setFloatVariable("jet6Pt",j6Pt);

  configMgr->treeMaker->setFloatVariable("fatjet1Pt",fatjet1Pt);
  configMgr->treeMaker->setFloatVariable("fatjet1M",fatjet1M);
  configMgr->treeMaker->setFloatVariable("fatjet1Eta",fatjet1Eta);
  configMgr->treeMaker->setFloatVariable("fatjet1Phi",fatjet1Phi);
  configMgr->treeMaker->setFloatVariable("fatjet1D2",fatjet1D2);
  configMgr->treeMaker->setFloatVariable("fatjet1Tau32",fatjet1Tau32);
  configMgr->treeMaker->setFloatVariable("fatjet1Wtagged",fatjet1Wtagged);
  configMgr->treeMaker->setFloatVariable("fatjet1Ztagged",fatjet1Ztagged);
  configMgr->treeMaker->setFloatVariable("fatjet1XbbscoreQCD",fatjet1XbbscoreQCD);
  configMgr->treeMaker->setFloatVariable("fatjet1XbbscoreTop",fatjet1XbbscoreTop);
  configMgr->treeMaker->setFloatVariable("fatjet1XbbscoreHiggs",fatjet1XbbscoreHiggs);
  configMgr->treeMaker->setFloatVariable("fatjet1nTrk",fatjet1nTrk);

  configMgr->treeMaker->setFloatVariable("fatjet2Pt",fatjet2Pt);
  configMgr->treeMaker->setFloatVariable("fatjet2M",fatjet2M);
  configMgr->treeMaker->setFloatVariable("fatjet2Eta",fatjet2Eta);
  configMgr->treeMaker->setFloatVariable("fatjet2Phi",fatjet2Phi);
  configMgr->treeMaker->setFloatVariable("fatjet2D2",fatjet2D2);
  configMgr->treeMaker->setFloatVariable("fatjet2Tau32",fatjet2Tau32);
  configMgr->treeMaker->setFloatVariable("fatjet2Wtagged",fatjet2Wtagged);
  configMgr->treeMaker->setFloatVariable("fatjet2Ztagged",fatjet2Ztagged);
  configMgr->treeMaker->setFloatVariable("fatjet2XbbscoreQCD",fatjet2XbbscoreQCD);
  configMgr->treeMaker->setFloatVariable("fatjet2XbbscoreTop",fatjet2XbbscoreTop);
  configMgr->treeMaker->setFloatVariable("fatjet2XbbscoreHiggs",fatjet2XbbscoreHiggs);
  configMgr->treeMaker->setFloatVariable("fatjet2nTrk",fatjet2nTrk);

  // Triggers
  for (const char* chainName : this->triggerChains) {
    configMgr->treeMaker->setBoolVariable(chainName, configMgr->obj->evt.getHLTEvtTrigDec(chainName));
  }

  // Triggers
  for (unsigned int i=0; i<this->singleLepTriggers.size(); i++) {
    TString chainName = this->singleLepTriggers[i];
    TString branchName = this->singleLepTriggerBranches[i];
    auto it = slep->triggerMap.find(chainName);
    bool match = false;
    if (it != slep->triggerMap.end()) {
      match = true;
    }
    configMgr->treeMaker->setBoolVariable(branchName.Data(), match);
  }

  // Weights
  configMgr->treeMaker->setDoubleVariable("leptonWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::LEP));
  configMgr->treeMaker->setDoubleVariable("bTagWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::BTAG));
  configMgr->treeMaker->setDoubleVariable("pileupWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PILEUP));
  configMgr->treeMaker->setDoubleVariable("eventWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT));
  configMgr->treeMaker->setDoubleVariable("genWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("jvtWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::JVT));
  configMgr->treeMaker->setDoubleVariable("genWeightUp",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("genWeightDown",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("SherpaVjetsNjetsWeight",configMgr->obj->evt.getSherpaVjetsNjetsWeight());
  configMgr->treeMaker->setDoubleVariable("polWeight",configMgr->obj->truthEvent.polWeight);

  // PDF variables (only for nominal trees)
  if (configMgr->treeMaker->getTreeState()=="") {
    configMgr->treeMaker->setFloatVariable("x1", configMgr->obj->evt.x1);
    configMgr->treeMaker->setFloatVariable("x2", configMgr->obj->evt.x2);
    configMgr->treeMaker->setFloatVariable("pdf1", configMgr->obj->evt.pdf1);
    configMgr->treeMaker->setFloatVariable("pdf2", configMgr->obj->evt.pdf2);
    configMgr->treeMaker->setFloatVariable("scalePDF", configMgr->obj->evt.scalePDF);
    configMgr->treeMaker->setIntVariable("id1", configMgr->obj->evt.id1);
    configMgr->treeMaker->setIntVariable("id2", configMgr->obj->evt.id2);
  }

  // Generator filtering
  // configMgr->treeMaker->setFloatVariable("GenHt", configMgr->obj->evt.GenHt);
  // configMgr->treeMaker->setFloatVariable("GenMET", configMgr->obj->evt.GenMET);

  //
  configMgr->doWeightSystematics();

  // Fill the output tree
  configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

  return true;

}
// ------------------------------------------------------------------------------------------ //
bool OneLep::passCuts(ConfigMgr*& configMgr)
{

  double weight = configMgr->objectTools->getWeight(configMgr->obj);
  // Fill cutflow histograms
  configMgr->cutflow->bookCut("cutFlow","allEvents",weight );

  // Apply all recommended event cleaning cuts
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  // Apply the crackVeto cleaning
  if ( !configMgr->objectTools->m_evtProperties->crackVeto ) return false;
  configMgr->cutflow->bookCut("cutFlow", "Crack veto cleaning for electrons and photons", weight);

  // jets selection with pT > 30 GeV
  if( getNJets(configMgr->obj, 30.0) < 1 ) return false;
  configMgr->cutflow->bookCut("cutFlow", "At least one jet with pT>30 GeV", weight);

  // baseline leptons selection - 1L and 2L
  if ( configMgr->obj->baseLeptons.size() < 1 || configMgr->obj->baseLeptons.size() > 2 )   return false;
  configMgr->cutflow->bookCut("cutFlow", "One or two baseline leptons", weight);

  // signal leptons selection - 1L and 2L
  if ( configMgr->obj->signalLeptons.size() < 1 || configMgr->obj->signalLeptons.size() > 2 )   return false;
    configMgr->cutflow->bookCut("cutFlow", "One or two signal leptons", weight);

  // MET > 100 GeV
  if(configMgr->obj->met.Et < 100.0) return false;
  configMgr->cutflow->bookCut("cutFlow","At least MET>100 GeV", weight );

  // mT > 50 GeV
  if( getMt(configMgr->obj) < 50.0) return false;
  configMgr->cutflow->bookCut("cutFlow","At least mT>50 GeV", weight );





  // Skimming categories

  // Running all skimming functions explicitely here is not the best
  // from a performance point of view, but in order to get the
  // sub-cutflows each one has to be executed

  //bool isPassedDibosonWW = passDibosonWW(configMgr);
  //bool isPassedMET = passMET(configMgr);
  // if (!(isPassedDibosonWW || isPassedMET)) return false;

  // bool isPassedSoftLepton = passSoftLepton(configMgr);
  // bool isPassedHardLepton = passHardLepton(configMgr);
  // bool isPassedMultijet = passMultijet(configMgr);
  // if (!(isPassedSoftLepton || isPassedHardLepton || isPassedMultijet)) return false;
  // configMgr->cutflow->bookCut("cutFlow","Final skimming", weight );

  return true;

}
// ------------------------------------------------------------------------------------------ //
bool OneLep::passDibosonWW(ConfigMgr*& configMgr)
{
  // Skimming for Diboson WW

  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  if( configMgr->obj->signalLeptons.size()!=2 ) return false;
  configMgr->cutflow->bookCut("cutFlow_dibosonWW", "Exactly 2 signal leptons", weight);

  if(  isSF(configMgr->obj->signalLeptons[0],configMgr->obj->signalLeptons[1]) ) return false;
  configMgr->cutflow->bookCut("cutFlow_dibosonWW", "Opposite flavour", weight);

  if( !isOS(configMgr->obj->signalLeptons[0],configMgr->obj->signalLeptons[1]) ) return false;
  configMgr->cutflow->bookCut("cutFlow_dibosonWW", "Opposite sign", weight);

  // Veto any b-jets with 85% and pT > 25 GeV to reduce overwhealming ttbar at this point
  if( getNBJets( configMgr->obj, 25.0 )>0 ) return false;
  configMgr->cutflow->bookCut("cutFlow_dibosonWW", "B-jet veto (25 GeV)", weight);

  return true;
}
// ------------------------------------------------------------------------------------------ //
bool OneLep::passMET(ConfigMgr*& configMgr)
{
  // Standard MET skimming

  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  if( getNJets(configMgr->obj, 25.0) < 2 ) return false;
  configMgr->cutflow->bookCut("cutFlow", "At least two jets with pT>25 GeV", weight);

  if(configMgr->obj->met.Et < 100.0) return false;
  configMgr->cutflow->bookCut("cutFlow", "MET > 100 GeV", weight);

  return true;
}
// ------------------------------------------------------------------------------------------ //

// ------------------------------------------------------------------------------------------ //
void OneLep::finalize(ConfigMgr*& configMgr)
{
  (void)configMgr;

}
// ------------------------------------------------------------------------------------------ //
