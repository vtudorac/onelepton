#include "SusySkim1LInclusive/OneLepR24.h"
#include "SUSYTools/SUSYObjDef_xAOD.h"

// ------------------------------------------------------------------------------------------ //
OneLepR24::OneLepR24() : BaseUser("SusySkim1LInclusive","OneLepR24")
{

}

// double costh(const TLorentzVector &a, const TLorentzVector &b){
//   return a.Vect().Unit() * b.Vect().Unit();
// }
// TLorentzVector BoostBack_parent_RF(const TLorentzVector &child_lab, const TLorentzVector &parent_lab){
//   // output TLorentzVector for child in the parent rest frame                                                       
//   TLorentzVector out = child_lab;    out.Boost(-parent_lab.BoostVector());
//   return out;
// }
// double costhStar(const TLorentzVector &child_lab, const TLorentzVector &parent_lab){
//   if(!(child_lab.E()>0 && parent_lab.E()>0)) return -9.;

//   TLorentzVector child_parent = BoostBack_parent_RF(child_lab, parent_lab);
//   return costh(child_parent, parent_lab);
// }

// ------------------------------------------------------------------------------------------ //
void OneLepR24::setup(ConfigMgr*& configMgr)
{
  // Make a cutflow stream
  configMgr->cutflow->defineCutFlow("cutFlow",configMgr->treeMaker->getFile("tree"));
  //configMgr->cutflow->defineCutFlow("cutFlow_dibosonWW",configMgr->treeMaker->getFile("tree"));
  //configMgr->cutflow->defineCutFlow("cutFlow_MET",configMgr->treeMaker->getFile("tree"));
  // configMgr->cutflow->defineCutFlow("cutFlow_HardLepton",configMgr->treeMaker->getFile("tree"));
  // configMgr->cutflow->defineCutFlow("cutFlow_SoftLepton",configMgr->treeMaker->getFile("tree"));
  // configMgr->cutflow->defineCutFlow("cutFlow_Multijet",configMgr->treeMaker->getFile("tree"));

 // Use object def from SUSYTools
 configMgr->obj->useSUSYToolsSignalDef(true);

 // Turn on bad muon veto
 configMgr->obj->applyBadMuonVeto(true);
 configMgr->obj->applyCosmicMuonVeto(true);

 // Muon cuts
 configMgr->obj->setBaselineMuonPt(6.0);
 configMgr->obj->setBaselineMuonEta(2.50);
 configMgr->obj->setSignalMuonPt(6.0);
 configMgr->obj->setSignalMuonEta(2.50);

 // Electrons
 configMgr->obj->setBaselineElectronEt(7.0);
 configMgr->obj->setBaselineElectronEta(2.47);
 configMgr->obj->setSignalElectronEt(7.0);
 configMgr->obj->setSignalElectronEta(2.47);

// Need to repeat above baselinemuon pt for objecttools, this is not yet propagated automatically
// Only applied if DoCombinationBaseline : 1 in deep config!
// configMgr->objectTools->setBaselineMuonPt(6.0);
// configMgr->objectTools->setBaselineMuonEta(2.50);
// configMgr->objectTools->setBaselineElectronEt(7.0);
// configMgr->objectTools->setBaselineElectronEta(2.47);
// configMgr->objectTools->setCombiHighPtThresh(8.0);

 // Jets
 configMgr->obj->setCJetPt(30.0);
 configMgr->obj->setCJetEta(2.80);
 configMgr->obj->setFJetPt(30.0);
 configMgr->obj->setFJetEtaMin(2.50);
 configMgr->obj->setFJetEtaMax(4.50);


  configMgr->addTriggerAna("HLT_xe70_mht",276262,284484,"metTrig");
  configMgr->addTriggerAna("HLT_xe90_mht_L1XE50",296939,302872,"metTrig");
  configMgr->addTriggerAna("HLT_xe110_mht_L1XE50",303943,320000,"metTrig");
  // 2017
  configMgr->addTriggerAna("HLT_xe110_pufit_L1XE55",320000,341649,"metTrig");
  // 2018
  configMgr->addTriggerAna("HLT_xe110_pufit_xe70_L1XE50",348836,-1,"metTrig");



  // Single lepton triggers
  // Stores the trigger decision and corresponding SFs ( and their systematics )
  // Defined by run number <start,end>; -1 means ignore that bound (e.g. 2016 runs are open ended)
  configMgr->addTriggerAna("HLT_mu20_iloose_L1MU15_OR_HLT_mu50",276262,284484,"singleLepTrig");
  configMgr->addTriggerAna("HLT_mu26_ivarmedium_OR_HLT_mu50",297730,-1,"singleLepTrig");
  configMgr->addTriggerAna("SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0",276262,-1, "singleLepTrig");

  //
  // ---- Output trees ---- //
  //

  // Int variables
  configMgr->treeMaker->addIntVariable("nLep_base",0);
  configMgr->treeMaker->addIntVariable("nLep_combiBase",0);
  configMgr->treeMaker->addIntVariable("nLep_combiBaseHighPt",0);
  configMgr->treeMaker->addIntVariable("nLep_signal",0);
  configMgr->treeMaker->addFloatVariable("mu",0.0);
  configMgr->treeMaker->addFloatVariable("actual_mu",0.0);
  configMgr->treeMaker->addIntVariable("AnalysisType",0.0);
  configMgr->treeMaker->addIntVariable("nVtx",0.0);

  configMgr->treeMaker->addIntVariable("nFatjets",0.0);
  configMgr->treeMaker->addIntVariable("nJet30",0.0);
  configMgr->treeMaker->addIntVariable("nBJet30_DL1",0.0);

  // Float variables
  configMgr->treeMaker->addFloatVariable("met",0.0);
  configMgr->treeMaker->addFloatVariable("mt",0.0);
  configMgr->treeMaker->addFloatVariable("mct",0.0);
  // configMgr->treeMaker->addFloat
  configMgr->treeMaker->addFloatVariable("meffInc30",0.0);
  configMgr->treeMaker->addFloatVariable("Ht30",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Pt",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Eta",0.0);
  configMgr->treeMaker->addFloatVariable("lep1Phi",0.0);
  configMgr->treeMaker->addIntVariable("lep1IFFTruthClassifier",0);
  configMgr->treeMaker->addFloatVariable("lep2Pt",0.0);
  configMgr->treeMaker->addFloatVariable("lep2Eta",0.0);
  configMgr->treeMaker->addFloatVariable("lep2Phi",0.0);
  configMgr->treeMaker->addIntVariable("lep2IFFTruthClassifier",0);

  configMgr->treeMaker->addFloatVariable("jet1Pt",0.0);
  configMgr->treeMaker->addFloatVariable("jet1M",0.0);
  configMgr->treeMaker->addFloatVariable("jet1Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("jet1Phi",-999.0);

  configMgr->treeMaker->addFloatVariable("jet2Pt",0.0);
  configMgr->treeMaker->addFloatVariable("jet2Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("jet2Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("mll",-999.0);
  configMgr->treeMaker->addFloatVariable("mjj",-999.0);
  configMgr->treeMaker->addFloatVariable("dPhi_lep_met",-999.0);

  configMgr->treeMaker->addFloatVariable("fatjet1Pt",0.0);
  configMgr->treeMaker->addFloatVariable("fatjet1M",0.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Eta",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Phi",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Wtagged",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Ztagged",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1D2",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1Tau32",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet1nTrk",-999.0);
  configMgr->treeMaker->addFloatVariable("fatjet2Pt",0.0);
  configMgr->treeMaker->addFloatVariable("fatjet2M",0.0);
  configMgr->treeMaker->addDoubleVariable("fatjet1WSF",1.0);
  configMgr->treeMaker->addDoubleVariable("fatjet1ZSF",1.0);

  // TST Cleaning information
  configMgr->treeMaker->addFloatVariable("met_Phi",0.0);
  configMgr->treeMaker->addFloatVariable("met_Signif",0.0);

   configMgr->treeMaker->addDoubleVariable("leptonTrigWeight", 1.0);
  // Doubles
  configMgr->treeMaker->addDoubleVariable("pileupWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("leptonWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("eventWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("jvtWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("bTagWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightUp",1.0);
  configMgr->treeMaker->addDoubleVariable("genWeightDown",1.0);
  configMgr->treeMaker->addDoubleVariable("SherpaVjetsNjetsWeight",1.0);
  configMgr->treeMaker->addDoubleVariable("polWeight",1.0);

  // Write these trigger chains
  // The list doesn't include lepton triggers - they are already defined by addTriggerAna
  this->triggerChains =  {
    "HLT_xe70_mht",
    "HLT_xe90_mht_L1XE50",
    "HLT_xe110_mht_L1XE50", // emulated
    "HLT_xe110_pufit_L1XE55", // 2017
    "HLT_xe110_pufit_xe70_L1XE50" //2018
  };
  for (TString& chainName : this->triggerChains) {
    configMgr->treeMaker->addBoolVariable(chainName.Data(), false);
  }


  // Set lumi
  configMgr->objectTools->setLumi(1.0);

}
// ------------------------------------------------------------------------------------------ //
bool OneLepR24::doAnalysis(ConfigMgr*& configMgr)
{


  // Pre-selection cuts
  if( !passCuts(configMgr) ) return false;

  // We requested exactly one lepton
  const LeptonVariable* slep = configMgr->obj->signalLeptons[0];

//  int nCombiBaseLeptons = configMgr->objectTools->m_evtProperties->nCombiBaseLeptons;
//  int nCombiHighPtBaseLeptons = configMgr->objectTools->m_evtProperties->nCombiHighPtBaseLeptons;
//  int nBaseLeptons = configMgr->objectTools->m_evtProperties->nBaseLeptons;
//  int nSignalLeptons = configMgr->objectTools->m_evtProperties->nSignalLeptons;

  //
  int AnalysisType   = slep->isMu() ? 2 : 1;

  // --------- Key observables ---------- //

  // Second lepton information
  float lep2Pt   = -999.0;
  float lep2Eta  = -999.0;
  float lep2Phi  = -999.0;
  float mll = -1.0;
  int lep2IFFTruthClassifier = -1;
  //
  if( configMgr->obj->baseLeptons.size() >= 2 ){
    lep2Pt     = configMgr->obj->baseLeptons[1]->Pt();
    lep2Eta    = configMgr->obj->baseLeptons[1]->Eta();
    lep2Phi    = configMgr->obj->baseLeptons[1]->Phi();
    // mll for 2L CR and VR - to be flled only if 2 leptons exist in the event
    mll = sqrt( 2*slep->Pt()*lep2Pt*( cosh(slep->Eta()-lep2Eta) - cos(slep->Phi()-lep2Phi) ));
    lep2IFFTruthClassifier = configMgr->obj->baseLeptons[1]->iff_type;
  }

  // std::cout << configMgr->obj->baseLeptons.size() << " : "<< nlep_base_highpt << " : " << configMgr->obj->looseLeptons.size() << " : " << configMgr->obj->looseLeptons[0]->Pt() << std::endl;

  // Truth information for ttbar
  int intTTbarDecayMode = -1;
  if( configMgr->obj->truthEvent.TTbarTLVs.size()>0 ){

    // Decay mode
    TruthEvent::TTbarDecayMode decayMode = configMgr->obj->truthEvent.decayMode;
    intTTbarDecayMode = (int)decayMode;

    const TLorentzVector* top     = configMgr->obj->truthEvent.getTTbarTLV("top");
    const TLorentzVector* antitop = configMgr->obj->truthEvent.getTTbarTLV("antitop");

    // Top kinematics
    configMgr->treeMaker->setFloatVariable("truthTopPt", top->Pt()  );
    configMgr->treeMaker->setFloatVariable("truthAntiTopPt", antitop->Pt() );
    configMgr->treeMaker->setFloatVariable("truthTtbarPt", (*top+*antitop).Pt()  );
    configMgr->treeMaker->setFloatVariable("truthTtbarM", (*top+*antitop).M()  );

    float avgPt = (top->Pt() + antitop->Pt() ) / 2.0;

    float NNLOWeight = configMgr->objectTools->getTtbarNNLOWeight( avgPt*1000.0,configMgr->obj->evt.dsid,true);

    configMgr->treeMaker->setDoubleVariable("ttbarNNLOWeight", NNLOWeight );
    configMgr->treeMaker->setDoubleVariable("ttbarNNLOWeightUp", NNLOWeight );
    configMgr->treeMaker->setDoubleVariable("ttbarNNLOWeightDown", NNLOWeight );

  }

  // if( slep->isMu() ){
  //   MuonVariable* mu = (MuonVariable*)slep;
  //   configMgr->treeMaker->setFloatVariable("muQOverP", mu->q / ( mu->P() * 1000.0 ) );
  //   configMgr->treeMaker->setFloatVariable("muQOverPErr", mu->cbQoverPErr );
  //   configMgr->treeMaker->setIntVariable("muQuality",mu->quality );
  // }

  // TTbar2LTagger
  float ttReso = 0.;
  float TopMass1 = 0.;
  float TopMass2 = 0.;
  float WMass1 = 0.;
  float WMass2 = 0.;
  int Npairs = getTTbar2LTagger( configMgr->obj, WMass1, WMass2, TopMass1, TopMass2, ttReso);

  // Transverse mass
  float mT  = getMt(configMgr->obj);

  // Mct variables
  float mct = getMct(configMgr->obj);
  float mct2 = getMct2(configMgr->obj);

  // Inclusive Meff, all jets with pT>30 GeV
  float meffInc30 = getMeff( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Ht: Scalar sum of jets
  float Ht30 = getHt( configMgr->obj, configMgr->obj->cJets.size(), 30.0 );

  // Aplanarity
  float JetAplanarity = getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),0.0, 30.0 );
  float LepAplanarity = getAplanarity( configMgr->obj,configMgr->obj->cJets.size(),configMgr->obj->signalLeptons.size(), 30.0 );

  //PV
  int nVtx           = configMgr->obj->evt.nVtx;

  // Jet multiplicity
  int nJet30  = getNJets( configMgr->obj, 30.0 );
  int nBJet30 = getNBJets( configMgr->obj, 30.0 );

  const int nFatJets = configMgr->obj->cFatjets.size();

  // Jet Kinematics
  float j1Pt = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Pt() : 0.0;
  float j2Pt = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->Pt() : 0.0;

  float j1Eta = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Eta(): -999.0;
  float j1Phi = configMgr->obj->cJets.size()>=1 ? configMgr->obj->cJets[0]->Phi(): -999.0;
  float j2Eta = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->Eta(): -999.0;
  float j2Phi = configMgr->obj->cJets.size()>=2 ? configMgr->obj->cJets[1]->Phi(): -999.0;
  
  // mjj variable
  float mjj = -999.;
  if (nJet30 >= 2)
    mjj = sqrt ( 2*j1Pt*j2Pt*( cosh(j1Eta-j2Eta)-cos(j1Phi-j2Phi) ) );
  // dPhi(lepton,MET) variiable
  float dPhi_lep_met = abs( TVector2::Phi_mpi_pi(slep->Phi()-configMgr->obj->met.phi) );

  float fatjet1M = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->M() : 0.0;
  float fatjet2M = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->M() : 0.0;

  float fatjet1Pt = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Pt() : 0.0;
  float fatjet2Pt = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Pt() : 0.0;

  float fatjet1Eta = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Eta(): -999.0;
  float fatjet1Phi = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Phi(): -999.0;
  float fatjet2Eta = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Eta(): -999.0;
  float fatjet2Phi = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Phi(): -999.0;

  float fatjet1D2 = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->D2 : 0.0;
  float fatjet2D2 = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->D2 : 0.0;

  float fatjet1Tau32 = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->Tau32 : 0.0;
  float fatjet2Tau32 = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->Tau32 : 0.0;

  float fatjet1Wtagged = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->passWTag50 : -999.0;
  float fatjet2Wtagged = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->passWTag50 : -999.0;

  float fatjet1Ztagged = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->passZTag50 : -999.0;
  float fatjet2Ztagged = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->passZTag50 : -999.0;

  float fatjet1XbbscoreHiggs = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->XbbScoreHiggs : -999.0;
  float fatjet2XbbscoreHiggs = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->XbbScoreHiggs : -999.0;

  float fatjet1XbbscoreTop = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->XbbScoreTop : -999.0;
  float fatjet2XbbscoreTop = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->XbbScoreTop : -999.0;

  float fatjet1XbbscoreQCD = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->XbbScoreQCD : -999.0;
  float fatjet2XbbscoreQCD = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->XbbScoreQCD : -999.0;

  float fatjet1nTrk = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->nTrk : -1.0;
  float fatjet2nTrk = configMgr->obj->cFatjets.size()>=2 ? configMgr->obj->cFatjets[1]->nTrk : -1.0;

  double fatjet1WSF = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->bjt_wsf50 : 1.0;
  double fatjet1ZSF = configMgr->obj->cFatjets.size()>=1 ? configMgr->obj->cFatjets[0]->bjt_zsf50 : 1.0;


  // Leading 2 B-jet invariant mass
  float mbb = -1.0;

  if (configMgr->obj->bJets.size()>=2) {
    mbb = (*(configMgr->obj->bJets[0])+*(configMgr->obj->bJets[1])).M();
  }

  float mlj1 = configMgr->obj->cJets.size()>=1 ? (*(configMgr->obj->cJets[0])+*(slep)).M() : 0.0;

  // Leading jet and lepton invariant mass
  float mlb1 = 0.0;

  if (configMgr->obj->bJets.size()>=1) {
    mlb1 = (*(configMgr->obj->bJets[0])+*(slep)).M();
  }

  //
  // -------- Fill the output tree variables ---------- //
  //

  //configMgr->treeMaker->setFloatVariable("leptonTrigWeight", configMgr->obj->evt.globalDiLepTrigSF);
  configMgr->treeMaker->setDoubleVariable("leptonTrigWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::TRIG));

  // Int actual_mu
  configMgr->treeMaker->setFloatVariable("mu",configMgr->obj->evt.avg_mu);
  configMgr->treeMaker->setFloatVariable("actual_mu",configMgr->obj->evt.actual_mu);
  configMgr->treeMaker->setIntVariable("AnalysisType",AnalysisType);
  configMgr->treeMaker->setIntVariable("nLep_signal",configMgr->obj->signalLeptons.size());
  configMgr->treeMaker->setIntVariable("nLep_base",configMgr->obj->baseLeptons.size());
 // configMgr->treeMaker->setIntVariable("nLep_combiBase",nCombiBaseLeptons);
 // configMgr->treeMaker->setIntVariable("nLep_combiBaseHighPt",nCombiHighPtBaseLeptons);
  configMgr->treeMaker->setIntVariable("nJet30",nJet30);
  configMgr->treeMaker->setIntVariable("nFatjets",nFatJets);
  configMgr->treeMaker->setIntVariable("nBJet30_DL1",nBJet30);
  configMgr->treeMaker->setIntVariable("nVtx",nVtx);

  // Float
  configMgr->treeMaker->setFloatVariable("mct",mct);
  configMgr->treeMaker->setFloatVariable("met",configMgr->obj->met.Et);
  configMgr->treeMaker->setFloatVariable("met_Phi",configMgr->obj->met.phi );
  configMgr->treeMaker->setFloatVariable("met_Signif",configMgr->obj->met.metSignif);
  configMgr->treeMaker->setFloatVariable("mt",mT);
  configMgr->treeMaker->setFloatVariable("meffInc30",meffInc30);
  configMgr->treeMaker->setFloatVariable("Ht30",Ht30);
  configMgr->treeMaker->setFloatVariable("lep1Pt",slep->Pt() );
  configMgr->treeMaker->setFloatVariable("lep1Eta",slep->Eta() );
  configMgr->treeMaker->setFloatVariable("lep1Phi",slep->Phi() );
  configMgr->treeMaker->setIntVariable("lep1IFFTruthClassifier",slep->iff_type);
  configMgr->treeMaker->setFloatVariable("lep2Pt",lep2Pt);
  configMgr->treeMaker->setFloatVariable("lep2Eta",lep2Eta);
  configMgr->treeMaker->setFloatVariable("lep2Phi",lep2Phi);
  configMgr->treeMaker->setIntVariable("lep2IFFTruthClassifier",lep2IFFTruthClassifier);

  /// Jet information
  configMgr->treeMaker->setFloatVariable("jet1Pt",j1Pt);
  configMgr->treeMaker->setFloatVariable("jet1Eta",j1Eta);
  configMgr->treeMaker->setFloatVariable("jet1Phi",j1Phi);

  configMgr->treeMaker->setFloatVariable("jet2Pt",j2Pt);
  configMgr->treeMaker->setFloatVariable("jet2Eta",j2Eta);
  configMgr->treeMaker->setFloatVariable("jet2Phi",j2Phi);
  configMgr->treeMaker->setFloatVariable("mll",mll);
  configMgr->treeMaker->setFloatVariable("mjj",mjj);
  configMgr->treeMaker->setFloatVariable("dPhi_lep_met",dPhi_lep_met);

  configMgr->treeMaker->setFloatVariable("fatjet1Pt",fatjet1Pt);
  configMgr->treeMaker->setFloatVariable("fatjet1M",fatjet1M);
  configMgr->treeMaker->setFloatVariable("fatjet1Eta",fatjet1Eta);
  configMgr->treeMaker->setFloatVariable("fatjet1Phi",fatjet1Phi);
  configMgr->treeMaker->setFloatVariable("fatjet1D2",fatjet1D2);
  configMgr->treeMaker->setFloatVariable("fatjet1Tau32",fatjet1Tau32);
  configMgr->treeMaker->setFloatVariable("fatjet1Wtagged",fatjet1Wtagged);
  configMgr->treeMaker->setFloatVariable("fatjet1Ztagged",fatjet1Ztagged);
  configMgr->treeMaker->setFloatVariable("fatjet1nTrk",fatjet1nTrk);
  configMgr->treeMaker->setFloatVariable("fatjet2Pt",fatjet2Pt);
  configMgr->treeMaker->setFloatVariable("fatjet2M",fatjet2M);
  configMgr->treeMaker->setDoubleVariable("fatjet1WSF",fatjet1WSF);
  configMgr->treeMaker->setDoubleVariable("fatjet1ZSF",fatjet1ZSF);

  // Triggers
  for (const char* chainName : this->triggerChains) {
    configMgr->treeMaker->setBoolVariable(chainName, configMgr->obj->evt.getHLTEvtTrigDec(chainName));
  }

  // Triggers
   for (unsigned int i=0; i<this->singleLepTriggers.size(); i++) {
     TString chainName = this->singleLepTriggers[i];
     TString branchName = this->singleLepTriggerBranches[i];
     auto it = slep->triggerMap.find(chainName);
     bool match = false;
     if (it != slep->triggerMap.end()) {
       match = true;
     }
     configMgr->treeMaker->setBoolVariable(branchName.Data(), match);
   }

  // Weights
  configMgr->treeMaker->setDoubleVariable("leptonWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::LEP));
  configMgr->treeMaker->setDoubleVariable("bTagWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::BTAG));
  configMgr->treeMaker->setDoubleVariable("pileupWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::PILEUP));
  configMgr->treeMaker->setDoubleVariable("eventWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::EVT));
  configMgr->treeMaker->setDoubleVariable("genWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("jvtWeight",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::JVT));
  configMgr->treeMaker->setDoubleVariable("genWeightUp",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("genWeightDown",configMgr->objectTools->getWeight(configMgr->obj,"",ObjectTools::WeightType::GEN));
  configMgr->treeMaker->setDoubleVariable("SherpaVjetsNjetsWeight",configMgr->obj->evt.getSherpaVjetsNjetsWeight());
  configMgr->treeMaker->setDoubleVariable("polWeight",configMgr->obj->truthEvent.polWeight);


  configMgr->doWeightSystematics();

  // Fill the output tree
  configMgr->treeMaker->Fill(configMgr->getSysState(),"tree");

  return true;

}
// ------------------------------------------------------------------------------------------ //
bool OneLepR24::passCuts(ConfigMgr*& configMgr)
{


  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  // Fill cutflow histograms
  configMgr->cutflow->bookCut("cutFlow","allEvents",weight );

  // Apply all recommended event cleaning cuts
  if( !configMgr->obj->passEventCleaning( configMgr->cutflow, "cutFlow", weight ) ) return false;

  // Apply the crackVeto cleaning
 // if ( !configMgr->objectTools->m_evtProperties->crackVeto ) return false;
  configMgr->cutflow->bookCut("cutFlow", "Crack veto cleaning for electrons and photons", weight);

  // jets selection with pT > 30 GeV
  if( getNJets(configMgr->obj, 30.0) < 1 ) return false;
  configMgr->cutflow->bookCut("cutFlow", "At least one jet with pT>30 GeV", weight);

  // baseline leptons selection - 1L and 2L
  if ( configMgr->obj->baseLeptons.size() < 1 || configMgr->obj->baseLeptons.size() > 2 )   return false;
  configMgr->cutflow->bookCut("cutFlow", "One or two baseline leptons", weight);

  // signal leptons selection - 1L and 2L
  if ( configMgr->obj->signalLeptons.size() < 1 || configMgr->obj->signalLeptons.size() > 2 )   return false;
    configMgr->cutflow->bookCut("cutFlow", "One or two signal leptons", weight);

  // MET > 200 GeV
  if(configMgr->obj->met.Et < 200.0) return false;
  configMgr->cutflow->bookCut("cutFlow","At least MET>200 GeV", weight );

  // mT > 50 GeV
  if( getMt(configMgr->obj) < 50.0) return false;
  configMgr->cutflow->bookCut("cutFlow","At least mT>50 GeV", weight );


  return true;

}
// ------------------------------------------------------------------------------------------ //
bool OneLepR24::passMT(ConfigMgr*& configMgr)
{
  // Standard MET skimming

  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  if(getMt(configMgr->obj) < 50.0) return false;
  configMgr->cutflow->bookCut("cutFlow", "mt > 50 GeV", weight);

  return true;
}
// ------------------------------------------------------------------------------------------ //


// ------------------------------------------------------------------------------------------ //
bool OneLepR24::passMET(ConfigMgr*& configMgr)
{
  // Standard MET skimming

  double weight = configMgr->objectTools->getWeight(configMgr->obj);

  if( getNJets(configMgr->obj, 25.0) < 2 ) return false;
  configMgr->cutflow->bookCut("cutFlow", "At least two jets with pT>25 GeV", weight);

  if(configMgr->obj->met.Et < 180.0) return false;
  configMgr->cutflow->bookCut("cutFlow", "MET > 180 GeV", weight);

  return true;
}
// ------------------------------------------------------------------------------------------ //

// ------------------------------------------------------------------------------------------ //
void OneLepR24::finalize(ConfigMgr*& configMgr)
{

  (void)configMgr;

}
// ------------------------------------------------------------------------------------------ //

