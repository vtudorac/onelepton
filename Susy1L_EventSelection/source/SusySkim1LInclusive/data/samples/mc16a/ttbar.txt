
# NAME     : ttbar
# DATA     : 0
# AF2      : 0
# PRIORITY : 0

# ttbar (https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopFocusGroup)
mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6337_s3126_r9364_p4172

# MET/HT sliced ttbar
mc16_13TeV.407342.PhPy8EG_A14_ttbarHT1k5_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6414_s3126_r9364_p4172
mc16_13TeV.407343.PhPy8EG_A14_ttbarHT1k_1k5_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6414_s3126_r9364_p4172
mc16_13TeV.407344.PhPy8EG_A14_ttbarHT6c_1k_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6414_s3126_r9364_p4172
mc16_13TeV.407345.PhPy8EG_A14_ttbarMET200_300_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6414_s3126_r9364_p4172
mc16_13TeV.407346.PhPy8EG_A14_ttbarMET300_400_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6414_s3126_r9364_p4172
mc16_13TeV.407347.PhPy8EG_A14_ttbarMET400_hdamp258p75_nonallhad.deriv.DAOD_SUSY5.e6414_s3126_r9364_p4172
