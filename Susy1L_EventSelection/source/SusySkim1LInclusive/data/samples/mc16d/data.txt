
# NAME     : data
# DATA     : 1
# AF2      : 0
# PRIORITY : 0

# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/SUSYxAODDerivationsr21
# rucio ls data17_13TeV.period\*.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p\* | grep -e p3372 -e p3388 -e p3402 | sort

# nominal lumi: 43813.7
# with HLT_xe110_pufit_L1XE55: 43584.4

data17_13TeV.periodB.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
data17_13TeV.periodC.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
data17_13TeV.periodD.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
data17_13TeV.periodE.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
data17_13TeV.periodF.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
data17_13TeV.periodH.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
data17_13TeV.periodI.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
data17_13TeV.periodK.physics_Main.PhysCont.DAOD_SUSY5.grp17_v01_p4173
