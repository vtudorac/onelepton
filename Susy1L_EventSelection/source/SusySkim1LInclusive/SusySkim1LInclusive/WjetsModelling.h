#ifndef SusySkim1LInclusive_WjetsModelling_h
#define SusySkim1LInclusive_WjetsModelling_h

//RootCore
#include "SusySkimMaker/BaseUser.h"

class WjetsModelling : public BaseUser
{

 public:
  WjetsModelling();
  ~WjetsModelling() {};

  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);

  void fillRecoBranches(ConfigMgr*& configMgr);
  void do_WAnalysis(ConfigMgr*& configMgr);
  void do_ZAnalysis(ConfigMgr*& configMgr);
  void fillTruthBranches(ConfigMgr*& configMgr);
  void fillCommonBranches(ConfigMgr*& configMgr);
  void fillJetKinematics(ConfigMgr*& configMgr);


  bool passRecoCuts(ConfigMgr*& configMgr);
  bool passTruthCuts(ConfigMgr*& configMgr);

  bool passCuts(ConfigMgr*& configMgr){ return true; }

  void finalize(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& /*mergeTool*/){return true; }
  bool execute_merge(MergeTool*& /*mergeTool*/){return true; }

 protected:

  unsigned int m_saveNJets;

};

static const BaseUser* WjetsModelling_instance __attribute__((used)) = new WjetsModelling();

#endif
