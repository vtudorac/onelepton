#ifndef SusySkim1LInclusive_ttbarModelling_H
#define SusySkim1LInclusive_ttbarModelling_H

#include "SusySkimMaker/BaseUser.h"

class ttbarModelling : public BaseUser
{

 public:
  ttbarModelling();
  ~ttbarModelling() {};

  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);


  bool reconstructTop(ConfigMgr*& configMgr,
		      TLorentzVector& lepTop,TLorentzVector& lepNu,TLorentzVector& lepW,TLorentzVector& lepB,
		      TLorentzVector& hadTop,TLorentzVector& hadW,TLorentzVector& hadB,
                      int& wJet_idx1, int& wJet_idx2);


  float getMinDR(JetVariable* jet,const TLorentzVector* cand1,const TLorentzVector* cand2);

  bool init_merge(MergeTool*& /*mergeTool*/){return true; }
  bool execute_merge(MergeTool*& /*mergeTool*/){return true; }

};

static const BaseUser* ttbarModelling_instance __attribute__((used)) = new ttbarModelling();

#endif
