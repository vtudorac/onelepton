#ifndef XAODNTUPLEANALYSIS_OneLepR24_H
#define XAODNTUPLEANALYSIS_OneLepR24_H

//RootCore
#include "SusySkimMaker/BaseUser.h"

class OneLepR24 : public BaseUser
{

 public:
  OneLepR24();
  ~OneLepR24() {};

  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);

  bool passMET(ConfigMgr*& configMgr);
  bool passMT(ConfigMgr*& configMgr);
  bool passHardLepton(ConfigMgr*& configMgr);
  bool passSoftLepton(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& /*mergeTool*/){return true; }
  bool execute_merge(MergeTool*& /*mergeTool*/){return true; }

  std::vector<TString> triggerChains;
  std::vector<TString> singleLepTriggers;
  std::vector<TString> singleLepTriggerBranches;

};

static const BaseUser* OneLepR24_instance __attribute__((used)) = new OneLepR24();

#endif
