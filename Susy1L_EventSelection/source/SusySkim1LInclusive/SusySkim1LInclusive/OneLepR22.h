#ifndef XAODNTUPLEANALYSIS_OneLepR22_H
#define XAODNTUPLEANALYSIS_OneLepR22_H

//RootCore
#include "SusySkimMaker/BaseUser.h"

class OneLepR22 : public BaseUser
{

 public:
  OneLepR22();
  ~OneLepR22() {};

  void setup(ConfigMgr*& configMgr);
  bool doAnalysis(ConfigMgr*& configMgr);
  bool passCuts(ConfigMgr*& configMgr);
  void finalize(ConfigMgr*& configMgr);

  bool passMET(ConfigMgr*& configMgr);
  bool passMT(ConfigMgr*& configMgr);
  bool passHardLepton(ConfigMgr*& configMgr);
  bool passSoftLepton(ConfigMgr*& configMgr);

  bool init_merge(MergeTool*& /*mergeTool*/){return true; }
  bool execute_merge(MergeTool*& /*mergeTool*/){return true; }

  std::vector<TString> triggerChains;
  std::vector<TString> singleLepTriggers;
  std::vector<TString> singleLepTriggerBranches;

};

static const BaseUser* OneLepR22_instance __attribute__((used)) = new OneLepR22();

#endif
